--training school database
--ให้ทำตามคำสั่งในไฟล์นี้ครับ
DROP VIEW IF EXISTS show_registeritem;
DROP VIEW IF EXISTS show_category;
DROP VIEW IF EXISTS show_subject_and_customer;
DROP VIEW IF EXISTS show_customer_and_branch;
DROP VIEW IF EXISTS show_customer_and_register;
DROP VIEW IF EXISTS show_fullname;
DROP TABLE IF EXISTS registeritem;
DROP TABLE IF EXISTS register;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS subject;
DROP TABLE IF EXISTS branch;
DROP TABLE IF EXISTS grade;
DROP TABLE IF EXISTS customer;
DROP TABLE IF EXISTS name_title;
DROP TABLE IF EXISTS telephone;
DROP TABLE IF EXISTS address;

CREATE TABLE subject(
subject_id SERIAL PRIMARY KEY NOT NULL,
name VARCHAR(100) NOT NULL
);
COMMENT ON TABLE subject IS 'ตารางเก็บรายวิชา ว่า โรงเรียนเรามีสอนอะไรบ้าง';
COMMENT ON COLUMN subject.subject_id IS 'เป็นตัวบอก ID ของวิชา';
COMMENT ON COLUMN subject.name IS 'ชื่อรายวิชา';


CREATE TABLE branch(
branch_id SERIAL PRIMARY KEY NOT NULL,
name VARCHAR(100)NOT NULL,
address_branch TEXT NOT NULL,
telephone VARCHAR(20)NOT NULL
);
COMMENT ON TABLE branch IS 'ตารางเก็บสาขา ทั่อยู่ และ เบอร์โทรศัพท์ ของโรงเรียน';
COMMENT ON COLUMN branch.branch_id IS 'บอก ID ของชื่อสาขา';
COMMENT ON COLUMN branch.name IS 'ชื่อสาขา';
COMMENT ON COLUMN branch.address_branch IS 'ที่อยู่ของโรงเรียน';
COMMENT ON COLUMN branch.telephone IS 'เบอร์โทรศัพท์ของโรงเรียน';


CREATE TABLE grade(
grade_id SERIAL PRIMARY KEY NOT NULL,
grade_name VARCHAR(2)NOT NULL
);
COMMENT ON TABLE grade IS 'ตารางบอกระดับชั้นว่าอยู่ ม อะไร ในที่นี้จะแทนด้วย grade 10ม4 11ม5 12ม6';
COMMENT ON COLUMN grade.grade_id IS 'ID ของระดับชั้น';
COMMENT ON COLUMN grade.grade_name IS 'ชื่อชองระดับชั้น';


CREATE TABLE category(
category_id SERIAL PRIMARY KEY NOT NULL,
branch_id INTEGER NOT NULL,
subject_id INTEGER NOT NULL,
grade_id INTEGER NOT NULL
);
COMMENT ON TABLE category IS 'ตารางเก็บค่าประเภทเวลาเลือกลงสมัคร เช่น เลือกรายวิชา จะบอกว่ารายวิชานี้มีที่สาขาไหนบ้าง ฯลฯ';
COMMENT ON COLUMN category.category_id IS 'ID ของประเภทต่างๆ';
COMMENT ON COLUMN category.branch_id IS 'ตารางเก็บค่ารายระเอียดของโรงเรียน เรียกค่ามาจากตาราง branch';
COMMENT ON COLUMN category.subject_id IS 'ตารางเก็บค่ารายวิชา เรียกค่ามาจาก subject';
COMMENT ON COLUMN category.grade_id IS 'เก็บค่าชั้นปี เรียกค่ามาจาก grade';


----------------------------------------

CREATE TABLE customer
(
customer_id SERIAL PRIMARY KEY NOT NULL,
nametitle_id INTEGER NOT NULL,
first_name VARCHAR(100) NOT NULL,
last_name VARCHAR(255) NOT NULL,
tell_id INTEGER NOT NULL,
address_id INTEGER NOT NULL
);
COMMENT ON TABLE customer IS 'ตารางแสดงรายชื่อนักเรียนที่มาสมัครเรียน';
COMMENT ON COLUMN customer.customer_id IS 'ID ของนักเรียน';
COMMENT ON COLUMN customer.nametitle_id IS 'คำนำหน้าชื่อ ซึ่งจะดึงค่ามาจากตาราง name_title';
COMMENT ON COLUMN customer.first_name IS 'ชื่อจริง';
COMMENT ON COLUMN customer.last_name IS 'นามสกุล';
COMMENT ON COLUMN customer.tell_id IS 'เบอร์โทรศัพท์ของนักเรียน ซึ่งดึงค่ามาจาก ตาราง Telephone';
COMMENT ON COLUMN customer.address_id IS 'ที่อยู่ของนักเรียน ซึ่งดึงค่ามาจาก ตาราง Address';

CREATE TABLE name_title(
nametitle_id SERIAL PRIMARY KEY NOT NULL,
nameprefix VARCHAR(255)
);

COMMENT ON TABLE name_title IS 'ตารางเก็บคำนำหน้าเชื่อ';
COMMENT ON COLUMN name_title.nametitle_id IS 'ID คำนำหน้าชื่อ';
COMMENT ON COLUMN name_title.nameprefix IS 'คำนำหน้าชื่อ';


CREATE TABLE telephone(
tell_id SERIAL PRIMARY KEY NOT NULL,
tell VARCHAR(20)
);
COMMENT ON TABLE telephone IS 'ตารางเก็บเบอร์โทรศัพท์ของนักเรียน';
COMMENT ON COLUMN telephone.tell_id IS 'ID เบอร์โทรศัพท์นักเรียน';
COMMENT ON COLUMN telephone.tell IS 'เบอร์โทรศัพท์นักเรียน';


CREATE TABLE address(
  address_id SERIAL PRIMARY KEY NOT NULL,
  address_No VARCHAR(20)NOT NULL,
  address_long Text,
  zipcode VARCHAR(5)
);
COMMENT ON TABLE address IS 'ตารางเก็บที่อยู่ของนักเรียน';
COMMENT ON COLUMN address.address_id IS 'ID ที่อยู่';
COMMENT ON COLUMN address.address_No IS 'เก็บบ้านเลขที่';
COMMENT ON COLUMN address.zipcode IS 'รหัสไปษณีย์';
COMMENT ON COLUMN address.address_long IS 'เก็บรายระเอียดที่อยู่';


CREATE TABLE register(
  register_id SERIAL PRIMARY KEY NOT NULL,
  customer_id INTEGER NOT NULL,
  registertime TIMESTAMP WITHOUT TIME ZONE
);
COMMENT ON TABLE register IS 'ตารางการลงทะเบียน';
COMMENT ON COLUMN register.register_id IS 'ID ผู้สมัคร';
COMMENT ON COLUMN register.customer_id IS 'รายระเอียดของผู้สมัคร';
COMMENT ON COLUMN register.registertime IS 'เก็บค่าเวลาในการลงทะเบียน';


CREATE TABLE registeritem(
  registeritem_id SERIAL PRIMARY KEY NOT NULL,
  register_id INTEGER NOT NULL,
  caterogy_id INTEGER NOT NULL
);
COMMENT ON TABLE registeritem IS 'ตารางแสดงผลรวมว่าสมัครไปแล้วกี่วิชา';
COMMENT ON COLUMN registeritem.register_id IS 'ID';
COMMENT ON COLUMN registeritem.register_id IS 'เรียกค่าจาก register มาแสดง';
COMMENT ON COLUMN registeritem.register_id IS 'เรียกค่าจาก category มาแสดง';

ALTER TABLE customer
  ADD CONSTRAINT customer_name_title_nametitle_id_fk
FOREIGN KEY (nametitle_id) REFERENCES name_title (nametitle_id);
ALTER TABLE customer
  ADD CONSTRAINT customer_telephone_tell_id_fk
FOREIGN KEY (tell_id) REFERENCES telephone (tell_id);
ALTER TABLE customer
  ADD CONSTRAINT customer_address_address_id_fk
FOREIGN KEY (address_id) REFERENCES address (address_id);
ALTER TABLE category
  ADD CONSTRAINT category_branch_branch_id_fk
FOREIGN KEY (branch_id) REFERENCES branch (branch_id);
ALTER TABLE category
  ADD CONSTRAINT category_subject_subject_id_fk
FOREIGN KEY (subject_id) REFERENCES subject (subject_id);
ALTER TABLE category
  ADD CONSTRAINT category_grade_grade_id_fk
FOREIGN KEY (grade_id) REFERENCES grade (grade_id);
ALTER TABLE register
  ADD CONSTRAINT register_customer_customer_id_fk
FOREIGN KEY (customer_id) REFERENCES customer (customer_id);
ALTER TABLE public.registeritem
  ADD CONSTRAINT registeritem_register_register_id_fk
FOREIGN KEY (register_id) REFERENCES register (register_id);
ALTER TABLE public.registeritem
  ADD CONSTRAINT registeritem_category_category_id_fk
FOREIGN KEY (caterogy_id) REFERENCES category (category_id);

INSERT INTO subject (name)
    VALUES ('thai'),('english'),('math'),('physics'),('biology'),('chemisty'),('computer');

INSERT INTO branch (name,address_branch,telephone)
    VALUES ('rama2','rama2 soi50/2','02-443-2534'),('rama3','rama3 soi30','02-144-5643'),('rama9','rama9 soi50','02-147-3342');

INSERT INTO grade (grade_name)
    VALUES ('10'),('11'),('12');

INSERT INTO telephone (tell)
    VALUES ('081-567-3456'),('084-231-5643'),('081-243-1784'),('096-378-4198');

INSERT INTO name_title (nameprefix)
    VALUES ('Mr'),('Miss'),('Ms'),('Dr');

INSERT INTO address (address_No, address_long, zipcode)
    VALUES ('3/52','Daokanong','10150'),('144/132','phetkasem53Rd','10160'),('453','phetkasem69Rd','10160'),('185/23','bangbon5','10150');

INSERT INTO customer (nametitle_id, first_name, last_name, tell_id, address_id)
    VALUES (2,'Jetsika','Nfargo',1,1),(1,'Jakkit','Chaotubtim',2,2),(2,'Parichat','Noiyai',3,3),(1,'Akekachai','Thaison',4,4);

INSERT INTO register (customer_id,registertime)
    VALUES (1,now()),(2,now()),(3,now()),(4,now());

INSERT INTO category (branch_id, subject_id, grade_id)
    VALUES (1,2,1),(2,1,2),(3,1,2),(2,5,1);

INSERT INTO registeritem (register_id, caterogy_id)
    VALUES (1,1),(2,2),(3,3),(4,2);


CREATE VIEW show_registeritem AS
  SELECT
  registeritem.registeritem_id,
  register.register_id,
  customer.first_name
  FROM
    registeritem
    INNER JOIN register ON registeritem.register_id = register.register_id
    INNER JOIN customer ON register.customer_id = customer.customer_id;

CREATE VIEW show_category AS
  SELECT
    registeritem.registeritem_id,
    registeritem.caterogy_id,
    subject.subject_id,
    subject.name
  FROM
    registeritem
    LEFT JOIN category ON registeritem.caterogy_id = category.category_id
    LEFT JOIN subject ON subject.subject_id = category.subject_id;

CREATE VIEW show_subject_and_customer AS -- แต่ละวิชามีคนเลือกทั้งหมดี่คน
SELECT
  subject_id,
  name,
  (
    SELECT
      count(register.register_id)
    FROM
      register
      INNER JOIN registeritem ON register.register_id = registeritem.register_id
      INNER JOIN category ON registeritem.caterogy_id = category.category_id
    WHERE category.subject_id = subject.subject_id
  ) as total_register
FROM
  subject;

CREATE VIEW show_customer_and_branch AS  -- แต่ละสาขามีคนเลือกทั้งหมดกี่คน
SELECT
  branch_id,
  name,
  (
    SELECT
      count(register.register_id)
    FROM
      register
      INNER JOIN registeritem ON register.register_id = registeritem.register_id
      INNER JOIN category ON registeritem.caterogy_id = category.category_id
    WHERE category.branch_id = branch.branch_id
  )    as total_register
FROM
  branch;

CREATE VIEW show_customer_and_register AS -- แต่ละคนลงไปทั้งหมดกี่วิชา
SELECT
  customer_id,
  first_name,
  (
    SELECT
      count(register.register_id)
    FROM
      register
      INNER JOIN registeritem ON register.register_id = registeritem.register_id
      INNER JOIN category ON registeritem.caterogy_id = category.category_id
    WHERE customer.customer_id = register.customer_id
  ) as total_subject
FROM
  customer;

CREATE VIEW show_fullname AS -- Show FullName
  SELECT
   LENGTH(CONCAT(first_name,'',last_name)),
   CASE
    WHEN LENGTH(CONCAT(first_name,'',last_name)) > 13 THEN 'out'
    END
  FROM
    customer
  WHERE LENGTH(CONCAT(first_name,'',last_name)) > 13;


SELECT   -- ลูกค้าคนที่ 1 เรียนวิชาอะไร
    register.customer_id,
    subject.subject_id,
    subject.name
FROM
  register
  INNER JOIN registeritem ON register.register_id = registeritem.register_id
  INNER JOIN category ON registeritem.caterogy_id = category.category_id
  INNER JOIN subject ON category.subject_id = subject.subject_id
  WHERE register.customer_id = 1;



SELECT
  branch.branch_id,
  branch.name as branchname,
  subject.subject_id,
  subject.name as subjectname
FROM
 branch
 INNER JOIN category ON branch.branch_id = category.branch_id
 INNER JOIN subject ON category.subject_id = subject.subject_id;



SELECT
  first_name,
  (SELECT sum(tell_id) FROM customer WHERE customer.tell_id = customer.customer_id)
FROM customer;


