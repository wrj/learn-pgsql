package mk

class CategoryController {

    def index() {

    }
    def create(){
        Category c1 = new Category()
        c1.setName("ของคราว")
        if(!c1.validate()){
            c1.errors.allErrors.each {
                print(it)
            }
        }else {
            c1.save()
        }


        Category c2 = new Category()
        c2.setName("ผัก")
        if(!c2.validate()){
            c2.errors.allErrors.each {
                print(it)
            }
        }
        else{
            c2.save()
        }

        Category c3 = new Category()
        c3.setName("เครื่องดื่ม")
        if(!c3.validate()){
            c3.errors.allErrors.each {
                print(it)
            }
        }
        else{
            c3.save()
        }

    }
    def update(){

    }
    def delete(){

    }
    def cCategory(){
        Category c = Category.get(1) // สร้าง Instance ขึ้นามา แล้ว get ค่าของ Category id ที่ 1
        Integer cMenu
        cMenu = c.countCategory()
        println(cMenu)
    }
    def list(){
        def c = Category.getAll() // สร้างตัวแปร list ขึ้นมา ดึงค่ามาจาก Category
        c.each { // เนื่องจาก c เป็น List จึงต้อง loop มา เพื่อแสดงผล
            println(it.getName())
            def m = Menu.findAllByCategory(it) // สร้าง list ขึ้นมาเพื่อเก็บค่า ที่มาดึงจาก Menu
            m.each { // Loop เพื่อหาว่าเมนูนี้ อยู่ใน Category ไหน
                println(it.getName())
            }
        }
    }
}
