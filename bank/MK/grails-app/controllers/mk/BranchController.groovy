package mk

import grails.converters.JSON

class BranchController {

    def index() {

    }
    def create(){
        Branch b1 = new Branch()
        b1.setBranch("CentralRama2")
        if (!b1.validate()){
            b1.errors.allErrors.each {
                print(it)
            }
        }
        else{
            b1.save()
        }
        Branch b2 = new Branch()
        b2.setBranch("BigCRama2")
        if (!b2.validate()){
            b2.errors.allErrors.each {
                print(it)
            }
        }
        else{
            b2.save()
        }
        Branch b3 = new Branch()
        b3.setBranch("TheMallThapra")
        if (!b3.validate()){
            b3.errors.allErrors.each {
                print(it)
            }
        }
        else{
            b3.save()
        }
    }
    def checkCustomerInBranch(){
//        Branch b = Branch.get(1)
//        def b1 = b.findAll()
//        b1.each {
//            println(it.getBranch())
//        }
        Branch b = Branch.get(1)
        def e = b.getEmployee()
        e.each {
            println(it.getFirstName())
        }
    }
    def checkTotalinBranch(){
        Branch b = Branch.get(2)
        double total = 0.0
        def p = b.getPayments()
        p.each {
            total += it.getTotalPrice()
        }
        println(total)
    }
    def checkTotalAllBranch(){
        Branch b = new Branch()
        Double sumTotal = 0.0
        def b1 = Branch.getAll()
        b1.each{
            Double total = 0.0
            def p = it.getPayments()
            p.each {
                total += it.getTotalPrice()
            }
            sumTotal += total
            println("ยอดรวมของสาขา ${it.getBranch()} = ${total}")
        }
        println(sumTotal)
    }
    def checkTotalinBranchByCallingMethod(){
        Branch b = Branch.get(1)
        b.getTotalPrice()
    }
    def list(){
        def b = Branch.getAll()
        List listJ = []
        b.each {
            def mapJ = [:]
            mapJ.put("Date",new Date())
            mapJ.put("id",it.getId())
            mapJ.put("name",it.getBranch())
            mapJ.put("totalPrice",it.getTotalPrice())
            def listEmployee = []
            def e = it.getEmployee()
            e.each {
                def mapEmployeeJson = [:]
                mapEmployeeJson.put("employee",it.getFirstName())
                listEmployee.add(mapEmployeeJson)
            }
            mapJ.put("employee",listEmployee)
            def listPayment = []
            def p = it.getPayments()
            p.each {
                def mapPaymentJson = [:]
                mapPaymentJson.put("payment",it.getTotalPrice())
                listPayment.add(mapPaymentJson)
            }
            mapJ.put("payment",listPayment)
            listJ.add(mapJ)
        }
        render listJ as JSON
    }
}
