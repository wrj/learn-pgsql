package mk

class PaymentController {

    def index() {

    }
    def create(){
        Payment p1 = new Payment()
        Employee e1 = Employee.get(2)
        p1.setTotalPrice(420)
        p1.setEmployee(e1)
        p1.setBranch(e1.getBranch())
        if(p1.save()){
            PaymentItem pI1 = new PaymentItem()
            Menu m1 = Menu.get(1)
            pI1.setMenues(m1)
            pI1.setPayment(p1)
            if(!p1.save()){
                p1.errors.allErrors.each {
                    println(it)
                }
            }
            PaymentItem pI2 = new PaymentItem()
            Menu m2 = Menu.get(2)
            pI2.setMenues(m2)
            pI2.setPayment(p1)
            if(!p1.save()){
                p1.errors.allErrors.each {
                    println(it)
                }
            }
        }

        Payment p2 = new Payment()
        Employee e2 = Employee.get(1)
        p2.setTotalPrice(305)
        p2.setEmployee(e2)
        p2.setBranch(e2.getBranch())
        if(p2.save()){
            PaymentItem pI3 = new PaymentItem()
            Menu m3 = Menu.get(5)
            pI3.setMenues(m3)
            pI3.setPayment(p2)
            if(!p2.save()){
                p2.errors.allErrors.each {
                    println(it)
                }
            }
            PaymentItem pI4 = new PaymentItem()
            Menu m4 = Menu.get(6)
            pI4.setMenues(m4)
            pI4.setPayment(p2)
            if(!p2.save()){
                p2.errors.allErrors.each {
                    println(it)
                }
            }
        }
    }
}
