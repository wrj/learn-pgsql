package mk

class MenuController {

    def index() {

    }
    def create(){
        Menu m1 = new Menu()
        Category c1 = Category.get(1)
        m1.setName("เป็ดย่าง")
        m1.setPrice(250)
        m1.setCategory(c1)
        if(!m1.validate())
        {
            m1.errors.allErrors.each {
                print(it)
            }
        }
        else {
            m1.save()
        }

        Menu m2 = new Menu()
        Category c2 = Category.get(1)
        m2.setName("หมูสไสด์")
        m2.setPrice(170)
        m2.setCategory(c2)
        if(!m2.validate()){
            m2.errors.allErrors.each {
                print(it)
            }
        }
        else{
            m2.save()
        }

        Menu m3 = new Menu()
        Category c3 = Category.get(2)
        m3.setName("ชุดผักเอ็มเค")
        m3.setPrice(185)
        m3.setCategory(c3)
        if(!m3.validate()){
            m3.errors.allErrors.each {
                print(it)
            }
        }
        else{
            m3.save()
        }
        Menu m4 = new Menu()
        Category c4 = Category.get(2)
        m4.setName("เห็ดเข็มทอง")
        m4.setPrice(120)
        m4.setCategory(c4)
        if(!m4.validate()){
            m4.errors.allErrors.each {
                print(it)
            }
        }
        else{
            m4.save()
        }

        Menu m5 = new Menu()
        Category c5 = Category.get(3)
        m5.setName("น้ำผลไม้")
        m5.setPrice(45)
        m5.setCategory(c5)
        if(!m5.validate()){
            m5.errors.allErrors.each {
                print(it)
            }
        }
        else {
            m5.save()
        }

        Menu m6 = new Menu()
        Category c6 = Category.get(3)
        m6.setName("น้ำอัดลม")
        m6.setPrice(35)
        m6.setCategory(c6)
        if(!m6.validate()){
            m6.errors.allErrors.each {
                print(it)
            }
        }
        else{
            m6.save()
        }
    }

    def update(){
        Menu m1 = Menu.get(1)
        m1.setName("หมูย่าง")
        m1.setPrice(200)
        if(!m1.validate()){
            m1.errors.allErrors.each {
                print(it)
            }
        }
        else{
            m1.save()
        }

        Menu m2 = Menu.get(2)
        m2.setName("อกไก่")
        m2.setPrice(170)
        m2.save()
        if(!m2.validate()){
            m2.errors.allErrors.each {
                print(it)
            }
        }
        else {
            m2.save()
        }
    }
    def delete(){
        Menu m = Menu.get(1)
        m.delete()
    }

}
