package mk

import grails.converters.JSON

class EmployeeController {   // ชื่อคลาส

    def index() {  // Action หรือ Method

    }
    def create(){
        Employee e1 = new Employee() // สร้าง Instance e1 ขึ้นมา
        Branch b1 = Branch.get(1) // ให้ b1 ดึงค่าจาก Branch โดยใส่ get(1) นั้นหมายความว่า id ที่ 1 ของ Branch
        e1.setBranch(b1) // set ค่า Branch เป็น b1
        e1.setFirstName("เอกชัย") // ให้ e1 set Property FirstName ใน คลาส Employee มีค่าเท่ากับ "เอกชัย"
        e1.setLastName("ศรีวิชัย") // ให้ e1 set Property LastName ใน คลาส Employee มีค่าเท่ากับ "ศรีวิชัย"
        if(!e1.validate()){  // ทำการตรวจสอบ e1 ว่า มีอะไร error ถ้า error ให้เข้าไปทำในเงื่อนไข
            e1.errors.allErrors.each { // เช็คว่า e1 มีอะไร error บ้าง โดยการ วน Loop แล้ว แสดงผลค่าที่ error ออกมา
                print(it)
            }
        }
        else{ // ถ้าไม่ error ไม่มา save
            e1.save()
        }

        Employee e2 = new Employee()
        Branch b2 = Branch.get(2)
        e2.setBranch(b2)
        e2.setFirstName("ฤๅชัย")
        e2.setLastName("อันฑะโอฬาร")
        if(!e2.validate()){
            e2.errors.allErrors.each {
                print(it)
            }
        }
        else{
            e2.save()
        }
    }
    def showEmployeeBill(){ // คลาสนี้จะมีการเรียก Method มากจาก Domain employee
        Employee e = Employee.get(2) // สร้าง Instance ขึ้นมา
        Integer emB // สร้างตัวแปร ขึ้นมาเพื่อรอรับค่า ที่ return มาจาก employeeBill() ใน Domain employee
        emB = e.employeeBill() // ให้ emB เท่ากับ e.employeeBill()
        println(emB) // แสดงค่า emB ใน emB จะแสดงว่า ลูกจ้างทำบิลกี่ใบ
        Double em
        em = e.employeeSell()
        println(em) // ใน em จะแสดงส่า ลูกจ้างทำยอดขายรวมได้เท่าไหร่
    }
    def list(){
        def e = Employee.getAll() //สร้างตัวแปรมา ดึงต่าทั้งหมด จาก Class employ  เป็น list
        List listJ = [] // สร้าง list ขึ้นมาเพื่อที่จะเก็บ map เอาไว้ในนี้เพื่อที่จะทำ Json
        e.each { //ใช้ loop ที่จะให้วนตามจำนวนของลูกจ้าง
            def mapEmployee = [:] // สร้าง map ขึ้นมา map ประกอบไปด้วยเป็น key กับ value
            mapEmployee.put("ชื่อ",it.getFirstName()) // เอาข้อมูลเข้าไปใน map ที่ให้ value เป็น it. เพราะว่า อยู่ใน loop เลย ใช้ it และ สิ่งที่เราต้องการจะ get มา
            mapEmployee.put("นามสกุล",it.getLastName())
            mapEmployee.put("สาขา",it.getBranch().getBranch()) // ในที่นี้จะเห็นเป็นการ get มาสองตัวเนื่องจาก ว่า Domain Employee มัน be long to กับ Branch อยู่จึงสามารถ get ค่าได้เลย
            def listPayment = [] // สร้าง list ขึ้นมา เพื่อจะเก็บ map ของ payment
            def p = it.getPayment() // สร้างตัวแปรขึ้นมา เพื่อดึงค่าจาก payment
            def c = Payment.countByEmployee(it) // เหมือนกับบรรทัดบน แต่จะเป็นการ count แบบ โดยตรงกับคลาส
//            def paymentSize = p.size()
            Double totalPayment = 0.0 // สร้างขึ้นมาเพื่อที่จะหายอดรวมว่า ขายได้เท่าไหร่
            p.each { // ใช้ loop เพื่อทที่จะวนหา จำนวนใบเสร็จของลูกจ้างแต่ละคน
                totalPayment += it.getTotalPrice() // ให้ totalPayment บวก บิลแต่ละใบไปเก็บเป็นยอดรวม
                def mapPayment = [:]
                mapPayment.put("ทำยอดได้",it.getTotalPrice())
                mapPayment.put("ยอดรวม",totalPayment)
                listPayment.add(mapPayment)
            }
            mapEmployee.put("ยอดรวม",totalPayment)
            mapEmployee.put("Billpayment",listPayment)
            mapEmployee.put("countBill",c)
//            mapEmployee.put("countBill",paymentSize)
            listJ.add(mapEmployee) // นำ map ไปยัดใน list
        }
        render listJ as JSON // แปลงออกมาเป็น Json
    }
}
