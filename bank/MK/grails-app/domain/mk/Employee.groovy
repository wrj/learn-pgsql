package mk

class Employee {

    String firstName
    String lastName
    static hasMany = [payment:Payment]
    static belongsTo = [branch:Branch]
    static constraints = {
    }
    static mapping = {
        id genarator:'sequence',params: [sequence:'sqe_employee_id']
    }
    // Method ตรงนี้ นี้ จะสร้างขึ้นเพื่อนำไปใช้ใน Controller เช่น Method employeeBill จะเป็นการนับจำนวนของ payment ว่ามีกี่ใบ
    // จากนั้น ถ้าไปเรียกใช้ใน Controller ** ดูได้จาก EmployeeController บรรทัดที่ 42
    Integer employeeBill(){
        return this.payment.size()
    }
    Double employeeSell(){
        Double total = 0.0
        this.getPayment().each {
            total += it.totalPrice
        }
        return total
    }
}
