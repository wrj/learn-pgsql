package mk

class Branch {

    String branch;
    static hasMany = [payments:Payment,employee:Employee]
    static constraints = {
        branch blank: false;
    }
    static mapping = {
        id genarator:'sequence',params: [sequence:'sqe_branch_id']
    }

    Double getTotalPrice(){
        Double total = 0.0
        this.getPayments().each {
            total += it.getTotalPrice()
        }
        println(total)
        return total
    }
}
