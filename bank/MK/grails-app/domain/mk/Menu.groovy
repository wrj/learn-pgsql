package mk

class Menu {

    String name;
    double price;
    static belongsTo = [category: Category]
    static hasMany = [paymentitem:PaymentItem]
    static constraints = {
        name blank:false

    }
    static mapping = {
        id genarator:'sequence',params: [sequence:'sqe_menu_id']
    }
}
