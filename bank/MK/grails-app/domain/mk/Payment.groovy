package mk

class Payment {
    Double totalPrice
    static belongsTo = [branch:Branch,employee:Employee]
    static constraints = {
    }
    static mapping = {
        id genarator:'sequence',params: [sequence:'sqe_payment_id']
    }
}
