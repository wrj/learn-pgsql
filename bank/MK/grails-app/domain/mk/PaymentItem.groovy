package mk

class PaymentItem {
    static belongsTo = [payment:Payment,menues:Menu]
    static constraints = {
    }
    static mapping = {
        id genarator:'sequence',params: [sequence:'sqe_paymentitem_id']
    }
}
