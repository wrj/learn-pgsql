package testproject

class Customer {

    String name
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'seq_customer_id']
    }
}
