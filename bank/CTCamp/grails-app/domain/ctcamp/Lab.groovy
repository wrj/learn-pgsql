package ctcamp

class Lab {

    String lab
    static hasMany = [student:Student]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'seq_lab_id']
    }
}
