package ctcamp

class Major {

    String major

    static hasMany = [student:Student]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'seq_major_id']
    }
}
