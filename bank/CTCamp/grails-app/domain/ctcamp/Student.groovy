package ctcamp

class Student {

    String firstName
    String lastName
    String studentID
    static belongsTo = [major:Major,lab:Lab]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'seq_student_id']
    }
}
