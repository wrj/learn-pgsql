import Tutor.Branch
import Tutor.Category
import Tutor.Grade
import Tutor.Subject

class BootStrap {

    def init = { servletContext ->
        Subject s = new Subject()
        s.setSubjectName("Math")
        s.save()
        Grade g = new Grade()
        g.setGradeName("ม ต้น")
        g.save()
        Branch b = new Branch()
        b.setBranchName("rama2")
        b.setBranchAddress("52/1")
        b.setBranchTelephone("0853458741")
        b.save()
        Category c = new Category()
        c.setPrice(500)
        c.setSubject(s)
        c.setGrade(g)
        c.setBranch(b)
        c.save()
    }
    def destroy = {
    }
}
