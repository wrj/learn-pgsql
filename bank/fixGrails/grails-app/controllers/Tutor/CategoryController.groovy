package Tutor

class CategoryController {

    def index() {
        Category c = new Category()
        if(!c.validate()){
            c.errors.allErrors.each {
                print(it)
            }
        }
    }
    def create(){
        Category c = new Category()
        Subject s = Subject.get(1)
        Grade g = Grade.get(1)
        Branch b = Branch.get(1)
        c.setPrice(500)
        c.setBranch(b)
        c.setGrade(g)
        c.setSubject(s)
        c.save()
    }
    def update(){
        Category c = Category.get(1)
        c.setPrice(1000)
        c.save()
    }
    def delete(){
        Category c = Category.get(1)
        c.delete()
    }
}
