package Tutor

class BranchController {

    def index() {
        Branch b = new Branch()
        if(!b.validate()){
            b.errors.allErrors.each {
                println(it)
            }
        }
    }
    def create(){
        Branch b = new Branch()
        b.setBranchName("rama2")
        b.setBranchAddress("rama2")
        b.setBranchTelephone("0531402335")
        b.save()
    }
    def update(){
        Branch b = new Branch()
        b.getBranchAddress(1)
        b.getBranchName(1)
        b.getBranchTelephone(1)
        b.setBranchAddress("rama3")
        b.setBranchName("rama3")
        b.setBranchTelephone("0531402335")
        b.save()
    }
    def delete(){
        Branch b = new Branch()
        b.getBranchAddress(1)
        b.getBranchName(1)
        b.getBranchTelephone(1)
        s.delete()
    }
}
