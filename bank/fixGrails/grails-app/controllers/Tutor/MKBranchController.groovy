package Tutor

class MKBranchController {

    def index() {

    }
    def create(){
        MKBranch b1 = new MKBranch()
        b1.setBranchName("CentralRama2")
        b1.save()
        MKBranch b2 = new MKBranch()
        b2.setBranchName("BigCRama2")
        b2.save()
        MKBranch b3 = new MKBranch()
        b3.setBranchName("TheMallBangkae")
        b3.save()
    }
    def update(){
        MKBranch b1 = new MKBranch()
        b1.getBranchName(1)
        b1.setBranchName("CentralRama3")
        b1.save()
        MKBranch b2 = new MKBranch()
        b2.getBranchName(2)
        b2.setBranchName("BigCPetchkasem")
        b2.save()
        MKBranch b3 = new MKBranch()
        b3.getBranchName(3)
        b3.setBranchName("TheMallThapra")
        b3.save()
    }
    def delete(){
        MKBranch b1 = new MKBranch()
        b1.getBranchName(1)
        b1.delete()
        MKBranch b2 = new MKBranch()
        b2.getBranchName(2)
        b2.delete()
        MKBranch b3 = new MKBranch()
        b3.getBranchName(3)
        b3.delete()
    }
}
