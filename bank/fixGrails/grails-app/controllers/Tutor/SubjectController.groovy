package Tutor

class SubjectController {

    def index() {
        Subject s = new Subject()
        if(!s.validate()){
            s.errors.allErrors.each {
                println("Error is:" + it)
            }
        }
    }
    def create(){
        Subject s = new Subject()
        s.setSubjectName("PHYSIC")
        s.save()
    }
    def update(){
        Subject s = new Subject()
        s.getSubjectName(1)
        s.setSubjectName("Math")
        s.save()
    }
    def delete(){
        Subject s = new Subject()
        s.getSubjectName()
        s.delete()
    }
}
