package Tutor

class MenuController {

    def index() {

    }
    def create(){
        Menu m1 = new Menu()
        MKBranch b1 = MKBranch.get(1)
        b1.setBranchName(b1)
        m1.setMenu("เป็ดย่าง")
        m1.setPrice(250)
        m1.save()
        Menu m2 = new Menu()
        MKBranch b2 = MKBranch.get(2)
        b2.setBranchName(b2)
        m2.setMenu("ลูกชิ้นเอ็มเค")
        m2.setPrice(150)
        m2.save()
        Menu m3 = new Menu()
        MKBranch b3 = MKBranch.get(3)
        b3.setBranchName(b3)
        m3.setMenu("ขนมจีบ")
        m3.setPrice(120)
        m3.save()
    }
    def update(){
        Menu m1 = new Menu()
        MKBranch b1 = MKBranch.get(1)
        b1.getBranchName(1)
        m1.getMenu(1)
        m1.setMenu("หมูสไลด์")
        m1.save()
        Menu m2 = new Menu()
        MKBranch b2 = MKBranch.get(2)
        b2.getBranchName(2)
        m2.getMenu(2)
        m2.setMenu("ชุดผักเอ็มเค")
        m2.save()
        Menu m3 = new Menu()
        MKBranch b3 = MKBranch.get(3)
        b3.getBranchName(3)
        m3.getMenu(3)
        m3.setMenu("ไข่")
        m3.save()
    }
    def delete(){
        Menu m1 = new Menu()
        MKBranch b1 = MKBranch.get(1)
        b1.getBranchName(1)
        m1.getMenu(1)
        m1.delete()
        Menu m2 = new Menu()
        MKBranch b2 = MKBranch.get(2)
        b2.getBranchName(2)
        m2.getMenu(2)
        m2.delete()
        Menu m3 = new Menu()
        MKBranch b3 = MKBranch.get(3)
        b3.getBranchName(3)
        m3.getMenu(3)
        m3.delete()
    }
}
