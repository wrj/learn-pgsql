package fixgrails

import grails.converters.JSON

class PersonController {

    def index() {
        Person p = new Person(name: "Banking", age:21, LastVisit: new Date())
        p.save()

        Face f = new Face(hairColor: "red",nose: new Nose(noseColor: "black"))
        f.save()
    }
}
