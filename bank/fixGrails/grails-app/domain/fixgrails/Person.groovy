package fixgrails

class Person {

    String name
    Integer age
    Date LastVisit

    static constraints = {
    }

    static mappping = {

        id generator: 'sequence',params:[sequence:'seq_person_id']
    }
}
