package fixgrails

class Face {

    String hairColor
    Nose nose

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence',params:[sequence:'seq_face_id']
    }
}
