package fixgrails

class Nose {

    String noseColor
    static belongsTo = [face:Face]

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence',params:[sequence:'seq_nose_id']
    }
}
