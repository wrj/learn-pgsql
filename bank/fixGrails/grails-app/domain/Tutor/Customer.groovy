package Tutor

class Customer {
    String firstname
    String lastname
    static hasMany = [register:Register,telephone:Telephone,address:Address]

    static belongsTo = [nametitle:NameTitle]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'customer_id']
    }
}
