package Tutor

class Menu {

    String menu;
    double price;
    static hasMany = [mkpayment:MKPayment]
    static belongsTo = [mkbranch:MKBranch]
    static constraints = {
        menu blank:false

    }
    static mapping = {
        id genarator:'sequence',params: [sequence:'seq_menu_id']
    }
}
