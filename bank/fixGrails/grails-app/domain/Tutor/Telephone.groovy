package Tutor

class Telephone {
    String tell

    static belongsTo = [customer:Customer]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'telephone_id']
    }
}
