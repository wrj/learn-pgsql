package Tutor

class NameTitle {
    String namePrefix


    static hasMany = [customer:Customer]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'nameTitle_id']
    }
}
