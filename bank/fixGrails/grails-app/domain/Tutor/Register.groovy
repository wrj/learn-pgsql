package Tutor

class Register {
    Date timestamp
    static hasMany = [registeritem:RegisterItem]
    static belongsTo = [customer:Customer]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'register_id']
    }
}
