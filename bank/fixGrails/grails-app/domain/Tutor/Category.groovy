package Tutor

class Category {
    Double price
    static belongsTo = [branch:Branch,grade:Grade,subject:Subject]
    static hasMany = [registeritem:RegisterItem]
    static constraints = {
        price nullable: false
    }
    static  mapping = {
        id genarator:'sequence',params: [sequence:'category_id']
    }
}
