package Tutor

class Branch {

    String branchName
    String branchAddress
    String branchTelephone


    static hasMany = [categories:Category]
    static constraints = {
        branchName blank: false
        branchAddress blank: false
        branchTelephone blank: false
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'branch_id']

    }
}
