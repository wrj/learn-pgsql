package Tutor

class Grade {

    String gradeName

    static hasMany = [categories:Category]
    static constraints = {
        gradeName blank: false
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'grade_id']
    }
}
