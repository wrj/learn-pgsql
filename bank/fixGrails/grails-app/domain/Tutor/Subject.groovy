package Tutor

class Subject {
    String subjectName

    static hasMany = [categories:Category]
    static constraints = {
        subjectName blank: false,nullable: false
    }

    static mapping = {
        id generator:'sequence',params: [sequence:'subject_id']
    }
}
