package Tutor

class MKBranch {

    String branchName

    static hasMany = [menu:Menu]
    static constraints = {
        branchName blank: false;
    }
    static mapping = {
        id genarator:'sequence',params: [sequence:'seq_branch_id']
    }
}
