package Tutor

class Address {
    String addressNo
    String addressLong
    String zipcode

    static belongsTo = [customer:Customer]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'address_id']
    }
}
