#Config Grails
ใช้ version 3.1.6

การ config ใช้ postgresql ที่ไฟล์ build.gradle

        //runtime "com.h2database:h2"
        runtime "org.postgresql:postgresql:9.4.1207.jre7"

        comment บรรทัดที่ใช้ h2 เพิ่ม runtime ที่เรียกใช้ postgresql แทน

การ config database ที่ไฟล์ application.yml

    ที่หัวข้อ dataSource ปรับเป็น
    dataSource:
        pooled: true
        jmxExport: true
        driverClassName: org.postgresql.Driver
        dialect: org.hibernate.dialect.PostgreSQLDialect
        username: postgres
        password: postgres

    ที่หัวข้อ environments ปรับจาก h2 เป็น postgres

    dataSource:
        dbCreate: update
        url: jdbc:postgresql://127.0.0.1:5432/database_name

