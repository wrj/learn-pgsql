package lgrails

class Person { /* คลาสนี้เป็นคลาสที่ เอา Face and Nose มา เป็น Foreign Key
                  สังเกตุได้ว่า จะประกาศ คลาส Face and Nose มาอยู่ใน Properties        */
    String name
    Integer age
    Date lastVisit
    Nose nose
    Face face

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_person_id']// ให้ Gen ID แบบ sequence โดย sequence จะชื่อ seq_person_id
    }
}
