package lgrails

class Face {

    String hairColor // Properties in class เอาไว้รับค่าสีผม รับค่าแบบ String

    static belongsTo = [person:Person] // เป็น Foreign Key ของ Person Table

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_face_id']// ให้ Gen ID แบบ sequence โดย sequence จะชื่อ seq_face_id
    }
}
