package lgrails

class Nose {

    String noseColor //Properties in class เอาไว้รับค่าสีจมูก รับค่าแบบ String
    String noseShap // Properties in class เอาไว้รับค่ารูปร่างของจมูก รับค่าแบบ String

    static belongsTo = [person:Person] // เป็น Foreign Key ของ Person Table

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_nose_id'] // ให้ Gen ID แบบ sequence โดย sequence จะชื่อ seq_nose_id

    }
}
