package lgrails

class NoseController {

    def index() {

    }

    def newForm(){

    }

    def updateForm(){

    }

    def create(){  // สร้าง Method ที่ขื่อว่า create เอาไว้เพิ่ม recode ใน table นี้
        Nose n = new Nose() //สร้าง Instance ของคลาส Nose ขึ้นมา
        n.setNoseColor(params.noseColor) // Set ค่า NoseColor โดยรับค่ามาจาก params.noseColor
        n.setNoseShap(params.noseShap) // // Set ค่า NoseShape โดยรับค่ามาจาก params.noseColorShape **
        n.save()//update nose set nose_shap = '',nose_color='' where id = ?
        redirect(controller: "person",action: "index") // หลังจากที่ทำ Action นี้เสร็จแล้วให้ไปที่ controller person ที่ action index **
    }

    def update(){ // สร้าง Method ที่ชื่อว่า update จะสังเกตุวได้ว่า จาก create คือ จะ get ค่าที่มีอยู่แล้วมาเปลี่ยนแปลง
        Nose n = Nose.get(params.id)//select * from nose where id = ? // get ค่าที่มีอยู่แล้วมาเปลี่ยนแปลง
        n.setNoseColor(params.noseColor) // Set ค่า NoseColor โดยรับค่ามาจาก params.noseColor
        n.setNoseShap(params.noseShap)   // Set ค่า NoseShape โดยรับค่ามาจาก params.noseColorShape
        n.save()//update nose set nose_shap = '',nose_color='' where id = ?
        redirect(controller: "person",action: "index") // หลังจากที่ทำ Action นี้เสร็จแล้วให้ไปที่ controller person ที่ action index
    }

    def delete(){ //สร้าง Method ที่ชื่อว่า delete จะทำคล้ายๆ update แต่จะ get แค่ id มา แล้วลบเลย
        Nose n = Nose.get(params.id)//select * from nose where id = ?
        n.delete()//delete from nose where id = ?
        redirect(controller: "person",action: "index") // หลังจากที่ทำ Action นี้เสร็จแล้วให้ไปที่ controller person ที่ action index
    }
}
