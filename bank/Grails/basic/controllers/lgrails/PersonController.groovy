package lgrails

import grails.converters.JSON

class PersonController {

    def index() {

    }

    def newForm(){

    }

    def updateForm(){
    }

    def create(){ // สร้าง Method ที่ชื่อว่า create โดย Method นี้มีการรับค่ามาจาก Class Face and Nose ด้วย
        //Get Nose
        Nose n = Nose.get(params.nose_id)//select * from nose where id = ?
        //Get Face
        Face f = Face.get(params.face_id)//select * from face where id = ?
        //save
        Person p = new Person() //สร้าง Instance ของ Person ขึ้นมาชื่อว่า p
        p.setName("") // set name รับค่าเป็น String
        p.setAge() // set age รับค่าเป็น Integer
        p.setLastVisit(new Date()) // set เวลา รับค่า เป็น Date ที่ต้อง New เพราะว่า Date เป็นคลาส
        p.setNose(n) // รับค่า จาก Instance n
        p.setFace(f) // รับค่า จาก Instance f
        n.save() // save
        redirect(controller: "person",action: "index") // หลังจากที่ทำ Action นี้เสร็จแล้วให้ไปที่ controller person ที่ action index
    }

    def update(){
        //save
        Person p = Person.get(params.id)//select * from person where id = ?
        p.setName("") // set name รับค่าเป็น String
        p.setAge()  // set age รับค่าเป็น Integer
        p.setLastVisit(new Date()) // set เวลา รับค่า เป็น Date ที่ต้อง New เพราะว่า Date เป็นคลาส
        p.setNose(Nose.get(params.nose_id)) //รับค่าจากการ select * from nose where id = ?
        p.setFace(Face.get(params.face_id)) //รับค่าจากการ select * from face where id = ?
        n.save()//update person
        redirect(controller: "person",action: "index") // หลังจากที่ทำ Action นี้เสร็จแล้วให้ไปที่ controller person ที่ action index
    }

    def delete(){
        Person p = Person.get(params.id)//select * from person where id = ?
        p.delete()//delete from person where id = ?
        redirect(controller: "person",action: "index") // หลังจากที่ทำ Action นี้เสร็จแล้วให้ไปที่ controller person ที่ action index
    }
}
