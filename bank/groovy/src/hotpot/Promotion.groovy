package hotpot

/**
 * Created by piyawat on 6/23/2017 AD.
 */
class Promotion {

    Integer checkPerson(Integer num)
    {
        Integer discount = Math.floor(num.doubleValue()/4)
        return discount;
    }

    Double checkTotal(Double num){
        double cTotal;
        if(num > 1000 && num < 1500){
             cTotal = (num*5)/100
        }
        else if(num > 1500){
            cTotal = (num*10)/100
        }
        return cTotal;
    }

}
