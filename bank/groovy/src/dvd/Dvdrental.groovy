package dvd
/**
 * Created by piyawat on 6/23/2017 AD.
 */
class Dvdrental {

    static void main(String[] args)
    {
        Category c = new Category()
//        Create New Category
        c.setName("New Category 1")
        c.createCategory();
        c.createCategoryByName("New Category 2");
//        End Create New Category
//        Get Category By Id
        c.getCategoryById(1);
        println(c.getName());
        //Set New Name
        c.setName("Hello");
        //Update Category Name
        c.updateCategory();
        c.updateCategoryById(1,"Hello")
        //Delete Category
        c.deleteCategory()
        c.deleteCategoryById(1)
    }
}
