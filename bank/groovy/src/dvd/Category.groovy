package dvd

import groovy.sql.Sql

/**
 * Created by piyawat on 6/26/2017 AD.
 */
class Category {
    Integer categoryId;
    String name;

    List categoryList(Integer limit = 5){
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        def categoryList = sql.rows("SELECT category_id,name FROM category LIMIT ${limit}");
        sql.close()
        return categoryList
    }

    void getCategoryById(Integer pCategoryId) {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        def categoryList = sql.rows("SELECT category_id,name FROM category WHERE category_id = ${pCategoryId}");
        sql.close()

        if (!categoryList.isEmpty())//ถ้า categoryList ไม่เท่ากับค่าว่าง
        {
            def categoryObj = categoryList.get(0);
            this.categoryId = categoryObj.get("category_id");
            this.name = categoryObj.get("name");
        }
    }

    void updateCategory()
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        String sqlText = "UPDATE category SET name = '${this.name}' WHERE category_id = ${this.categoryId}"
        sql.execute(sqlText)
        sql.close()
    }

    void updateCategoryById(Integer pCategoryId,String pName)
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        String sqlText = "UPDATE category SET name = '${pName}' WHERE category_id = ${pCategoryId}"
        sql.execute(sqlText)
        sql.close()
    }

    void createCategory()
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        String sqlText = "INSERT INTO category (name) VALUES ('${this.name}')"
        sql.execute(sqlText)
        sql.close()
    }

    void createCategoryByName(String categoryName)
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        String sqlText = "INSERT INTO category (name) VALUES ('${categoryName}')"
        sql.execute(sqlText)
        sql.close()
    }

    void deleteCategory()
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        String sqlText = "DELETE FROM category WHERE category_id = ${this.categoryId}"
        sql.execute(sqlText)
        sql.close()
    }

    void deleteCategoryById(Integer pCategoryId)
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        String sqlText = "DELETE FROM category WHERE category_id = ${pCategoryId}"
        sql.execute(sqlText)
        sql.close()
    }
}
