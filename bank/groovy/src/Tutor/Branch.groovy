package Tutor

import groovy.sql.Sql

// def sql = Sql.newInstance(dbUrl,dbUser,dbpassword,dbDriverClassName)
/**
 * Created by Bankinginverter on 6/28/2017.
 */
class Branch {
    int branchID;
    String branchName;
    String addressBranch;
    String dbUrl = 'jdbc:postgresql://localhost:5432/Tutor' ;
    String dbUser = 'postgres';
    String dbpassword = 'postgres';
    String dbDriverClassName = 'org.postgresql.Driver';

    List branchList(){
        def sql = Sql.newInstance(dbUrl,dbUser,dbpassword,dbDriverClassName)
        def branchList = sql.rows("SELECT * FROM branch")
        sql.close()
        return branchList;

    }


    // Method getBrancbyID ทำหน้าที่ Get ค่าให้กับ Properties
    void getBracnhbyID(int pGetBranchById){
        Sql sql = Sql.newInstance(dbUrl,dbUser,dbpassword,dbDriverClassName) // สร้าง instance เพื่อติดต่อกับ Data base
        def branchIdList = sql.rows("SELECT branch_id,name FROM branch WHERE branch_id = ${pGetBranchById}") // SELECT branch_id และ name จาก ตาราง branch แล้วกำหนดเงื่อนไขว่า ให้ branch_id = ${pGetBranchById}
        sql.close() // ปิด Data base
        if(!branchIdList.isEmpty()){ // กำหนดเงื่อนไขว่า ถ้า branchList ไม่เท่ากับค่าว่าง ให้มาทำในเงื่อนไขนี้
            def branchObj = branchIdList.get(0) // สร้างตัวแปร มารับค่าจาก branchList
            this.branchID = branchObj.get("branch_id") // ให้ตัวแปร branchID ที่อยู่ใน properties มีค่าเท่ากับ branchObj.get("branch_id")
            this.branchName = branchObj.get("name") // ให้ตัวแปร branchName ที่อยู่ใน properties มีค่าเท่ากับ branchObj.get("name")
        }
    }

    void addBranchName(){  // เราสามารถ get จากข้างนอกก็ได้ โดยผ่าน Properties หรือไม่ก็ ตั้ง Parameter ไว้ข้างใน แล้ว Set ผ่าน Parameter ก็ได้
        def sql = Sql.newInstance(dbUrl,dbUser,dbpassword,dbDriverClassName)
        String sqlText = "INSERT INTO branch (name,address_branch,telephone) VALUES ('${this.branchName}','${this.addressBranch}','${this.telephone}')"
        sql.execute(sqlText)
        sql.close()
    }

    void updateBranchData(){ // Update จะใช้ก็ต่อเมื่อ ข้อมูลหลังจากที่มีการ เพิ่ม หรือ ลบ ค่าออกจาก DataBase นั้นๆแล้ว
        def sql = Sql.newInstance(dbUrl,dbUser,dbpassword,dbDriverClassName)
        String sqlText = "UPDATE branch SET name = '${this.branchName}' WHERE branch_id = ${this.branchID}"
        sql.execute(sqlText)
        sql.close()
    }

    // Method countBranchCustomer ทำหน้าที่เช็คว่า สาขานี้มี นร ทั้งหมดกี่คน
    int countBranchInCustomer(){
        def sql = Sql.newInstance(dbUrl,dbUser,dbpassword,dbDriverClassName)
        String sqlText = $/
            SELECT count(b.branch_id)FROM branch b
            INNER JOIN category ON b.branch_id = category.branch_id
            INNER JOIN registeritem ON category.category_id = registeritem.caterogy_id
            INNER JOIN register ON registeritem.register_id = register.register_id
            INNER JOIN customer ON register.customer_id = customer.customer_id
            WHERE b.branch_id = ${branchID}
        /$
        def branchList = sql.rows(sqlText)
        sql.close()
        def brachObj = [:]
        if(!branchList.isEmpty()){
            brachObj = branchList.get(0)
        }
         return brachObj.get("count")
    }

    int countBranchInSubject(){ // Method นี้เช็คว่า 1 สาขา มีกี่วิชาที่เปิดสอน
        def sql = Sql.newInstance(dbUrl,dbUser,dbpassword,dbDriverClassName)
        String sqlText = $/
            SELECT count(b.branch_id) FROM branch b
            INNER JOIN category ON b.branch_id = category.branch_id
            INNER JOIN subject ON category.subject_id = subject.subject_id
            WHERE b.branch_id = ${this.branchID}
        /$
        def branchList = sql.rows(sqlText)
        sql.close()
        def branchObj = [:]
        if(!branchList.isEmpty()){
            branchObj = branchList.get(0)
        }
        return branchObj.get("count")
    }

    List countBranchInGrade(){  //Method นี้เช็คว่า 1 สาขาจะมีเด็กในแต่ละช่วงฉันกี่คน
         def sql = Sql.newInstance(dbUrl,dbUser,dbpassword,dbDriverClassName)
        String sqlText = $/
            SELECT count(b.branch_id) FROM branch b
            INNER JOIN category ON b.branch_id = category_id
            INNER JOIN grade ON category.grade_id = grade.grade_id
            WHERE grade.grade_id = ${this.branchID}
        /$
        def branchList = sql.rows(sqlText)
        print(branchList)
        sql.close()
        def branchObj = [:]
        if(!branchList.isEmpty()){
            branchObj = branchList.get(0)
        }
        return branchList
    }
}
