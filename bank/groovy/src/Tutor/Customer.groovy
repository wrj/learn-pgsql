package Tutor

import groovy.sql.Sql

/**
 * Created by Bankinginverter on 6/28/2017.
 */
class Customer {
    int customerId;                                         // }
    int subjectId;                                          // |
    int nameTitleId;                                        // |
    String firstName;                                       // |
    String LastName;                                         //  ======} Properties ของคลาส
    String subjectName;
    String dbUrl = 'jdbc:postgresql://localhost:5432/Tutor' // |
    String dbUser = 'postgres'                              // |
    String dbpassword = 'postgres'                          // |
    String dbDriverClassName = 'org.postgresql.Driver'      // }

    void getCustomerByID(int pGetCustomerID) {     // Method นี้เอาไว้ get ค่าให้กับ Method countCustomerInSubject
        def sql = Sql.newInstance(dbUrl, dbUser, dbpassword, dbDriverClassName)
        // ติดต่อกับ Database โดยในที่นี้ประกาศแทนด้วยชื่อตัวแปรไว้แล้ว
        def getCustomByIDList = sql.rows("SELECT c.customer_id,c.nametitle_id,c.first_name,c.last_name,c.tell_id,c.address_id FROM customer c WHERE customer_id = ${pGetCustomerID}")
        // ประกาศ List ขึ้นมาเพื่อที่จะเก็บ Database
        sql.close() // ปิด DataBase
        if (!getCustomByIDList.isEmpty()) { //ถ้า getCustomerByIDList ไม่เท่ากับ ค่า ว่าง ให้เข้ามาทำเงื่อนไขข้างใน
            def getCustomerObj = getCustomByIDList.get(0)
            // ประกาศ List ขึ้นมา ตัวนึง ให้ = getCustomerByIDList ใน Index ที่ 0
            this.customerId = getCustomerObj.get("customer_id")
            // customerID ใน Properties = getCustomer.get ในฟิลด์ ที่ชื่อว่า customer_id
            this.firstName = getCustomerObj.get("first_name")
            // firsterName ใน Properties = getCustomer.get ในฟิลด์ ที่ชื่อว่า first_name
        }
    }

    int countCustomerInSubject() {
        def sql = Sql.newInstance(dbUrl, dbUser, dbpassword, dbDriverClassName) //ติดต่อ Database
        String sqlText = $/    
            SELECT count(r.register_id)  FROM register r     
                INNER JOIN registeritem ON r.register_id = registeritem.register_id
                INNER JOIN category ON registeritem.caterogy_id = category.category_id
                INNER JOIN subject ON category.subject_id = subject.subject_id
                WHERE r.customer_id = ${customerId}
        /$  // เลือก Database จากตาราง register แล้วทำการ Join registeritem ไปถึง subject
        def subjectList = sql.rows(sqlText)  // สามารถประกาศ เป็น String ไว้เก็บ database แล้วค่อยเอามาใส่ก็ได้
        sql.close() // ปิด Database
        def subjectObj = [:] // ประกาศ Map ขึ้นมา
        if (!subjectList.isEmpty()) { // เช็คว่า subjectList ว่างหรือ ไม่
            subjectObj = subjectList.get(0)  // ให้ SubjectObj = SubjectList ใน Index ช่องที่ 0
        }
        return subjectObj.get("count"); // return ค่ากับสู่ subjectObj ที่ get ค่า count
    }

    void getSubjectByID(int pGetSubjectID) {
        def sql = Sql.newInstance(dbUrl, dbUser, dbpassword, dbDriverClassName)
        def getSubjectByIDList = sql.rows("SELECT s.subject_id,s.name FROM subject s  WHERE subject_id = ${pGetSubjectID}")
        sql.close()
        if (!getSubjectByIDList.isEmpty()) {
            def getSubjectObj = getSubjectByIDList.get(0)
            this.subjectId = getSubjectObj.get("subject_id")
            this.subjectName = getSubjectObj.get("name")
        }
    }
    int countSubjectInCustomer() {
        def sql = Sql.newInstance(dbUrl, dbUser, dbpassword, dbDriverClassName)
        String sqlText = $/
            SELECT count(s.subject_id) FROM subject s
                INNER JOIN category ON s.subject_id = category.subject_id
                INNER JOIN registeritem ON category.category_id = registeritem.caterogy_id
                INNER JOIN register ON registeritem.register_id = register.register_id
                INNER JOIN customer ON register.customer_id = customer.customer_id
                WHERE s.subject_id = ${subjectId}
        /$
        def customerList = sql.rows(sqlText)
        sql.close()
        def customerObj = [:]
        if (!customerList.isEmpty()) {
            customerObj = customerList.get(0)
        }
        return customerObj.get("count");
    }
}
