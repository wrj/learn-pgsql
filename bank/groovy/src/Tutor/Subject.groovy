package Tutor

import groovy.sql.Sql

/**
 * Created by Bankinginverter on 6/27/2017.
 */
class Subject {

    int subjectID;
    String subjectName;
    String dbUrl = 'jdbc:postgresql://localhost:5432/Tutor';
    String dbUser = 'postgres';
    String dbPassword = 'postgres';
    String dbDriverClassName = 'org.postgresql.Driver';

    List subjectList(){
        def sql = Sql.newInstance(dbUrl,dbUser,dbPassword,dbDriverClassName)
        def subjectList = sql.rows("SELECT subject_id,name FROM subject")
        sql.close()
        return subjectList
    }
    void addSubjectName(){ // เพิ่ม ชื่อเข้ามา โดยจะต้อง รับค่ามาจาก Method getSubjectByID ก่อนแล้วจึงจะเรียกใช้ Method นี้ได้
        def sql = Sql.newInstance(dbUrl,dbUser,dbPassword,dbDriverClassName)
        String sqlText = "INSERT INTO subject (name) VALUES ('${this.subjectName}')" //ประกาศตัวแปร String ขึ้นมาตัวนึง เพื่อรับค่าการ คำสั่ง Query มา **ในทีนี้ใช้คำสั่ง Insert จากตาราง Subject โดยเลือก ฟิลด์ที่ชื่อว่า Name แล้วใส่ค่าลงไป โดยค่าที่ใส่จะเท่ากับตัวแปร this.subjectname ซึ่งเป็น Properties ของคลาสนี้**
        sql.execute(sqlText) // ใช้คำสั่ง execute เพื่อให้ sqlText ทำงาน
        sql.close() // ปิด DataBase
    }
    void removeSubjectID(){ // ลบ id ออกจากตาราง โดยจะต้อง รับค่ามาจาก Method getSubjectByID ก่อนแล้วจึงจะเรียกใช้ Method นี้ได้
        def sql = Sql.newInstance(dbUrl,dbUser,dbPassword,dbDriverClassName)
        String sqlText = "DELETE FROM subject WHERE subject_id = ${this.subjectID}" //ประกาศตัวแปร String ขึ้นมาตัวนึง เพื่อรับค่าการ คำสั่ง Query มา **ในทีนี้ใช้คำสั่ง DELETE จากตาราง Subject โดยเลือกฟิลด์ที่ชื่อว่า subject_id แล้วกำหนดเงื่อนไขว่า ให้ ค่าในฟิลด์ subject_id ต้อง = this.subjectID
        sql.execute(sqlText) // ใช้คำสั่ง execute เพื่อให้ sqlText ทำงาน
        sql.close()// ปิด DataBase
    }
    void updateSubject(){ // Update จะใช้ก็ต่อเมื่อ ข้อมูลหลังจากที่มีการ เพิ่ม หรือ ลบ ค่าออกจาก DataBase นั้นๆแล้ว
        def sql = Sql.newInstance(dbUrl,dbUser,dbPassword,dbDriverClassName)
        String sqlText = "UPDATE subject SET name = '${this.subjectName}' WHERE subject_id = ${this.subjectID}" //ประกาศตัวแปร String ขึ้นมาตัวนึง เพื่อรับค่าการ คำสั่ง Query มา **ในทีนี้ใช้คำสั่ง Update โดยเลือก ฟิลด์ที่ชื่อว่า Name แล้วใส่ค่าลงไป โดยค่าที่ใส่จะเท่ากับตัวแปร this.subjectname แล้วกำหนดเงื่อนไขว่า ให้ ค่าในฟิลด์ subject_id ต้อง = this.subjectID ถ้าไม่ตรงกันจะไม่ทำงาน
        sql.execute(sqlText) // ใช้คำสั่ง execute เพื่อให้ sqlText ทำงาน
        sql.close() // ปิด DataBase
    }
    void getSubjectByID(int getID){
        def sql = Sql.newInstance(dbUrl,dbUser,dbPassword,dbDriverClassName)
        def subjectIdList = sql.rows("SELECT subject_id,name FROM subject WHERE subject_id = ${getID}")
        sql.close()
        if(!subjectIdList.isEmpty()){
            def subjectObj = subjectIdList.get(0)
            this.subjectID = subjectObj.get("subject_id")
            this.subjectName = subjectObj.get("name")
        }
    }


    int getSubjectInCustomer(){
        def sql = Sql.newInstance(dbUrl,dbUser,dbPassword,dbDriverClassName)
        String sqlText = $/
                        SELECT count(r.register_id)  FROM register r     
                INNER JOIN registeritem ON r.register_id = registeritem.register_id
                INNER JOIN category ON registeritem.caterogy_id = category.category_id
                INNER JOIN subject ON category.subject_id = subject.subject_id
                WHERE r.customer_id = ${this.subjectID}
                /$
        def subjectList = sql.rows(sqlText)
        sql.close()
        def subjectObj = [:]
        if(!subjectList.isEmpty()){
            subjectObj = subjectList.get(0)
        }
        return subjectObj.get("count")
    }

//Method หลักๆที่ควรจะมี ใน Database นั้น คือ Insert Update Create Delete ส่วนการ Select นั้น ขึ้นอยู่กับความต้องการของงานว่าจะต้อง Select อะไรขึ้นมาดูบ้าง


}
