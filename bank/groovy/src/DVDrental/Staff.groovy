package DVDrental

import groovy.sql.Sql

/**
 * Created by Bankinginverter on 6/27/2017.
 */
class Staff {
    int staffId;
    String firstName;
    String lastName;
    int addressId;
    String email;
    int storeId
    boolean active;

    void getStaffByID(int pStaffId){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental', 'postgres', 'postgres', 'org.postgresql.Driver')
        def staffList = sql.rows("SELECT staff_id,first_name,last_name,address_id,email,store_id,active FROM staff WHERE staff_id = ${pStaffId}");
        sql.close()
        if(!staffList.isEmpty()){
            def staffObj = staffList.get(0)
            this.staffId = staffObj.get("staff_id")
            this.firstName = staffObj.get("first_name")
            this.lastName = staffObj.get("last_name")
            this.addressId = staffObj.get("address_id")
            this.email = staffObj.get("email")
            this.storeId = staffObj.get("store_id")
            this.active = staffObj.get("active")
        }

    }
    def getAddress(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental', 'postgres', 'postgres', 'org.postgresql.Driver')
        def addressList = sql.rows("SELECT a.address_id,a.address,a.address2,a.district,a.city_id FROM address a INNER JOIN city ON a.city_id = city.city_id WHERE a.address_id = ${this.addressId}")
        sql.close()
        def addressObj = [:]
        if(!addressList.isEmpty()){
           addressObj = addressList.get(0)
        }
        return addressObj
    }
    int getPaymentCount(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental', 'postgres', 'postgres', 'org.postgresql.Driver')
        def paymentList = sql.rows("SELECT count(payment_id) as count FROM payment WHERE staff_id = ${this.staffId}")
        sql.close()
        def paymentObj;
        paymentObj = paymentList.get(0)
        return paymentObj.get("count")
    }
    double getPaymentTotal(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental', 'postgres', 'postgres', 'org.postgresql.Driver')
        def paymentTotal = sql.rows("SELECT sum(amount) as sum FROM payment WHERE staff_id = ${this.staffId}")
        sql.close()
        def paymentTotalObj = paymentTotal.get(0)
        return paymentTotalObj.get("sum")
    }

}
