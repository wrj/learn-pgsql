package pokemon

class Category {
    String name
    Date date

    static belongsTo = [branch:Branch]
    static hasMany = [item:Item]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'seq_category_id']
    }
}
