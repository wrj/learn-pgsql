package pokemon

class Item {
    String name
    Double price
    Date date

    static belongsTo = [category:Category]
        static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'seq_item_id']
    }
}
