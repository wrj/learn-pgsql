package pokemon

class Branch {
    String name
    Date date

    static hasMany = [category:Category]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'seq_branch_id']
    }
}
