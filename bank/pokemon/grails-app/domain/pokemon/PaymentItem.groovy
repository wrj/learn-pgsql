package pokemon

class PaymentItem {

    static belongsTo = [item:Item,payment:Payment]
    static constraints = {
    }
    static mapping = {
        id generator:'sequence',params: [sequence:'seq_payment_item_id']
    }
}
