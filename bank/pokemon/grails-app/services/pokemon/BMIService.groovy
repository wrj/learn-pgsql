package pokemon

import grails.transaction.Transactional
import org.grails.datastore.mapping.query.Query.In

@Transactional
class BMIService {

    Integer nowBC = 2017
    Integer nowBE = 2560
    Integer age
    def serviceMethod() {

    }
    def Birthday(Integer choose,Integer date,Integer month, Integer year){

        if(choose == 1){
            println("This is B.C")
            year -= 543
            age = nowBC - year
            println("${date},${month},${year}")
            println("Age: ${age}")
        }
        if(choose == 2){
            println("This is B.E")
            year += 543
            age = nowBE - year
            println("${date},${month},${year}")
            println("Age: ${age}")
        }
    }

    def area(Integer height,Integer width,Integer tall){
        Integer areaRectangle = height * width
        Integer taiTall = tall - height
        Integer areaTai = 0.5 * width * taiTall
        println("RecTangle = ${areaRectangle}")
        println("TaiAngle = ${areaTai}")
        println("ALLArea = ${areaRectangle + areaTai}")
    }
}
