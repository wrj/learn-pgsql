package pokemon

import grails.converters.JSON

class ItemBController {
    def BMIService

    def index() {
//        BMIService.Birthday(1,13,2,2539)
          BMIService.area(6,8,12)
    }
    def create(){
        Item i = new Item()
        Category c = Category.get(1)
        i.setName("PokeBall")
        i.setPrice(200)
        i.setCategory(c)
        i.setDate(new Date())
        if(!i.save()){
            i.errors.allErrors.each {
                print(it)
            }
        }
        else {
            i.save()
        }
        Item i2 = new Item()
        Category c2 = Category.get(1)
        i2.setName("GreatBall")
        i2.setPrice(600)
        i2.setCategory(c2)
        i2.setDate(new Date())
        if(!i2.save()){
            i2.errors.allErrors.each {
                print(it)
            }
        }
        else {
            i2.save()
        }
        Item i3 = new Item()
        Category c3 = Category.get(1)
        i3.setName("UltraBall")
        i3.setPrice(1200)
        i3.setCategory(c3)
        i3.setDate(new Date())
        if(!i3.save()){
            i3.errors.allErrors.each {
                print(it)
            }
        }
        else {
            i3.save()
        }
        Item i4 = new Item()
        Category c4 = Category.get(2)
        i4.setName("Potion")
        i4.setPrice(200)
        i4.setCategory(c4)
        i4.setDate(new Date())
        if(!i4.save()){
            i4.errors.allErrors.each {
                print(it)
            }
        }
        else {
            i4.save()
        }
        Item i5 = new Item()
        Category c5 = Category.get(2)
        i5.setName("SuperPotion")
        i5.setPrice(600)
        i5.setCategory(c5)
        i5.setDate(new Date())
        if(!i5.save()){
            i5.errors.allErrors.each {
                print(it)
            }
        }
        else {
            i5.save()
        }
        Item i6 = new Item()
        Category c6 = Category.get(2)
        i6.setName("HyperPotion")
        i6.setPrice(1200)
        i6.setCategory(c6)
        i6.setDate(new Date())
        if(!i6.save()){
            i6.errors.allErrors.each {
                print(it)
            }
        }
        else {
            i6.save()
        }
        Item i7 = new Item()
        Category c7 = Category.get(2)
        i7.setName("MaxPotion")
        i7.setPrice(2400)
        i7.setCategory(c7)
        i7.setDate(new Date())
        if(!i7.save()){
            i7.errors.allErrors.each {
                print(it)
            }
        }
        else {
            i7.save()
        }
        Item i8 = new Item()
        Category c8 = Category.get(5)
        i8.setName("EscapeRope")
        i8.setPrice(550)
        i8.setCategory(c8)
        i8.setDate(new Date())
        if(!i8.save()){
            i8.errors.allErrors.each {
                print(it)
            }
        }
        else {
            i8.save()
        }
        Item i9 = new Item()
        Category c9 = Category.get(4)
        i9.setName("TM28")
        i9.setPrice(2800)
        i9.setCategory(c9)
        i9.setDate(new Date())
        if(!i9.save()){
            i9.errors.allErrors.each {
                print(it)
            }
        }
        else {
            i9.save()
        }
        Item iX = new Item()
        Category cX = Category.get(4)
        iX.setName("TM16")
        iX.setPrice(5000)
        iX.setCategory(cX)
        iX.setDate(new Date())
        if(!iX.save()){
            iX.errors.allErrors.each {
                print(it)
            }
        }
        else {
            iX.save()
        }

    }
    def showItem(){        // Show Item ว่า มี อะไรบ้าง
        def i = Item.list()
        List summaryTotal = []
        double total = 0
        i.each {
            def summaryTotalMap = [:]
            summaryTotalMap.put("ItemName : ",it.getName())
            summaryTotalMap.put("price",it.getPrice())
            total += it.getPrice()
            summaryTotal.add(summaryTotalMap)
        }
        println("Total = "+total)
        summaryTotal.add("Total :"+total)
        render summaryTotal as JSON
    }

}
