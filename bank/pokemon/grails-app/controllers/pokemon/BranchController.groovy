package pokemon

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BranchController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Branch.list(params), model:[branchCount: Branch.count()]
    }

    def show(Branch branch) {
        respond branch
    }

    def create() {
        respond new Branch(params)
    }

    @Transactional
    def save(Branch branch) {
        if (branch == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (branch.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond branch.errors, view:'create'
            return
        }

        branch.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'branch.label', default: 'Branch'), branch.id])
                redirect branch
            }
            '*' { respond branch, [status: CREATED] }
        }
    }

    def edit(Branch branch) {
        respond branch
    }

    @Transactional
    def update(Branch branch) {
        if (branch == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (branch.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond branch.errors, view:'edit'
            return
        }

        branch.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'branch.label', default: 'Branch'), branch.id])
                redirect branch
            }
            '*'{ respond branch, [status: OK] }
        }
    }

    @Transactional
    def delete(Branch branch) {

        if (branch == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        branch.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'branch.label', default: 'Branch'), branch.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'branch.label', default: 'Branch'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
