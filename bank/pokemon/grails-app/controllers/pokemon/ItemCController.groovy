package pokemon

class ItemCController {

    def index() {
        println(params)
        List c = Category.list()
        [category: c]
    }

    def create() {
        Item item = new Item(params)
        item.setDate(new Date())
        Category c = Category.get(params.category)
        item.setCategory(c)
        println(params)
        if (!item.save()) {
            item.errors.allErrors.each {
                println(it)
            }
        }
    }

    def edit() {
        Category category = Category.get(params.id)
        List c = Category.list()
        [category: c]
        println(params)
    }

    def update() {
        Category category = Category.get(params.id)
        category.setName(params.name)
        if (!category.save()) {
            category.errors.allErrors.each {
                println(it)
            }
        }
        redirect(actionName: "index")
    }
}