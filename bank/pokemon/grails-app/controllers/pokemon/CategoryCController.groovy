package pokemon

class CategoryCController {

    def index() {
        String search = ""
        Integer page = 1
        if(params.get("search"))
        {
            search = params.search
        }
        if(params.get("page"))
        {
            page = params.page.toInteger()
        }
        Integer limit = 2
        Integer offset = (page - 1) * limit
        println offset
        List b = Branch.list()
        List c = Category.findAllByNameIlike("%${search}%",[max:limit,offset:offset])
        List cTotal = Category.findAllByNameIlike("%${search}%")
        Integer countPage = Math.ceil(cTotal.size()/limit)
        [branch:b,category:c,search:search,page:page,countPage:countPage]
    }
    def create(){
        Category category = new Category(params)
        category.setDate(new Date())
        Branch b = Branch.get(params.branch)
        category.setBranch(b)
        println(params)
        if(!category.save()){
            category.errors.allErrors.each {
                println(it)
            }
        }
    }
    def edit(){
        Category category = Category.get(params.id)
        List b = Branch.list()
        [category:category,branch:b]
    }

    def update(){
        println params
        Category category = Category.get(params.id)
        category.setName(params.name)
        if(!category.save()){
            category.errors.allErrors.each {
                println(it)
            }
        }
     redirect(actionName:"index")
    }
    def delete(){
        Category category = Category.get(params.id)
        category.delete()
    }
}
