package pokemon

import grails.converters.JSON

class CategoryBController {

    def index() {

    }
    def create(){
        Category c = new Category()
        Branch b = Branch.get(2)
        c.setName("PokeBall")
        c.setBranch(b)
        c.setDate(new Date())
        if(!c.save()){
            c.errors.allErrors.each {
                print(it)
            }
        }
        else{
            c.save()
        }
        Category c2 = new Category()
        Branch b2 = Branch.get(1)
        c2.setName("Potion")
        c2.setBranch(b2)
        c2.setDate(new Date())
        if(!c2.save()){
            c2.errors.allErrors.each {
                print(it)
            }
        }
        else{
            c2.save()
        }
        Category c3 = new Category()
        Branch b3 = Branch.get(3)
        c3.setName("Stone")
        c3.setBranch(b3)
        c3.setDate(new Date())
        if(!c3.save()){
            c3.errors.allErrors.each {
                print(it)
            }
        }
        else{
            c3.save()
        }
        Category c4 = new Category()
        Branch b4 = Branch.get(3)
        c4.setName("TMs")
        c4.setBranch(b4)
        c4.setDate(new Date())
        if(!c4.save()){
            c4.errors.allErrors.each {
                print(it)
            }
        }
        else{
            c4.save()
        }
        Category c5 = new Category()
        Branch b5 = Branch.get(1)
        c5.setName("Equipmentd")
        c5.setBranch(b5)
        c5.setDate(new Date())
        if(!c5.save()){
            c5.errors.allErrors.each {
                print(it)
            }
        }
        else{
            c5.save()
        }
    }
    def checkItemInCategory(){
        Category c = Category.get(1)
        def i = c.getItem()
        i.each {
            println(it.getName()+" = "+it.getPrice())
        }
    }
    def checkCategoryInBranch(){
        Branch b = Branch.get(1)
        def c = b.getCategory()
        c.each {
            println(it.getName())
        }
    }
    def checkBranchInCategory(){
        Category c = Category.get(1)
        def b = c.getBranch()
        b.each {
            println(it.getName())
        }
    }
    def jsonRenderCategoryDetail(){  // เก็บรายระเอียดของ Category ให้ออกมาเป็นรูปแบบของ Json
        def c = Category.getAll() // สร้าง Instance มารับค่าจาก categoryB
        List summary = [] //สร้าง List เก็บ map
        c.each {
            def mapJ = [:] // สร้าง map เอาไว้เก็บข้อมูลของ Category 
            mapJ.put("id",it.getId())
            mapJ.put("date",it.getDate())
            mapJ.put("Category",it.getName())
            def branch = []
            def b = it.getBranch()

            mapJ.put("Branch",b)
            summary.add(mapJ)
        }
        render summary as JSON
    }
}
