<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        Title
    </title>
    <asset:stylesheet src="bootstrap.css"/>
</head>
<body>
<g:form action="create">
    <div class="form-group">
        <label for="exampleInputName">CategoryName</label>
        <input type="input" class="form-control" name="name" id="exampleInputName" placeholder="Name">
    </div>
    <div class="form-group">
        <select name="branch">
            <g:each in="${branch}">
                <option value="${it.getId()}">${it.getName()}</option>
            </g:each>
        </select>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</g:form>
<div class="container">
    <table class="table table-bordered">
        <tr>
            <td>
                <g:form action="index" method="GET">
                    <input type="text" name="search" value="${search}">
                    <input type="submit" value="Search">
                </g:form></td>
            <td></td>
        </tr>
        <g:each in="${category}">
            <tr>
                <td>${it.getName()}</td>
                <td><a href="<g:createLink action="edit" id="${it.getId()}"/>">Edit</a></td>
            </tr>
        </g:each>
        <tr>
            <td>
                <ul>
                    <g:each var="i" in="${ (0..< countPage) }">
                        <li><a href="<g:createLink action="index" params="[search:search,page:i+1]"/>">${i+1}</a></li>
                    </g:each>
                </ul>
            </td>
            <td></td>
        </tr>
    </table>
</div>
</body>
</html>
