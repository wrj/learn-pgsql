<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        Title
    </title>
    <asset:stylesheet src="bootstrap.css"/>
</head>

<body>
<g:form action="update" id="${category.getId()}" method="POST">
    <div class="form-group">
        <label for="exampleInputName">CategoryName</label>
        <input type="input" class="form-control" name="name" id="exampleInputName" placeholder="Name" value="${category.getName()}">
    </div>
    <div class="form-group">
        <label for="branch">Branch</label>
        <select name="branch" id="branch">
            <g:each in="${branch}">
                <option value="${it.getId()}" <g:if test = "${it.getId() == category.getBranchId()}">selected</g:if> >${it.getName()}</option>
            </g:each>
        </select>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</g:form>
</body>
</html>
