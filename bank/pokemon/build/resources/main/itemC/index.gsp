<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        Title
    </title>
    <asset:stylesheet src="bootstrap.css"/>
</head>
<body>
<g:form action="create">
    <div class="form-group">
        <label for="exampleInputName">ItemName</label>
        <input type="input" class="form-control" name="name" id="exampleInputName" placeholder="Name">
    </div>
    <div class="form-group">
    <label for="exampleInputPrice">Price</label>
    <input type="input" class="form-control" name="price" id="exampleInputPrice" placeholder="Price">
</div>
    %{--<div class="form-group">--}%
        %{--<label for="exampleInputCategory">Catgeroy</label>--}%
        %{--<input type="input" class="form-control" name="category" id="exampleInputCategory" placeholder="Category">--}%
    %{--</div>--}%
    <div class="form-group">
        <select name="category">
            <g:each in="${category}">
                <option value="${it.getId()}">${it.getName()}</option>
            </g:each>
        </select>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</g:form>
</body>
</html>
