#Groovy
1. Method 
    
    *   Static Method จะสามารถเรียกใช้ได้ทันที โดยที่สร้าง Object
        
    *   None Static Method จะเรียกใช้ได้ก็ต่อเมื่อต้องทำการสร้าง Object ก่อน
    
    *   void Method ไม่ต้อง return ค่าคืนให้ Parameters
    
    *   Method Return Values ต้องมีการ return ค่ากลับไปให้ Parameters
     
        **`เนื่องจากว่า Groovy ภาษาที่มีความพิเศษ จึงไม่ต้องใส่ return ก็ได้ แต่ควรจะใส่ไว้ให้ติดเป็นนิสัย`**
        
        
2. Variable หลักๆมีอยู่ 3 ชนิดใหญ่ๆด้วยกัน 

    *   ชนิดที่เป็นตัวเลข int float double ฯลฯ
    
    *   ชนิดที่เป็นข้อความ char String ฯลฯ
    
    *   Boolean มีค่าเป็น จริง กับ เท็จ
    
3. List คือ Array ใน groovy จะใช้สัญลักษณ์ [] ในการบ่งบอกว่า นี่คือ list

    *   การเก็บข้อมูลใน list จะกำหนดค่าลงไปเลยก็ได้ [1,2,3,4,5] หรือจะคำสั่ง add เข้าไปทีหลังก็ได้
        
    *   การเรียกข้อมูลมาใช้นั้นจะต้องอ้างอิงถึงตำแหน่งใน list ซึ่งเรียกว่า Index โดยจะเริ่มจาก 0 ตามลำดับ
        
        Value [1,2,3,4,5]
        
        Index [0,1,2,3,4]
        
        เช่นจะเรียกข้อมูลจาก Index ที่ 2 variablename.get(2) จะได้ค่าออกมาก = 3
           
    *   การลบข้อมูล ทำเช่นเดียวกับการ add แต่เปลี่ยนเป็น remove แทน variablename.remove(2)
     
        ค่าในช่อง Index ที่ 3 จะหายไป
        
4. Map เป็นการการเก็บข้อมูล แบบ Key value จะอยู่ในรูปแบบของ array
 
    *   การเก็บขอมูล จะเก็บในลักษณะดังนี้ ["firstname" : "banking", "lastname" : "inverter"]
        
        [Key:Value]
        
    *   containsKey() เป็นการพิมพ์ชื่อ key ลงไป แล้วเช็คว่ายังมี Key นี้อยู่หรือไม่ จะได้ค่าออกมาเป็น boolean
        
    *   get() เป็นการพิมพ์ชื่อ Key ลงไปแล้วจะแสดง Value ออกมา ถ้าไม่มีจะแสดงค่า null
    
    *   keySet() เอาไว้นับจำนวน Key ว่ามีเท่าไหร่
    
    *   put() คือการเพิ่ม Key value เข้าไปจะไปอยู่ข้างหลังเสมอ
     
    *   size() บอกขนาดใน ของ list ว่ามีเท่าไหร่
    
    *   value() เป็นการพิมพ์ชื่อ value เข้าไปแล้วจะแสดง value ออกมา
    


5.  Loop คือคำสั่งวนซ้ำ มีหลักๆ คือ for/for in/while/each 

    *   for (เริ่มค่าที่เท่าไหร่;กำหนดรอบ;เพิ่มรอบละเท่าไหร่)
        {
        
        }
        
    *   while(กำหนดเงื่อนไขในLoop)
        {
        
        }
       
    *   for in (ชนิดของตัวแปร ชื่อตัวแปร จำนวนรอบ)
        {
        
        }
        
    *   array.each loop นี้นิยมใช้กับ array เพราะจะวนจนคบจำนวนสมาชิกใน array
        {
        
        }
        
    *   break หากเจอคำสั่งนี้จะออกจาก loop ทันที
    
    
6.  Method เกี่ยวกับตัวเลข

    *   การแปลง Type ของ Value
        println(ตัวแปร.ชนิดที่เราจะแปลง());

    *   compareTo การเปรียบเทียบตัวเลข จะมีค่าออกมา 3 ค่า
        
        0 คือ เท่ากัน
        
        1 คือ ตัวแปรที่เราเปรียบเทียบมีค่ามากกว่า
        
        -1 คือ ตัวแปรที่เราเปรียบเทียบมีค่าน้อยกว่า
        
        Integer x = 5;
        
        x.compareTo(5); //0
        
        x.compareTo(4); //1
        
        x.compareTo(6); //-1
    
    *   equals เป็นการเปรียบเทียบว่าเท่ากับตัวแปรตั้งต้นหรือไม่ 
    
        x.equals(5)
         
        ถ้าเท่ากันจะเป็น True ถ้าไม่เท่ากันจะเป็น False
        
    *   toString เป็นการแปลงตัวเลขเป็นสตริง
         
        x.toString()
        
    *   parse เป็นการแปลงสตริง เป็นตัวเลข
    
        ชนิดตัวแปรที่ต้องการ.parseชนิดตัวแปรที่ต้องการ()
        
    *   การปัดเศษทศนิยม มีอยู่ 3 รูปแบบ คือ
    
        *   ceil ปัดขึ้น ไมว่าทศนิยมจุดเท่าไหร่ก็ตาม เช่น 5.2 จะปัดเป็น 6 ทันที
        
        *   floor ปัดลง ไม่ว่าทศนิยมจุดเท่าไหร่ก็ตาม เช่น 5.9 จะเป็น 5 ทันที
        
        *   round ถ้ามากกว่า 5 จะปัดขึ้น ถ้าน้อยกว่า 5 จะปัดลง
    
6/22/2017
#Class
    * การสร้างคลาส
    * การเรียกใช้คลาส แบบ Object
    
#getter and setter Methods
    * การส่งค่ารับค่าภายในคลาส
    * การส่งค่ารับค่ากับคลาสอื่นๆ
    
#Inheritance
    * การสืบทอดคลาสจากคลาสหลัก
#Properties
    * คือตัวแปรที่อยู่ในคลาส

`**ไฟล์แบบฝึกหัดแนบไว้ใน bitbuckget คับผม**`

6/23/2017
#Review Class/get set/Inheritance/Properties
    วันนี้พี่บอลให้ลองทำโจทย์ แต่ยังใช้ OOP ไม่ค่อยคล่องครับ ต้องหัดทำแบบฝึกหัดเยอะครับ


#Intro XML And JSON
    * โครงสร้างของ XML
    
        <root>
        
            <element arttribute>
            
                <tag>
                
                </tag>
                
            </element>
            
        </root>
    
    `**ส่วนใหญ่ arrttribute จะนิยมใส่ใน element**`
    
    * โครงสร้างของ JSON
    
        จะคล้ายๆ กับ Map ของ groovy [{Key:Value}]
        
#Connect DATABASE ด้วย Groovy
    sql.rows ใช้กับ Select เท่านั้น
    sql.excute ใช้กับ Insert update create deate

    ` ยังไม่ค่อยเข้าใจในการเรียกใช้ Data Base ใน Groovy`
    
#โครงสร้างการทำ Json และทบทวน การดึง data base มาใช้
    Syntax
        JsonOutput.toJson(datatype obj)
        เช่น def a = ["Name:Hello","Id:World"]
           def output = JsonOutput.toJson(output)
    
    ทบทวน การดึง DataBase มาใช้ทำให้เข้าใจมกาขึ้นครับ 
    
#ทำแบบฝึกหัดต่อจากเมื่อวาน ใน DataBase ของตนเอง
    ทำ Class and Method ในแต่ละตาราง
    Query หาค่าต่างๆโดยใช้ Class and Method 
    แบบฝึกหัดอยู่ใน Folder Learn postgres คับ
    

    



 