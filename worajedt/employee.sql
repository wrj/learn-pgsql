-- Employee database
-- ให้ทำตามคำสั่งในไฟล์นี้

-- department
-- เพื่อบอกโครงสร้าง แผนก ของบริษัท และความสัมพันธ์

DROP TABLE IF EXISTS department;

CREATE TABLE department (
  dep_id SERIAL NOT NULL PRIMARY KEY ,
  name VARCHAR(50) NOT NULL UNIQUE ,
  parent_id INT DEFAULT 0,
  dep_type_id INT DEFAULT 1,
  status INT DEFAULT 1,
  note VARCHAR(250),
  last_update TIMESTAMP NOT NULL DEFAULT current_timestamp
);

-- department type
-- สำหรับ look up ว่า department นี้เป็นระดับไหน
DROP TABLE IF EXISTS dep_type;

CREATE TABLE dep_type (
  dep_type_id SERIAL NOT NULL PRIMARY KEY ,
  type VARCHAR(50) NOT NULL ,
  level INT DEFAULT 1,
  status INT DEFAULT 1
);

INSERT INTO dep_type (type, level, status) VALUES ('สูงสุด',1,1);
INSERT INTO dep_type (type, level, status) VALUES ('ฝ่าย',2,1);
INSERT INTO dep_type (type, level, status) VALUES ('งาน',3,1);
INSERT INTO dep_type (type, level, status) VALUES ('หน่วย',4,1);
INSERT INTO dep_type (type, level, status) VALUES ('อิสระ',99,1);

-- Create foreign key
ALTER TABLE department
  ADD CONSTRAINT fk_dep_dep_type
FOREIGN KEY (dep_type_id)
REFERENCES dep_type(dep_type_id)
ON UPDATE CASCADE;