# README #

บทเรียนและโครงการสำหรับเรียนรู้ PostgreSQL เพื่อทำงานควบคู่กับ Groovy สำหรับนักศึกษาฝึกงาน ปี 2560

Copyright 2017 - Skoode Skill Co.,Ltd.
CC-BY-SA 3.0

### คำสั่ง ###

* รีวิวโจทย์
* ออกแบบ แล้วเขียนเป็น sql script ไฟล์เดียวพอ
* comment รายละเอียดการออกแบบไว้ใน sql comment ด้วย
* comment field แต่ละ field ด้วยว่า field นี้เอาไว้ทำอะไร
* แยก folder สำหรับแต่ละคนให้แล้ว ดูรายละเอียดเอกสารแต่ละคนได้เลย

__commit ถี่ๆ และเขียน commit log ดีๆ __

### วิธีการใช้งาน intellij + sql ###

![วิธีการใช้งาน](https://img.youtube.com/vi/imhq9wk9nMw/0.jpg)

(https://www.youtube.com/watch?v=imhq9wk9nMw)

Worajedt Sitthidumrong