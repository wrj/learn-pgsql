#Grails + PostgreSQL

##_Config_
  * link การ config
         http://docs.grails.org/latest/guide/conf.html#automaticDatabaseMigration
  
  #### - Application.yml
  * **Datasouce**
     - **driverClassName** เป็นการบอกว่า JDBC driver เราชื่ออะไร
     - **dialect** เป็นภาษาที่ class หรือ String ใช้ติดต่อ database
     - **username**ชื่อ username ในการเชื่อมต่อ
     - **password**ชื่อ password ในการเชื่อมต่อ
  
  
  * **Environment**
      - **dbCreate** คือคำสั่งในจัดการ database
      - **url** คือการบอกว่า database ที่เราจะใช้งานอยู่ที่ไหน
      
  #### - Build.gradle
  
  * **Dependencies**

##_Domain_
  #### - การกำหนดชื่อ table เอง
    - ใช้ properties table ที่ mapping
    static mapping = {
      table 'ชื่อ table'
    }

  #### - id
    - การสร้าง colume id แบบ auto
      static mapping = {
        id  generator: 'sequence',
            column: 'ชื่อ column',
            params: [sequence:'sequence_name']
      }
    - การสร้าง id แบบ composite key
      ใช้ implements Serializable ที่ class
      class CategoriesProduct implements Serializable {
        Categories categories
        Product product
        static mapping = {
          id  composite:['categories','product']
        }
      }

  #### - DataType (บอกประเภทของตัวแปร ว่าเป็นประเภทไหน)
    - String = เก็บได้ทั้งตัวอักษรและตัวเลข
    - Integer = เก็บค่าเป็นตัวเลข
    - Double = เก็บค่าเป็นเลขที่มีจุดทศนิยม
    - Boolean = เก็บค่าที่เป็น true หรือ false
    - Date = เก็บค่าของวัน-เดือน-ปี และเวลา
    - List = เป็นตัวแปรที่สามารถเก็บค่าได้หลายค่ามี index หรือ ลำดับ
    - Map = เป็นตัวแปรที่สามารถเก็บค่าได้หลายค่ามี Key : Values
 
  #### - การทำ auto timestamp ของ วันที่
    - ให้ใช้ Properties ว่า 
    Date dateCreated
    Date lastUpdated

  #### - ยกเลิก version ของ record
    - ใส่ version false ที่ mapping
    static mapping = {
      version false
    }

  #### - Static Contraints (การใส่ค่าให้ domain validate data)
  
  #### - Static Mapping
    - การทำ Column mapping
        static mapping = {
          column name: "picture", sqlType: "bytea" 
        }
        ### เป็นการบอกว่า column ที่จะ map ชื่ออะไร เป็น sqlType ประเภทไหน
  
  #### - Event
  
  #### - Comment
  
  #### - Relation
    - One to One (หนึ่งต่อหนึ่ง ห้ามซ้ำ เช่น คนหนึ่งคนมีบัตรประชาชนได้ใบเดียว)
        class Person {
            String name
        }
        class IdCard {
            static hasOne = [idCard:IdCard]
        }
        
    ------------------------------------------------------------------
        
    - One to Many (เช่น แม่หนึ่งคนมีลูกได้หลายคน)
    
        class Mother {
            String name
            static hasMany = [childrens:Children]
        }
        class Children {
            String name
        }
        
    ------------------------------------------------------------------
       
    - Many to Many (เช่น นักเรียนหลายคนเรียนได้หลายวิชา)
        class Students {
            String name
  
            static hasMany = [subjects:Subjects]
   
        }
        class Subjects {
            static belongTo = Students
            static hasMany = [students:Students]
        }
        
    ------------------------------------------------------------------
        
    - belongTo (เป็นการสร้าง Behaviour ให้ Class)
    
       * One to Many (แม่หนึ่งคนมีลูกได้หลายคน)
             
                 class Mother {
                     String name
                     static hasMany = [childrens:Children]
                 }
                 class Children {
                     String name
                     static belongTo = [mother:Mother]
                 }
            ### บอกให้ Class Mother เป็น Class หลักของ Class Children
       
    
       * Many to Many (นักเรียนหลายคนเรียนได้หลายวิชา)
            class Students {
                String name
      
                static hasMany = [subjects:Subjects]
       
            }
            class Subjects {
                static belongTo = Students
                static hasMany = [students:Students]
            }
            ### บอกให้ Class Students เป็น Class หลักของ Class Subjects
        
    

##_Controller_
  #### - List
    - การสร้าง action เพื่อแสดงข้อมูลทั้งหมด เป็นแบบ list
        เช่น
        def index() {
            List customer = Customers.list  //สร้างตัวแปรเพื่อเก็บค่าของ Class Customers
            customer.each {     // ทำloop เพื่อแสดงข้อมูล
                println (it.getName())  // Print ค่าที่เราต้องการจะแสดง เช่น ต้องการดูชื่อลูกค้าทั้งหมด
            }
        }
  
  #### - Create
    - การสร้าง action เพื่อเพิ่มข้อมูล
        def create() {
                Categories category = new Categories() //สร้างตัวแปรเพื่อเพิ่มข้อมูลลงใน Class Categories
                category.setCategoryName("India Food") //เพื่มข้อมูลลงใน field ที่อยู่ในตัวแปรนั้น
                category.setDescription("Roti")
                category.save() //save ข้อมูลลง database
            }
  #### - Update
    - การสร้าง action เพื่ออัพเดตข้อมูล
        def update() {
            Categories category = Categories.get(2) //สร้างตัวแปรเพื่อให้ Class Categories ไปดึง Id ที่ 2 ออกมา
            category.setCategoryName("Thai Food") // ป้อนข้อมูลใหม่ลงใน field ที่อยู่ในตัวแปรนั้น
            category.setDescription("Tom Yum")
            category.save() //save ข้อมูลลง database
        }
  
  #### - Delete
    - การสร้าง action เพื่อลบข้อมูล
        เช่น
        def delete() {
            Customers customer = Customers.get(1) //สร้างตัวแปรเพื่อให้ Class Customer ไปดึง Id ที่ 1 ออกมา 
            customer.delete()  // แล้วทำการลบข้อมูล
        }
  
  #### - Validate domain
    - การสร้าง Validate ของ Property จะสร้างไว้ที่ Constraint เพื่อ Check Error 
        ## ปกติค่า default จะเป็น nullable: false // ห้ามเป็นค่าว่าง
        
             blank: boolean //ค่าว่าง
             nullable: boolean //ค่า null
             email: boolean // pattern email
             size: range //ค่าความยาวตัวอักษร
             unique: boolean //ห้ามซ้ำ
             max: [integer,date] //ค่ามากสุด
             min: [integer,date] //ค่าน้อยสุด
             range: range //กำหนดค่าเป็นช่วง
    
    
        เช่น
        static constraints = {
            username blank:false,nullable:false //บอกให้ username ห้ามว่าง
        }
        
    - การใช้ Validate เพื่อ check ว่ามี error หรือไม่ [ถ้าไม่มี error จะเป็น True, ถ้ามี error จะเป็น false]  
        
        เช่น
        Customer customer = new Customer()
            customer.setFirstname('firstname')
            if(customer.validate())
            {
                customer.save()
            }else{
                customer.error.allError.each {
                    println (it)
                }
            }
            
            
  #### - Response JSON
  #### - Response XML
  #### - Error Handle
  #### - Event action
  #### - Session
  #### - Flash message
  #### - Alias
  #### - Condition
  #### - Redirect
  
##_Service_
สร้าง Service Class เพื่อใช้งานใน Controller  

##_View_

##_URLMapping_

##_Security_