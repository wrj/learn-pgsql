﻿-- care care database
-- ให้ทำตามคำสั่งในไฟล์นี้

-- ลบตาราง view
DROP VIEW IF EXISTS view_customer;
DROP VIEW IF EXISTS view_branch;
DROP VIEW IF EXISTS view_branch_category_service;
DROP VIEW IF EXISTS view_detail_customer;



-- ลบตาราง table
DROP TABLE IF EXISTS service_category;
DROP TABLE IF EXISTS branch_category;
DROP TABLE IF EXISTS payment_service;
DROP TABLE IF EXISTS payment;
DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS service;
DROP TABLE IF EXISTS branch;
DROP TABLE IF EXISTS customer;




-- สร้าง table category ประเภทบริการ
CREATE TABLE category
(
    category_id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(100) NOT NULL,
    created_at TIMESTAMP DEFAULT now(),
    last_updated TIMESTAMP DEFAULT current_timestamp
);
COMMENT ON COLUMN category.category_id IS 'id ของประเภทบริการ';
COMMENT ON COLUMN category.name IS 'ชื่อประเภทบริการ';
COMMENT ON COLUMN category.created_at IS 'วันที่สร้าง record';
COMMENT ON COLUMN category.last_updated IS 'วันที่ update record';
COMMENT ON TABLE category IS 'ตารางประเภทบริการ';

-- สร้าง table service บริการ
CREATE TABLE service
(
    service_id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(100) NOT NULL,
    small_price NUMERIC,
    medium_price NUMERIC,
    large_price NUMERIC,
    other_price NUMERIC,
    created_at TIMESTAMP DEFAULT now(),
    last_updated TIMESTAMP DEFAULT current_timestamp
);
COMMENT ON COLUMN service.service_id IS 'id ของบริการ';
COMMENT ON COLUMN service.name IS 'ชื่อบริการ';
COMMENT ON COLUMN service.small_price IS 'ราคารถขนาดเล็ก
';
COMMENT ON COLUMN service.medium_price IS 'ราคารถขนาดกลาง';
COMMENT ON COLUMN service.large_price IS 'ราคารถขนาดใหญ่';
COMMENT ON COLUMN service.other_price IS 'ราคารถขนาดอื่นๆ';
COMMENT ON COLUMN service.created_at IS 'วันที่สร้าง record';
COMMENT ON COLUMN service.last_updated IS 'วันที่ update record';
COMMENT ON TABLE service IS 'บริการ';



-- สร้าง table service_category ตางรางเก็บข้อมูลของบริการ กับ ประเภทบริการ
CREATE TABLE service_category
(
    category_id INT NOT NULL,
    service_id INT NOT NULL,
    CONSTRAINT service_category_category_id_service_id_pk PRIMARY KEY (category_id, service_id),
    CONSTRAINT service_category_category_category_id_fk FOREIGN KEY (category_id) REFERENCES category (category_id),
    CONSTRAINT service_category_service_service_id_fk FOREIGN KEY (service_id) REFERENCES service (service_id)
);
COMMENT ON COLUMN service_category.category_id IS 'category id ประเภทบริการ';
COMMENT ON COLUMN service_category.service_id IS 'service id ของบริการ';
COMMENT ON TABLE service_category IS 'ตารางเก็บข้อมูลของบริการ กับ ประเภทบริการ';

-- สร้าง table branch สาขาที่ให้บริการ
CREATE TABLE branch
(
    branch_id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(100) NOT NULL,
    open_time TIME NOT NULL,
    close_time TIME NOT NULL,
    telephone VARCHAR(20) NOT NULL,
    work_phone VARCHAR(20) NOT NULL,
    holiday VARCHAR(100),
    address TEXT NOT NULL,
    status BOOLEAN DEFAULT TRUE,
    created_at TIMESTAMP DEFAULT now(),
    last_updated TIMESTAMP DEFAULT current_timestamp
);
COMMENT ON COLUMN branch.branch_id IS 'id ของสาขาที่ให้บริการ';
COMMENT ON COLUMN branch.name IS 'ชื่อสาขา';
COMMENT ON COLUMN branch.open_time IS 'เวลาเปิดทำการ';
COMMENT ON COLUMN branch.close_time IS 'เวลาปิดทำการ';
COMMENT ON COLUMN branch.telephone IS 'เบอร์มือถือ';
COMMENT ON COLUMN branch.work_phone IS 'เบอร์ที่ทำงาน';
COMMENT ON COLUMN branch.holiday IS 'เก็บวันหยุดของสาขา เช่น ทุกวันพุธ';
COMMENT ON COLUMN branch.address IS 'ที่ตั้งสาขา';
COMMENT ON COLUMN branch.status IS 'สถานะสาขายังเปิดทำการ หรือ ปิดทำการ';
COMMENT ON COLUMN branch.created_at IS 'วันที่สร้าง record';
COMMENT ON COLUMN branch.last_updated IS 'วันที่ update record';
COMMENT ON TABLE branch IS 'สาขาที่ให้บริการ';


-- สร้าง table branch category ตารางเก็บข้อมูลของ สาขา + ประเภทของบริการ มี relation แบบ many to many
CREATE TABLE branch_category
(
    branch_id INT NOT NULL,
    category_id INT NOT NULL,
    CONSTRAINT branch_category_branch_id_category_id_pk PRIMARY KEY (branch_id, category_id),
    CONSTRAINT branch_category_branch_branch_id_fk FOREIGN KEY (branch_id) REFERENCES branch (branch_id),
    CONSTRAINT branch_category_category_category_id_fk FOREIGN KEY (category_id) REFERENCES category (category_id)
);
COMMENT ON COLUMN branch_category.branch_id IS 'branch id ของสาขา';
COMMENT ON COLUMN branch_category.category_id IS 'category id ของประเภทบริการ';
COMMENT ON TABLE branch_category IS 'ตารางเก็บข้อมูลของ สาขา + ประเภทของบริการ';


-- สร้าง table customer ข้อมูลลูกค้า
CREATE TABLE customer
(
    customer_id SERIAL PRIMARY KEY NOT NULL,
    firstname VARCHAR(100) NOT NULL,
    lastname VARCHAR(100) NOT NULL,
    brand VARCHAR(40),
    serie VARCHAR(40),
    license_plate VARCHAR(20),
    color VARCHAR(40),
    name_title VARCHAR(15) NOT NULL,
    email VARCHAR(100),
    telephone VARCHAR(20),
    gender VARCHAR(10) NOT NULL,
    address text NOT NULL,
    created_at TIMESTAMP DEFAULT now(),
    last_updated TIMESTAMP DEFAULT current_timestamp
);
COMMENT ON COLUMN customer.customer_id IS 'id ลุกค้า';
COMMENT ON COLUMN customer.firstname IS 'ชื่อลูกค้า';
COMMENT ON COLUMN customer.lastname IS 'นามสกุลลูกค้า';
COMMENT ON COLUMN customer.color IS 'สีรถ';
COMMENT ON COLUMN customer.brand IS 'ยี่ห้อรถ';
COMMENT ON COLUMN customer.serie IS 'รุ่นรถ';
COMMENT ON COLUMN customer.license_plate IS 'ทะเบียนรถ';
COMMENT ON COLUMN customer.name_title IS 'คำนำหน้าชื่อ';
COMMENT ON COLUMN customer.email IS 'email ลูกค้า';
COMMENT ON COLUMN customer.telephone IS 'เบอร์ติดต่อ';
COMMENT ON COLUMN customer.gender IS 'เพศ';
COMMENT ON COLUMN customer.address IS 'ที่อยู่ของลูกค้า';
COMMENT ON COLUMN customer.created_at IS 'วันที่สร้าง record';
COMMENT ON COLUMN customer.last_updated IS 'วันที่ update record';
COMMENT ON TABLE customer IS 'ลข้อมูลูกค้า';

-- สร้าง table Employee พนักงาน
CREATE TABLE employee
(
    employee_id SERIAL PRIMARY KEY NOT NULL,
    firstname VARCHAR(100) NOT NULL,
    lastname VARCHAR(100) NOT NULL,
    age INT DEFAULT 0,
    gender VARCHAR(10) NOT NULL,
    name_title VARCHAR(10) NOT NULL,
    telephone VARCHAR(20) NOT NULL,
    salary NUMERIC NOT NULL,
    address TEXT NOT NULL,
    email VARCHAR(100),
    branch_id INT NOT NULL,
    created_at TIMESTAMP DEFAULT now(),
    last_updated TIMESTAMP DEFAULT current_timestamp,
    CONSTRAINT employee_branch_id_fk FOREIGN KEY (branch_id) REFERENCES branch (branch_id)
);
CREATE UNIQUE INDEX employee_email_uindex ON public.employee (email);
COMMENT ON COLUMN employee.employee_id IS 'id ของสาขาที่ให้บริการ';
COMMENT ON COLUMN employee.firstname IS 'เวลาเปิดทำการ';
COMMENT ON COLUMN employee.lastname IS 'เวลาปิดทำการ';
COMMENT ON COLUMN employee.age IS 'เบอร์มือถือ';
COMMENT ON COLUMN employee.gender IS 'เบอร์ที่ทำงาน';
COMMENT ON COLUMN employee.name_title IS 'วันหยุด';
COMMENT ON COLUMN employee.telephone IS 'วันหยุด';
COMMENT ON COLUMN employee.salary IS 'เงินเดือน';
COMMENT ON COLUMN employee.address IS 'วันหยุด';
COMMENT ON COLUMN employee.branch_id IS 'ที่ตั้งสาขา';
COMMENT ON COLUMN employee.email IS 'email ของพนักงานครับ (ถ้ามี)';
COMMENT ON COLUMN employee.created_at IS 'วันที่สร้าง record';
COMMENT ON COLUMN employee.last_updated IS 'วันที่ update record';
COMMENT ON TABLE employee IS 'สาขาที่ให้บริการ';


-- สร้าง table payment การจ่ายงเิน
CREATE TABLE payment
(
    payment_id SERIAL PRIMARY KEY NOT NULL,
    customer_id INT NOT NULL,
    branch_id INT NOT NULL,
    employee_id INT NOT NULL,
    total_price NUMERIC NOT NULL,
    service_date date DEFAULT now(),
    created_at TIMESTAMP DEFAULT now(),
    last_updated TIMESTAMP DEFAULT current_timestamp,
    CONSTRAINT customer_customer_id_fk FOREIGN KEY (customer_id) REFERENCES customer (customer_id),
    CONSTRAINT employee_employee_id_fk FOREIGN KEY (employee_id) REFERENCES employee (employee_id),
    CONSTRAINT branch_branch_id_fk FOREIGN KEY (branch_id) REFERENCES branch (branch_id)
);
COMMENT ON COLUMN payment.payment_id IS 'id การจ่ายเงิน';
COMMENT ON COLUMN payment.customer_id IS 'id ลูกค้า';
COMMENT ON COLUMN payment.branch_id IS 'id สาขาที่ให้บริการ';
COMMENT ON COLUMN payment.employee_id IS 'id พนักงานที่ให้บริการ';
COMMENT ON COLUMN payment.total_price IS 'รวมราคายั้งหมด';
COMMENT ON COLUMN payment.service_date IS 'วันที่เข้ามาใช้บริการ';
COMMENT ON COLUMN payment.created_at IS 'วันที่สร้าง record';
COMMENT ON COLUMN payment.last_updated IS 'วันที่ update record';
COMMENT ON TABLE payment IS 'การจ่ายเเงิน';



-- สร้าง table payment service ตารางเก็บข้อมูลของ บริการที่ใช้ + การจ่ายเงิน
CREATE TABLE payment_service
(
    payment_service_id SERIAL PRIMARY KEY NOT NULL,
    service_id INT NOT NULL,
    payment_id INT NOT NULL,
    description TEXT,
    CONSTRAINT payment_service_service_service_id_fk FOREIGN KEY (service_id) REFERENCES service (service_id),
    CONSTRAINT payment_service_payment_payment_id_fk FOREIGN KEY (payment_id) REFERENCES payment (payment_id)
);
COMMENT ON COLUMN payment_service.payment_service_id IS 'id การคิกค่าบริการ';
COMMENT ON COLUMN payment_service.service_id IS 'service id ของกบริการที่ใช้';
COMMENT ON COLUMN payment_service.payment_id IS 'payment id การง่ายเงิน';
COMMENT ON COLUMN payment_service.description IS 'รายละเอียดเพิ่มเติม';
COMMENT ON TABLE payment_service IS 'ตารางเก็บข้อมูลของ บริการที่ใช้ + การจ่ายเงิน';

-- สร้าง view ของ customer (ใช้บริการกี่ครั้ง + ราคาทั้งหมดเท่าไร)
CREATE VIEW view_customer AS
    SELECT
        customer_id,
        firstname,
        (SELECT count(customer_id) FROM payment WHERE payment.customer_id = customer.customer_id),
        (SELECT sum(total_price) FROM payment WHERE payment.customer_id = customer.customer_id)
    FROM customer;

-- สร้าง view ของ branch (แต่ละสาขาออกบิลกี่บิล + ราคาทั้งหมดเท่าไร)
CREATE VIEW view_branch AS
    SELECT
        branch_id,
        name,
        (SELECT count(branch_id) FROM payment WHERE payment.branch_id = branch.branch_id),
        (SELECT sum(total_price) FROM payment WHERE payment.branch_id = branch.branch_id)
    FROM branch;

-- สร้าง view ของ branch category (แต่ละสาขามีประเภทบริการอะไรบ้าง + มีบริการอะไรในแต่ละประเภท)
CREATE VIEW view_brach_category_service AS
    SELECT
        branch.branch_id,
        category.category_id,
        branch.name as branch_name,
        category.name as category_name,
        service.service_id,
        service.name as service_name
    FROM branch
        INNER JOIN branch_category ON branch_category.branch_id = branch.branch_id
        INNER JOIN category ON branch_category.category_id = category.category_id
        INNER JOIN service_category ON service_category.category_id = category.category_id
        INNER JOIN service ON service_category.service_id = service.service_id

-- สร้าง view ของ view detail customer (แสดงรายละเอียดข้อมูลข้อค้า)
CREATE VIEW view_detail_customer as
    SELECT
        concat(customer.name_title,' ',customer.firstname,' ',customer.lastname) as fullname,
        initcap(customer.brand) as brand,
        initcap(customer.serie) as serie,
        customer.license_plate,
        payment_service.service_id,
        service.name
    -- ถ้าหา total price ไม่ต้องจำแนกประเภท
    --   (select sum(payment.total_price) from payment WHERE payment.customer_id = customer.customer_id)
    FROM
        payment
        INNER JOIN customer ON payment.customer_id = customer.customer_id
        INNER JOIN payment_service on payment.payment_id = payment_service.payment_id
        INNER JOIN service on payment_service.service_id = service.service_id;



-- insert branch
INSERT INTO branch (name, open_time, close_time, telephone, work_phone, holiday, address) VALUES ('พระราม 1','08:00','18:00','023456789','023456789','ทุกวันพุธ','831 ถนนพระราม1 วังใหม่ ปทุมวัน กรุงเทพมหานคร 10330');
INSERT INTO branch (name, open_time, close_time, telephone, work_phone, holiday, address) VALUES ('พระราม 2','09:00','20:00','023452396','023456790','ทุกวันพฤหัส','160 ถนนพระราม2 แขวงแสมดำ เขตบางขุนเทียน กรุงเทพมหานคร 10150');
INSERT INTO branch (name, open_time, close_time, telephone, work_phone, holiday, address) VALUES ('พระราม 4','08:00','18:00','023457897','023456412','ทุกวันพุธ',' 479/8 ถนนพระราม4 แขวงพระโขนง เขตคลองเตย กรุงเทพมหานคร 10110');
INSERT INTO branch (name, open_time, close_time, telephone, work_phone, holiday, address) VALUES ('พระราม 6','08:00','18:00','023456782','023456983','ทุกวันพุธ','9 ซอยอารีย์สัมพันธ์ ถนนพระราม6 แขวงสามเสนใน เขตพญาไท กรุงเทพมหานคร 10400');
INSERT INTO branch (name, open_time, close_time, telephone, work_phone, holiday, address) VALUES ('พระราม 9','09:00','19:00','023454861','023456779','ทุกวันอทาทิตย์','88/9 ถนนพระราม9 แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10320');


-- insert employee
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('บุญชู','โชคชัย',20,'male','นาย','0812345678',11000,'224 ถนนพระราม2 แขวงแสมดำ เขตบางขุนเทียน กรุงเทพฯ 10150','boonchu@gmail.com',1);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('บุญทิ้ง','โชคช่วย',22,'male','นาย','081234812',11000,'89 ถนนพระราม9 แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10320','boonting@gmail.com',1);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('คำแก้ว','แก้วคำ',25,'female','นาง','081264887',15000,'7 ซอยอารีย์สัมพันธ์ ถนนพระราม6 แขวงสามเสนใน เขตพญาไท กรุงเทพมหานคร 10400','kumkeaw@gmail.com',1);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('พชร','บุญมี',30,'male','นาย','0818914837',20000,'211/77 ถนนพระราม9 แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10320','patpchara@gmail.com',2);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('พรพรรณ','ดีใจ',25,'female','นาง','0817798612',15000,'11/46 ถนนพระราม2 แขวงแสมดำ เขตบางขุนเทียน กรุงเทพฯ 10150','pornpun@gmail.com',2);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('สมใจ','ยินดี',23,'female','นาง','0812332546',12000,'76 ถนนพระราม1 วังใหม่ ปทุมวัน กรุงเทพมหานคร 10330','somjai@gmail.com',2);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('อำนาจ','แผ่นดิน',27,'male','นาย','0812566589',16000,'100/152 ถนนพระราม2 แขวงแสมดำ เขตบางขุนเทียน กรุงเทพฯ 10150','aumnaj@gmail.com',3);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('กล้าหาญ','มั่งมี',27,'male','นาย','0812550064',16000,'45/65 ซอยอารีย์สัมพันธ์ ถนนพระราม6 แขวงสามเสนใน เขตพญาไท กรุงเทพมหานคร 10400','brave@gmail.com',3);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('สมชาย','ผาสุข',29,'male','นาย','0812511678',18000,'155 ถนนพระราม3 แขวงช่องนนทรี เขตยานนาวา กรุงเทพมหานคร 10120','somchai@gmail.com',3);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('ชัยชนะ','กลางเมือง',22,'male','นาย','081254687',11000,'173 ถนนดินสอ แขวงเสาชิงช้า เขตพระนคร กรุงเทพฯ 10200','win@gmail.com',4);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('เพลินใจ','มีดี',25,'female','นาง','0817986621',15000,'564 ถนนดินสอ แขวงเสาชิงช้า เขตพระนคร กรุงเทพฯ 10200','jaijai@gmail.com',4);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('หมูหวาน','จริงใจ',23,'female','นาง','089444889',12000,'1115 ถนนพระราม3 แขวงช่องนนทรี เขตยานนาวา กรุงเทพมหานคร 10120','piggy@gmail.com',4);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('บุญนาค','ทองแท่ง',29,'male','นาย','081009897',18000,'997 ถนนพระราม2 แขวงแสมดำ เขตบางขุนเทียน กรุงเทพมหานคร 10150','boonnak@gmail.com',5);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('ปูไทย','ปลาทอง',21,'male','นาย','0816668997',10000,'11/46 ถนนพระราม3 แขวงช่องนนทรี เขตยานนาวา กรุงเทพมหานคร 10120','crabthai@gmail.com',5);
INSERT INTO employee (firstname, lastname, age, gender, name_title, telephone, salary, address, email, branch_id) VALUES
    ('ทองคำ','ชูใจ',26,'male','นาย','0827779945',15000,'776/44 ซอยอารีย์สัมพันธ์ ถนนพระราม6 แขวงสามเสนใน เขตพญาไท กรุงเทพมหานคร 10400','golden@gmail.com',5);



-- insert category
INSERT INTO category (name) VALUES ('wash');
INSERT INTO category (name) VALUES ('paint protection');
INSERT INTO category (name) VALUES ('show car shine');


-- INSERT customer
INSERT INTO customer (firstname, lastname, brand, color, serie, license_plate, name_title, email, telephone, gender, address) VALUES
    ('วันทอง','วัยเยาว์','honda', 'ดำ', 'civic', 'กก 999', 'นาง', '','0899984488', 'female','121 ถนนพระราม2 แขวงแสมดำ เขตบางขุนเทียน กรุงเทพมหานคร 10150');
INSERT INTO customer (firstname, lastname, brand, color, serie, license_plate, name_title, email, telephone, gender, address) VALUES
    ('บุญมี','เจริญพร','toyota', 'เทา', 'altis', '2กน 2255', 'นาย', '','0891257786', 'male','49/87 ถนนพระราม3 แขวงช่องนนทรี เขตยานนาวา กรุงเทพมหานคร 10120');



-- insert service
INSERT INTO service (name, small_price, medium_price, large_price, other_price) VALUES ('ล้างรถ','150','150','180','200');
INSERT INTO service (name, small_price, medium_price, large_price, other_price) VALUES ('ล้างห้องเครื่อง','200','220','250','280');
INSERT INTO service (name, small_price, medium_price, large_price, other_price) VALUES ('คลีนเนอร์ แว็กซ์','350','350','400','550');
INSERT INTO service (name, small_price, medium_price, large_price, other_price) VALUES ('ขจัดละอองสี','2000','2500','3000','3500');
INSERT INTO service (name, small_price, medium_price, large_price, other_price) VALUES ('ขจัดคราบแมลง','1000','1500','2000','2500');


-- Insert Service Category
INSERT INTO service_category (category_id, service_id) VALUES (1,1);
INSERT INTO service_category (category_id, service_id) VALUES (2,2);
INSERT INTO service_category (category_id, service_id) VALUES (2,3);
INSERT INTO service_category (category_id, service_id) VALUES (3,4);
INSERT INTO service_category (category_id, service_id) VALUES (3,5);

-- Insert Branch Category

INSERT INTO branch_category (branch_id, category_id) VALUES (1,1);
INSERT INTO branch_category (branch_id, category_id) VALUES (1,2);
INSERT INTO branch_category (branch_id, category_id) VALUES (1,3);
INSERT INTO branch_category (branch_id, category_id) VALUES (2,1);
INSERT INTO branch_category (branch_id, category_id) VALUES (2,2);
INSERT INTO branch_category (branch_id, category_id) VALUES (3,1);
INSERT INTO branch_category (branch_id, category_id) VALUES (3,3);
INSERT INTO branch_category (branch_id, category_id) VALUES (4,2);
INSERT INTO branch_category (branch_id, category_id) VALUES (4,3);
INSERT INTO branch_category (branch_id, category_id) VALUES (5,3);

-- INSERT payment
INSERT INTO payment (customer_id, branch_id, employee_id, total_price,service_date) VALUES (1,5,1,350,'2016-04-02');
INSERT INTO payment (customer_id, branch_id, employee_id, total_price,service_date) VALUES (2,1,10,3250,'2017-06-01');
INSERT INTO payment (customer_id, branch_id, employee_id, total_price,service_date) VALUES (1,2,5,2150,'2017-03-05');
INSERT INTO payment (customer_id, branch_id, employee_id, total_price,service_date) VALUES (2,4,6,3000,'2017-08-20');
INSERT INTO payment (customer_id, branch_id, employee_id, total_price,service_date) VALUES (2,5,14,3550,'2017-10-02');

-- INSERT payment service
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (3,1,'คลีนเนอร์ แว็กซ์');
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (2,2,'ล้างห้องเครื่อง');
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (4,2,'ขจัดละอองสี');
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (1,3,'ล้างรถ');
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (4,3,'ขจัดละอองสี');
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (4,4,'ขจัดละอองสี');
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (2,5,'ล้างห้องเครื่อง');
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (3,5,'คลีนเนอร์ แว็กซ์');
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (4,5,'ขจัดละอองสี');
INSERT INTO payment_service (service_id, payment_id, description) VALUES  (5,5,'ขจัดคราบแมลง');


