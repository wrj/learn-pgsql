วันนี้พี่บอลให้ใช้ grails สร้าง Project ของตัวเองขึ้นมา และใช้คำสั่งต่างในการทำงาน

- สร้าง Domain และ Controller
- การ Insert
- การ Update
- การ Delete

- การทำ Relational
  - One to One
  - One to Many
  - Many to Many
  - Belong To
    - เป็นการบอก Foriegn key ตัวนี้ใช้ของ Domain อะไร (ถ้าลบตัวหลักตัวลูกก็จะโดนลบไปด้วย)

- การใช้ init ทำงาน
  - สามารถสร้างข้อมูลในนั้นได้เลยไม่ต้องสลับ Controller ไปมา


ไฟล์ Project ชื่อ CarcareGrails