package mkgrails

import grails.converters.JSON

class EmployeeController {

    def index() { }

    def create(){//สร้าง action
        Branch b = Branch.get(1)// สร้าง instance ของ Branch ชื่อ b โดยให้ branch ไปดึงข้อมุล id ที่ 1
        Employee e = new Employee()//สร้าง instance ของ Employee ชื่อ e
        e.setTitleName("นาย")//ป้อนค่าให้ property ของ e
        e.setFirstName("โชคชัย")//ป้อนค่าให้ property ของ e
        e.setLastName("โชคดี")//ป้อนค่าให้ property ของ e
        e.setAge(20)//ป้อนค่าให้ property ของ e
        e.setGender("male")//ป้อนค่าให้ property ของ e
        e.setAddress("41/44 พระราม2 บางขุนเทียน 10150")//ป้อนค่าให้ property ของ e
        e.setTelephone("0908744423")//ป้อนค่าให้ property ของ e
        e.setSalary(9000)//ป้อนค่าให้ property ของ e
        e.setBranch(b)//ป้อนค่าให้ property Branch ของ e โดยใช้ id ที่ b ดึงมา
        if (!e.validate()){// check ว่ามี error ไหม
            e.errors.allErrors.each {// ถ้ามีให้แจ้ง ว่า Error
                print(it)// ให้แสดงข้อมูล ที่ error
            }
        }
        else {
            e.save()// ถ้าไม่ error ให้ save
        }

        Branch b1 = Branch.get(2)
        Employee e1 = new Employee()
        e1.setTitleName("นาย")
        e1.setFirstName("บุญรอด")
        e1.setLastName("บัวทอง")
        e1.setAge(25)
        e1.setGender("male")
        e1.setAddress("92 พระราม9 ดินแดง 10310")
        e1.setTelephone("0917784566")
        e1.setSalary(12000)
        e1.setBranch(b1)
        if (!e1.validate()){
            e1.errors.allErrors.each {
                print(it)
            }
        }
        else {
            e1.save()
        }


        Branch b2 = Branch.get(3)
        Employee e2 = new Employee()
        e2.setTitleName("นาย")
        e2.setFirstName("โชคชัย")
        e2.setLastName("โชคดี")
        e2.setAge(22)
        e2.setGender("male")
        e2.setAddress("12/1 พระราม4 ตลองเตย 10140")
        e2.setTelephone("0867784523")
        e2.setSalary(10000)
        e2.setBranch(b2)
        if (!e2.validate()){
            e2.errors.allErrors.each {
                print(it)
            }
        }
        else {
            e2.save()
        }

    }


    def emPay(){// สร้าง Action
//      หาว่าพนักงานคนนี้ทำบิลไปทั้งหมดกี่ใบ
        Employee e = Employee.get(2) // สร้าง instance ของ Employee ชื่อ e โดยให้ไปดึงค่าจาก id ตัวที่ 2
        Integer m//สร้างตัวแปร  m เป็น Integer
        m = e.employeePayment()//ให้ m มีค่าเท่ากับ method ที่ return ค่ากลับมาของ Employee
        println(m)// แสดงข้อมุลของ m

//      หาว่าพนักคนนี้ทำยอดไปทั้งหมดเท่าไร
        Double p// สร้างตัวแปร p เป็น Double
        p = e.emSale()//ให้ m มีค่าเท่ากับ method ที่ return ค่ากลับมาของ Employee
        println(p)//แสดงข้อมุลของ p
    }


    def list(){//สร้าง action
        def e = Employee.getAll() //ส้ราง list ชื่อ e โดยให้ ดึงข้อมูล ของ Employee ทั้งหมด
        def list = []// กำหนดค่าให้ตัวแปร list เป็น Array
        e.each {//ให้ e ทำ loop
            def mapEmployee = [:]// กำนหดตัวแปรให้ mapEmployee เป็น Map
            mapEmployee.put("id",it.getId())//ป้อนค่า key ชื่อ "id" โดยให้ it (หรือ class Branch) ไปดึง id ออกมา มาใส่ใน value
            mapEmployee.put("firstname",it.getFirstName())//ป้อนค่า key ชื่อ "firstname" โดยให้ it (หรือ class Branch) ไปดึง fristname ออกมา มาใส่ใน value
            mapEmployee.put("lastname",it.getLastName())//ป้อนค่า key ชื่อ "lastname" โดยให้ it (หรือ class Branch) ไปดึง lastname ออกมา มาใส่ใน value
            mapEmployee.put("address",it.getAddress())//ป้อนค่า key ชื่อ "address" โดยให้ it (หรือ class Branch) ไปดึง address ออกมา มาใส่ใน value
            mapEmployee.put("branch",it.getBranch().getName())//ป้อนค่า key ชื่อ "branch (ที่เป้น Property ของ Employee)" โดยให้ it (หรือ class Branch) ไปดึงจาก class Branch โดยให้ดึงชื่อออกมา
            def p = it.getPayments()//สร้าง list ชื่อ p = it (หรือ Property ของ employee) ไปดึงข้อมุลของ Payment มา
//            def countPay = p.size()// สร้าง list ชื่อ countPay ให้เก็บค่าของ p (การนับจำนวน Payment ทั้งหมดแบบ list)
            def countP = Payment.countByEmployee(it)// สร้าง list ชื่อ countP ให้เก้บข้อมูลของ Payment ที่นับจำนวนที่ Employee ทั้งหมด
            def listPay = []// กำหนดค่าให้ listPay เป็นแบบ Array
            Double total = 0.0// สร้าง ตัวแปร total เป็นค่า Double มีค่าเป้น 0
            p.each {// ให้ p ทำ loop ข้อมูล
                total += it.getTotalPrice()// ให้ total += ยอดทั้งหมดที่ Payment ดึงยอดรวม มา
                def mapPay = [:] // กำหนดตัวแปร mapPay เป็นแบบ Map
                mapPay.put("totalprice",it.getTotalPrice())// ให้ตัวแปร key เป้น "totalprice" ส่วน value คือค่าที่ Payment ไปดึงยอดรวมออกมา แล้วเอาไปใส่ให้ mapPay
                listPay.add(mapPay)//เอา Map ที่ชื่อ mapPay ไปใส่ใน array ที่ชื่อ listPay
            }
            mapEmployee.put("totalPayment",total)//เอาค่า total มาใส่ใน map ที่ชื่อ mapEmployee
//            mapEmployee.put("count",countPay)//เอาค่า countPay มาใส่ใน map ที่ชื่อ mapEmployee
            mapEmployee.put("count",countP)// เอาค่า countP มาใส่ใน map ที่ชื่อ mapEmployeee

            mapEmployee.put("payment",listPay)// เอาค่าที่ถูกเก็บใน listPay มาใส่ใน mapEmployee

            list.add(mapEmployee)//นำ mapEmployee ที่เก็บข้อมุลไว้ มาใส่ใน array ที่ชื่อ list ตัวใหญ่
        }
        println(list)// แสดงข้อมูลที่หมดของ action list

        render list as JSON// สร้างไฟล์ Json
    }



}
