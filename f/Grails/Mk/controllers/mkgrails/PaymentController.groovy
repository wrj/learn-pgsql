package mkgrails

class PaymentController {

    def index() { }

    def create() {//สร้าง Action
//      Table1
        MkTables t = MkTables.get(1)// สร้าง instance ของ MkTables ชื่อ t ให้ไปดึงข้อมุล id ที่ 1
        Employee e = Employee.get(1)// สร้าง instance ของ Employee ชื่อ e ให้ไปดึงข้อมุล id ที่ 1
        Payment p = new Payment()//สร้าง instance ของ Payment ชื่อ p
        p.setTotalPrice(185)//ป้อนค่า Property ให่ p
        p.setMkTable(t)//ป้อนค่าให้ Property ของ p มีค่าเท่ากับ id ที่ t ดึง id มา
        p.setEmployee(e)//ป้อนค่าให้ Property ของ p มีค่าเท่ากับ id ที่ e ดึง id มา
        p.setBranch(e.getBranch())//ป้อนค่า p ตรงกับ id กับสาขา โดยให้ไปดึงข้อมุลสาขามาจาก พนักงานที่ประจำสาขาอยู่นั้น
        if(p.save())// ถ้าข้อมูลถูกให้ save และให้แสดงข้อมูลรายการอาหารของบิลนั้น
        {
            Food f = Food.get(3)// สร้าง instance ของ Food ชื่อ f โดยให้ไปดึงดึงข้อมุล id ที่3 ของ Food
            PaymentFoods pf = new PaymentFoods()//สร้าง instance ของ PaymentFoods ชื่อ pf ซึ่ง class นี้มีการผูกกับ Payment และ Food
            pf.setFood(f)//ป้อนค่าให้ Property ของ pf มีค่าเท่ากับ id ที่ f ดึง id มา
            pf.setPayment(p)//ป้อนค่าให้ Property ของ pf มีค่าเท่ากับ id ที่ p ดึง id มา
            if(!pf.save())//ถ้าไม่ save หรือ save ไม่ได้ ให้แจ้ง error
            {
                pf.errors.allErrors.each {// ถ้ามี Error ให้ แจ้ง
                    print(it)// ให้แสดงข้อมุลทั้งหมดถ้า Error
                }
            }

            Food f1 = Food.get(10)
            PaymentFoods pf1 = new PaymentFoods()
            pf1.setPayment(p)
            pf1.setFood(f1)
            if (!pf1.save()){
                pf1.errors.allErrors.each {
                    println(it)
                }
            }
        }



//      Table2
        MkTables t1 = MkTables.get(2)
        Employee e1 = Employee.get(2)
        Payment p1 = new Payment()
        p1.setTotalPrice(663)
        p1.setMkTable(t1)
        p1.setEmployee(e1)
        p1.setBranch(e1.getBranch())
        if (p1.save()){
            Food f = Food.get(2)
            PaymentFoods pf = new PaymentFoods()
            pf.setPayment(p1)
            pf.setFood(f)
            if (!pf.save()){
                pf.errors.allErrors.each {
                    print(it)
                }
            }

            Food f1 = Food.get(7)
            PaymentFoods pf1 = new PaymentFoods()
            pf1.setFood(f1)
            pf1.setPayment(p1)
            if (!pf1.save()){
                pf1.errors.allErrors.each {
                    print(it)
                }
            }

            Food f2 = Food.get(8)
            PaymentFoods pf2 = new PaymentFoods()
            pf2.setFood(f2)
            pf2.setPayment(p1)
            if (!pf2.save()){
                pf2.errors.allErrors.each {
                    print(it)
                }
            }

            Food f3 = Food.get(9)
            PaymentFoods pf3 = new PaymentFoods()
            pf3.setPayment(p1)
            pf3.setFood(f3)
            if (!pf3.save()){
                pf3.errors.allErrors.each {
                    print(it)
                }
            }

            Food f4 = Food.get(11)
            PaymentFoods pf4 = new PaymentFoods()
            pf4.setPayment(p1)
            pf4.setFood(f4)
            if (!pf4.save()){
                pf4.errors.allErrors.each {
                    print(it)
                }
            }
        }



//      Table3
        MkTables t2 = MkTables.get(3)
        Employee e2 = Employee.get(2)
        Payment p2 = new Payment()
        p2.setTotalPrice(280)
        p2.setMkTable(t2)
        p2.setEmployee(e2)
        p2.setBranch(e1.getBranch())
        if (p2.save()){
            Food f = Food.get(1)
            PaymentFoods pf = new PaymentFoods()
            pf.setFood(f)
            pf.setPayment(p2)
            if (!pf.save()){
                pf.errors.allErrors.each {
                    print(it)
                }
            }
            Food f2 = Food.get(4)
            PaymentFoods pf2 = new PaymentFoods()
            pf2.setFood(f2)
            pf2.setPayment(p2)
            if (!pf2.save()){
                pf2.errors.allErrors.each {
                    print(it)
                }
            }
        }


    }





    def update(){
        Payment p = Payment.get(2)
        p.setTotalPrice()
        p.save()
    }
}
