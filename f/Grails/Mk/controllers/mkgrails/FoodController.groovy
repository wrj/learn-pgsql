package mkgrails


class FoodController {

    def index() { }


    //สร้าง Action ชื่อ create
    def create() {
        Food f = new Food() // สร้าง Instance จอง Food เป็น f
        Category c = Category.get(5)//  สร้าง Instance ของ Category เป็น c โดยให้ Category ไปดึง id ที่ 5
        f.setName("เป็ดย่าง จานเล็ก")// ป้อนค่าให้ Property ของ f
        f.setPrice(150)// ป้อนค่าให้ Property ของ f
        f.setCategory(c)// ป้อนค่า f เข้าไปใน Category
        if (!f.validate()) { // check ว่าถ้ามี error ไหม
            f.errors.allErrors.each {// ถ้ามี error ให้แจ้งเตือนว่าอะไร error
                print(it)// print error ของ Food
            }
        }
            //ถ้าไม่มี error ให้ save
        else {
            f.save()
        }

        Food f1 = new Food() // สร้าง Instance จอง Food เป็น f1
        Category c1 = Category.get(5)//  สร้าง Instance ของ Category เป็น c1 โดยให้ Category ไปดึง id ที่ 5
        f1.setName("เป็ดย่าง จานใหญ่")// ป้อนค่าให้ Property ของ f1
        f1.setPrice(300)// ป้อนค่าให้ Property ของ f1
        f1.setCategory(c1)// ป้อนค่า f1 เข้าไปใน Category
        if (!f1.validate()){ // check ว่าถ้ามี error ไหม
            f1.errors.allErrors.each {// ถ้ามี error ให้แจ้งเตือนว่าอะไร error
                print(it)// print error ของ Food
            }
        }
        //ถ้าไม่มี error ให้ save
        else {
            f1.save()
        }

        Food f2 = new Food()// สร้าง Instance จอง Food เป็น f2
        Category c2 = Category.get(3)//  สร้าง Instance ของ Category เป็น c2 โดยให้ Category ไปดึง id ที่ 3
        f2.setName("หมี่เป็ดย่าง")// ป้อนค่าให้ Property ของ f2
        f2.setPrice(150)// ป้อนค่าให้ Property ของ f2
        f2.setCategory(c2)// ป้อนค่า f2 เข้าไปใน Category
        if (!f2.validate()){// check ว่าถ้ามี error ไหม
            f2.errors.allErrors.each {// ถ้ามี error ให้แจ้งเตือนว่าอะไร error
                print(it)// print error ของ Food
            }
        }
        //ถ้าไม่มี error ให้ save
        else {
            f2.save()
        }

        Food f3 = new Food()// สร้าง Instance จอง Food เป็น f3
        Category c3 = Category.get(3)//  สร้าง Instance ของ Category เป็น c2 โดยให้ Category ไปดึง id ที่ 3
        f3.setName("หมี่หมูแดง")// ป้อนค่าให้ Property ของ f3
        f3.setPrice(130)// ป้อนค่าให้ Property ของ f3
        f3.setCategory(c3)// ป้อนค่า f3 เข้าไปใน Category
        if (!f3.validate()){
            f3.errors.allErrors.each {// ถ้ามี error ให้แจ้งเตือนว่าอะไร error
                print(it)// print error ของ Food
            }
        }
        else {
            f3.save()
        }

        Food f4 = new Food()// สร้าง Instance จอง Food เป็น f4
        Category c4 = Category.get(1)//  สร้าง Instance ของ Category เป็น c2 โดยให้ Category ไปดึง id ที่ 1
        f4.setName("ผักบุ้ง")// ป้อนค่าให้ Property ของ f4
        f4.setPrice(20)// ป้อนค่าให้ Property ของ f4
        f4.setCategory(c4)// ป้อนค่า f เข้าไปใน Category
        if (!f4.validate()){
            f4.errors.allErrors.each {// ถ้ามี error ให้แจ้งเตือนว่าอะไร error
                print(it)// print error ของ Food
            }
        }
        else {
            f4.save()
        }

        Food f5 = new Food()
        Category c5 = Category.get(1)
        f5.setName("ผักกาด")
        f5.setPrice(25)
        f5.setCategory(c5)
        if (!f5.validate()){
            f5.errors.allErrors.each {
                print(it)
            }
        }
        else {
            f5.save()
        }

        Food f6 = new Food()
        Category c6 = Category.get(2)
        f6.setName("หมูสไลด์")
        f6.setPrice(160)
        f6.setCategory(c6)
        if (!f6.validate()){
            f6.errors.allErrors.each {
                print(it)
            }
        }
        else {
            f6.save()
        }

        Food f7 = new Food()
        Category c7 = Category.get(2)
        f7.setName("ลูกชิ้นกุ้ง")
        f7.setPrice(89)
        f7.setCategory(c7)
        if (!f7.validate()){
            f7.errors.allErrors.each {
                print(it)
            }
        }
        else {
            f7.save()
        }

        Food f8 = new Food()
        Category c8 = Category.get(2)
        f8.setName("ลูกชิ้นหมู")
        f8.setPrice(79)
        f8.setCategory(c8)
        if (!f8.validate()){
            f8.errors.allErrors.each {
                print(it)
            }
        }
        else {
            f8.save()
        }


        Food f9 = new Food()
        Category c9 = Category.get(4)
        f9.setName("ชาเขียวเย็น")
        f9.setPrice(35)
        f9.setCategory(c9)
        if (!f9.validate()){
            f9.errors.allErrors.each {
                print(it)
            }
        }
        else {
            f9.save()
        }

        Food f10 = new Food()
        Category c10 = Category.get(4)
        f10.setName("เก๊กฮวยเย็น")
        f10.setPrice(35)
        f10.setCategory(c10)
        if (!f10.validate()){
            f10.errors.allErrors.each {
                print(it)
            }
        }
        else {
            f10.save()
        }
    }



    def update(){ //สร้าง Action ไว้ update ข้อมูล
        Food f = Food.get(1) // สร้าง Instance ของ food เป็น f ให้ไปดึงข้อมุล ที่ id 1
        f.setName("ลูกชิ้นปู") // ป้อนค่าให้ Property ของ f
        f.setPrice(99) // ป้อนค่าให้ Property ของ f
        f.save() // save ข้อมูล
    }



    def delete(){
        Food f = Food.get(1)// สร้าง Instance ของ Food เป็น f ให้ดึงข้อมุล ของ id ที่ 1 มา
        f.delete() // สั่งให้ f ลบข้อมูลทิ้ง
    }
}
