package mkgrails

import grails.converters.JSON

class BranchController {

    def index() { }

    def create() {//สร้าง Action
        Branch b = new Branch()// สร้าง instance Branch
        b.setName("เซ็นทรัลพระราม2")//ป้อนค่าให้ Property ของ b
        b.setOpenTime(new Date())//ป้อนค่าให้ Property ของ b
        b.setCloseTime(new Date())//ป้อนค่าให้ Property ของ b
        b.setTelephone("0244159899")//ป้อนค่าให้ Property ของ b
        b.setAddress("เซ็นทรัลพระราม2")//ป้อนค่าให้ Property ของ b
        if (!b.validate()) {//check ว่า Error ไหม
            b.errors.allErrors.each {//ถ้ามี Error ให้ แจ้ง
                print(it)//แสดงข้อมูล Error
            }
        }
        else {
            b.save()//ถ้าไม่ error ให้ save
        }


        Branch b1 = new Branch()
        b1.setName("เซ็นทรัลพระราม3")
        b1.setOpenTime(new Date())
        b1.setCloseTime(new Date())
        b1.setTelephone("024436677")
        b1.setAddress("เซ็นทรัลพระราม3")
        if (!b1.validate()) {
            b1.errors.allErrors.each {
                print(it)
            }
        }
        else {
            b1.save()
        }

        Branch b2 = new Branch()
        b2.setName("เซ็นทรัลพระราม9")
        b2.setOpenTime(new Date())
        b2.setCloseTime(new Date())
        b2.setTelephone("029901234")
        b2.setAddress("เซ็นทรัลพระราม9")
        if (!b2.validate()) {
            b2.errors.allErrors.each {
                print(it)
            }
        }
        else {
            b2.save()
        }
    }

//    หาว่าสาขานี้มีพนักงานคนไหนบ้าง
    def bEmployee(){//สร้าง Action
        Branch b = Branch.get(1) //สร้าง instance ของ Branch ชื่อ b โดยให้ไปดึงข้อมุล id ที่ 1
        def e1 = b.getEmployees()// สร้าง list ชื่อ e1 โดยให้ Branch ไปดึงข้อมุลของ Employee
        e1.each { // ให้ e1 ทำ loop
            println(it.getFirstName()) //แสดงข้อมูล ที่ Branch ไปดึงของมูลของ firstname
        }
    }

//       หายอดรวมทั้งหมดหมดของสาขานี้
    def bTotalPrice(){// สร้าง action

        Branch b = Branch.get(1)//  สร้าง instance ของ Branch ชื่อ b ให้ไปดึงข้อมุลของ id ที่ 1
        b.getTotalPrice()// ให้ b ดึง method นี้มาใช้
//        Double total = 0.0
//        def p = b.getPayments()
//        p.each {
//            total += it.getTotalPrice()
//        }
//        println(total)
//
//
//
//        def b1 = Branch.getAll() //สร้าง list ชื่อ b1 โดยให้ Branch ไปดึงข้อมูลทั้งหมดออกมา
//        Double totalSum = 0.0 // สร้างตัวแปรชื่อ totalSum เป็น Double มีค่า 0
//        b1.each {//ให้ b1 ทำ loop
//            Double total2 = 0.0 // สร้างตัวแปรชื่อ total2 เป็น Double มีค่า 0
//            def p1 = it.getPayments()//สร้าง list ชื่อ p1 โดยให้ Branch ไปดึงข้อมุลของ Payment
//            p1.each {// ให้ p1 ทำ loop
//                total2 += it.getTotalPrice()//ให้ total2 += it ที่ไปดึงยอดรมของ Payment ออกมา
//            }
//            totalSum += total2 //ให้ ตัวแปรสองตัวมา + กัน
//            println("ยอดรวมของสาขาทั้งหมด ${it.getName()} ${total2}") //แสดงข้อมูลโดย Print เป็นข้อความ
//        }
//        println(totalSum) //แสดงข้อมุล ยอดรวมทั้งหมดของสาขา

    }



    def list(){
        def b = Branch.getAll()
        def listJ = []
        b.each {
            def mapJ = [:]
            mapJ.put("date",new Date())
            mapJ.put("id",it.getId())
            mapJ.put("name",it.getName())
            mapJ.put("totalprice",it.getTotalPrice())

            def listEmployee = []
            def e = it.getEmployees()
            e.each {
                def mapEmployee = [:]
                mapEmployee.put("id",it.getId())
                mapEmployee.put("name",it.getFirstName())
                mapEmployee.put("lastname",it.getLastName())
                mapEmployee.put("salary",it.getSalary())
                listEmployee.add(mapEmployee)
            }
            mapJ.put("employee",listEmployee)

            def listPayment = []
            def p = it.getPayments()
            p.each {
                def mapPayment = [:]
                mapPayment.put("totalprice",it.getTotalPrice())
                listPayment.add(mapPayment)
            }
            mapJ.put("payment",listPayment)
            listJ.add(mapJ)
        }
        render listJ as JSON
    }
}
