package mkgrails

class CategoryController {

    def index() { }

    def create() {//สร้าง action create
        Category c = new Category() //สร้าง instance ของ Category เป้น c
        c.setName("Vegetable")// ป้อนค่า Property ให้ c
        if (!c.validate()){// check ว่า c มี error ไหม
            c.errors.allErrors.each {// ถ้ามี error ให้ แจ้ง
                print(it)// แสดงข้อมุลที่ error
            }
        }
        else {
            c.save()// ถ้าไม่มี error ให้ save
        }

        Category c2 = new Category()
        c2.setName("Fresh Foods")
        if (!c2.validate()){
            c2.errors.allErrors.each {
                print(it)
            }
        }
        else {
            c2.save()
        }

        Category c3 = new Category()
        c3.setName("Noodles")
        if (!c3.validate()){
            c3.errors.allErrors.each {
                print(it)
            }
        }
        else {
            c3.save()
        }

        Category c4 = new Category()
        c4.setName("Beverages")
        if (!c4.validate()){
            c4.errors.allErrors.each {
                print(it)
            }
        }
        else {
            c4.save()
        }

        Category c5 = new Category()
        c5.setName("Roasted Ducks")
        if (!c5.validate()){
            c5.errors.allErrors.each {
                print(it)
            }
        }
        else {
            c5.save()
        }
    }



    def catCount(){
        Category c = Category.get(1)//
        Integer m
        m = c.countCat()
        println(m)
    }




    def list(){//สร้าง action ชื่อ list
        def c = Category.getAll()//สร้าง list ชื่อ c ให้ Category ไปดึงข้อมุทั้งหมด
        c.each {// ให้ c ทำ loop เพื่อดึงข้อมุล
            println(it.getName()) // ให้แสดงข้อมุลออกมาเป็น name โดยให้ it ที่เป้น instance ไปดึงข้อมูล name จะเห็นชื่อประเภท ทั้งหมด
            println("======= Menu =========")
            def f = Food.findAllByCategory(it)// สร้าง list ชื่อ f ให้ดึงข้อมูลทั้งหมดของ class Food ที่ Category ทุกตัว
            f.each { // ให้ f ทำ loop เพื่อดึงข้อมุล
                println(it.getName())// ให้แสดงข้อมูล ที่เป็น name ทั้งหมด
            }
            println("======== End Menu ==========")
        }
    }


//    def update(){
//        Category c = Category.get(1)
//        c.setName("Desserts")
//        c.save()
//    }
//
//    def delete(){
//        Category c = Category.get(2)
//        c.delete()
//    }
}
