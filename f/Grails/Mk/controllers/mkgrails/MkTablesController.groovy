package mkgrails

class MkTablesController {

    def index() { }

    def create(){ // สร้าง action
        MkTables t1 = new MkTables() //สร้าง instance ของ MkTables เป็น t1
        t1.setTableNo("T1")// ป้อนค่าให้ Property ของ t1
        if (!t1.validate()) { //check ว่า Error ไหม
            t1.errors.allErrors.each { //ถ้า Error ให้ แจ้ง Error
                print(it)//แสดงข้อมูลที่ Error
            }
        }
        else {
            t1.save() //ถ้าไม่ error ให้ save
        }

        MkTables t2 = new MkTables()//สร้าง instance ของ MkTables เป็น t1
        t2.setTableNo("T2")// ป้อนค่าให้ Property ของ t1
        if (!t2.validate()) {//check ว่า Error ไหม
            t2.errors.allErrors.each {//ถ้า Error ให้ แจ้ง Error
                print(it)//แสดงข้อมูลที่ Error
            }
        }
        else {
            t2.save()//ถ้าไม่ error ให้ save
        }

        MkTables t3 = new MkTables()//สร้าง instance ของ MkTables เป็น t1
        t3.setTableNo("T3")// ป้อนค่าให้ Property ของ t1
        if (!t3.validate()) {//check ว่า Error ไหม
            t3.errors.allErrors.each {//ถ้า Error ให้ แจ้ง Error
                print(it)//แสดงข้อมูลที่ Error
            }
        }
        else {
            t3.save()//ถ้าไม่ error ให้ save
        }
    }

    def update(){ // สร้าง action ไว้ update
        MkTables t = MkTables.get(1)//สร้าง instance ให้ MkTables เป็น t
        t.setTableNo("")//ป้อนค่า Property ให้ t
        t.save()//ให้ save
    }

    def delete(){ //สร้าง action delete
        MkTables t = MkTables.get(2)//สร้าง instance ของ MkTables เป็น t
        t.delete()//สั่งให้ delete
    }
}
