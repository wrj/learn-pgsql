package mkgrails

class Food {

    static belongsTo = [category:Category] // เป็นของ Category ถ้าลบ category Food ก็หาย
    static hasMany = [paymentFoods:PaymentFoods] // รายการอาหาร1รายการอยู่ บิลได้หลายใบ


    String name // Property name เป้น String
    Double price // Property price เป็น Double

    static constraints = {
        name blank: false // name ห้ามว่างต้องกรอกข้อมูล

    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_food_id'] //สร้าง id ของ Food
    }
}
