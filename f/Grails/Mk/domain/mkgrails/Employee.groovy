package mkgrails

import net.sf.ehcache.search.aggregator.Count

import java.awt.print.Printable

class Employee {
    static belongsTo = [branch:Branch] // เป็นของ Branch ถ้าลบ Branch  Employee ก็หายด้วย
    static hasMany = [payments:Payment] // พนักงาน 1 คน สามารถทำบิลได้หลายใบ

    String titleName // Property titlename เป็น String
    String firstName // Property firstname เป็น String
    String lastName // Property lastname เป็น String
    String gender // Property gender เป็น String
    Integer age // Property เป็น Integer
    String telephone //Property telephone เป็น String
    String address// Property address เป็น String
    Double salary // Property salary เป็น Double

    static constraints = {
        titleName blank: false // titlename ห้ามว่างต้องกรอกข้อมุล
        firstName blank: false // firstname ห้ามว่างต้องกรอกข้อมุล
        lastName blank: false // lastname ห้ามว่างต้องกรอกข้อมุล
        gender blank: false // gender ห้ามว่างต้องกรอกข้อมุล
        telephone blank: false // telephone ห้ามว่างต้องกรอกข้อมุล
        address blank: false // address ห้ามว่างต้องกรอกข้อมุล
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_employee_id'] // สร้าง id ของ Employee
    }

//หาจำนวนบิลว่าพนักงานคนนี้ทำบิลไปกี่ใบ
    Integer employeePayment(){ // สร้าง method เป็น Integer
        return this.getPayments().size() // return ค่า โดย Employee ไปดึงข้อมุล Payment แล้วนับจำนวน
    }

//หายอดขายว่าพนักงานคนนี้ทำยอดได้เท่าไร
    Double emSale(){ // สร้าง method เป็น Double
        Double totalSum = 0.0 // สร้าง variable เป็น Double ให้ค่าเป็น 0
        this.getPayments().each { // ทำ loop ให้ employee ไปดึงข้อมุล Payment
            totalSum += it.getTotalPrice() // totalSum  ไป + กับ  employee ที่ไปดึงยอดของ Payment ออกมา
        }
        return totalSum // และให้  return ค่ากลับ
    }
}
