package mkgrails

class Category {

    static hasMany = [foods:Food] // 1ประเภท มีอาหารได้หลายอย่าง

    String name // Property name เป็น String

    static constraints = {
        name blank: false // name ห้ามว่าง ต้องกรอกข้อมุล ("")
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_category_id']// สร้าง id ของ category
    }


    // สร้าง Method การนับว่า ประเภทนี้ มีกี่รายการอาหาร
    Integer countCat(){ // ให้ method countCat เป็น Integer
//        return this.getFoods().size() // ให้ส่งค่ากลับไป โดย Category ไปดึงรายการอาหารจาก Food แล้วนับจำนวน
//        หรือ
        return Food.countByCategory(this)// return ค่า โดย Domain Food ไปนับจำนวน ที่ category
    }
}
