package mkgrails

class MkTables {

    String tableNo// Property tableNo เป็น String

    static constraints = {
        tableNo blank: false // tableNo ห้ามว่างต้องกรอกข้อมูล
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_table_id'] // สร้าง id ของ MkTables
    }
}
