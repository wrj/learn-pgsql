package mkgrails

class PaymentFoods {

    static belongsTo = [payment:Payment,food:Food]// เป็นของ Payment และ Food ถ้าไม่มีสักตัว บิลรายการก็ไม่มี

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_payment_foods_id']//สร้าง id ของ paymentfoods
    }
}
