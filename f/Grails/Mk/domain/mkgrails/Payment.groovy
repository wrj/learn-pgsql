package mkgrails

class Payment {

    static belongsTo = [employee:Employee,mkTable:MkTables,branch:Branch] //เป็นของ Employee MkTables Branch ถ้าไม่มีตัวใดตัวนึง บิลก็ออกไม่ได้

    Double totalPrice // Property totalPrice เป็น  Double

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_payment_id'] // สร้าง id ของ Payment
    }
}
