package mkgrails

class Branch {

    //                 Property:Domain , Property:Domain
    static hasMany = [employees:Employee,payments:Payment] // สาขานึงมีบิลได้หลายใบ พนักงานได้หลายคน

    String name // Property name เป็น String
    Date openTime // Property opentime เป็น Date
    Date closeTime // Property closetime เป็น Date
    String address // Property address เป้น  String
    String telephone // Property telephone เป้น String

    static constraints = {
        name blank: false //  name ห้ามว่าง ("")
        address blank: false // address ห้ามว่าง ("")
        telephone blank: false // telephone ห้ามว่าง ("")
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_branch_id'] // สร้าง id ของ Branch
    }

    //สร้าง Method การหายอดรวมของสาขา
    Double getTotalPrice(){ // สร้าง method เป็นแบบ Double
        Double total = 0.0 //กำหนด variable เป็น Double เป็น 0
        this.getPayments().each { // ให้ Branch ไปดึงข้อมุลของ Domain Payment มาทำ loop เพื่อคำนวนราคา
            total += it.getTotalPrice() // บอกให้ total + กับตัวที่ Branch ไปดึงยอดของ Payment มา
        }
        println(total)
        return total // return ค่า เป็น total
    }
}
