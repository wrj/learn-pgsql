package CarcareGrails

class PaymentServices {

    static belongsTo = [carservices:CarServices,payment:Payment]
    static hasMany = [payment:Payment,carservices:CarServices,]

    String description

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_payment_services_id']
    }
}
