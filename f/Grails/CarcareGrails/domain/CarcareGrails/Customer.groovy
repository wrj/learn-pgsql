package CarcareGrails

class Customer {

    static hasMany = [payment:Payment]

    String nameTitle
    String firstName
    String lastName
    String brand
    String serie
    String licensePlate
    String color
    String email
    String telephone
    String gender
    String address


    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_Customer_id']
    }
}
