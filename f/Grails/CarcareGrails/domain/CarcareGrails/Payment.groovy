package CarcareGrails

class Payment {

    static belongsTo = [employee:Employee,customer:Customer,branch:Branch]

    Double totalPrice
    Date serviceDate

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_Payment_id']
    }
}
