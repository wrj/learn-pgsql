package CarcareGrails

class Category {

    static belongsTo = Branch
    static hasMany = [carservices:CarServices,branchs: Branch]

    String name

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_Category_id']
    }
}

