package CarcareGrails

class CarServices {

    static belongsTo = [category:Category]
    static hasMany = [category:Category]

    String name
    Double smallPrice
    Double mediumPrice
    Double largePrice
    Double otherPrice

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_car_service_id']
    }
}
