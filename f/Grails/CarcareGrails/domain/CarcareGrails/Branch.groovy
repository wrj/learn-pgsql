package CarcareGrails

class Branch {

    static hasMany = [category:Category,payment:Payment,employee:Employee]

    String name
    Date openTime
    Date closeTime
    String telephone
    String workPhone
    String holiday
    String address
    Boolean status

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_Branch_id']
    }
}
