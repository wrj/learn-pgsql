package CarcareGrails

class Employee {
    static belongsTo = [branch:Branch]
    static hasMany = [payment:Payment]

    String nameTitle
    String firstName
    String lastName
    Integer age
    String gender
    String telephone
    Double salary
    String address
    String email


    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_Employee_id']
    }
}
