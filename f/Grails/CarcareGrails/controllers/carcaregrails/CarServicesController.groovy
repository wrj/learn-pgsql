package carcaregrails

import CarcareGrails.CarServices

class CarServicesController {

    def index() {
        CarServices cs = new CarServices()
        cs.setName("ขจัดละอองสี")
        cs.setSmallPrice(2000.00)
        cs.setMediumPrice(2500.00)
        cs.setLargePrice(3000.00)
        cs.setOtherPrice(3500.00)
        cs.validate()
        if(cs.hasErrors())
        {
            cs.errors.allErrors.each {
                println it
            }
        }else{
            cs.save()
        }

    }
//
//    def update() {
//        CarServices cs = CarServices.get(1)
//        cs.setName("ขจัดละอองสี")
//        cs.setSmallPrice(3000)
//        cs.setMediumPrice(2500)
//        cs.setLargePrice(3000)
//        cs.setOtherPrice(3500)
//        cs.save()
//    }
//
//
//    def delete() {
//        CarServices cs = CarServices.get(3)
//        cs.delete()
//    }
}
