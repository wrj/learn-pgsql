import CarcareGrails.Branch
import CarcareGrails.Category

class BootStrap {

    def init = { servletContext ->
        Branch b = new Branch()
        b.setName("พระราม1")
        b.setOpenTime(new Date())
        b.setCloseTime(new Date())
        b.setTelephone("023456789")
        b.setWorkPhone("023456789")
        b.setHoliday("ทุกวันพุธ")
        b.setAddress("831 ถนนพระราม1 วังใหม่ ปทุมวัน กรุงเทพมหานคร 10330")
        b.setStatus(true)
        b.save()

        Branch b2 = new Branch()
        b2.setName("พระราม2")
        b2.setOpenTime(new Date())
        b2.setCloseTime(new Date())
        b2.setTelephone("023452396")
        b2.setWorkPhone("023456790")
        b2.setHoliday("ทุกวันพฤหัส")
        b2.setAddress("160 ถนนพระราม2 แขวงแสมดำ เขตบางขุนเทียน กรุงเทพมหานคร 10150")
        b2.setStatus(true)
        b2.save()


        Category c = new Category()
        c.setName("wash")
        c.save()
        Category c2 = new Category()
        c2.setName("paint protection")
        c2.save()
        Category c3 = new Category()
        c3.setName("show car shine")
        c3.save()

        b.addToCategory(c2)
        b.save()
        b2.addToCategory(c3)
        b2.save()


    }
    def destroy = {

    }
}
