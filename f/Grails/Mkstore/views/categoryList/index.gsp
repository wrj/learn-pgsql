<html>
<head>
    <title>My Category</title>
</head>
<body>
<table width="100%">
    <tr>
        <td>
            <a href="<g:createLink action="newForm"/>">New</a>
        </td>
    </tr>
    <tr>
        <td>Category</td>
    </tr>
    <tr>
        <g:each in="${category}">
            <tr>
                <td>${it.getName()}</td>
                <td>
                    <a href="<g:createLink action="edit" id="${it.getId()}"/>">edit</a>
                    <a href="<g:createLink action="delete" id="${it.getId()}"/>">delete</a>
                </td>
            </tr>
        </g:each>
    </tr>
</table>
</body>
</html>