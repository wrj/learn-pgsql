<table>
    <tr>
        <td>Name :</td>
        <td><g:textField name="name" value="${category.getName()}"/> </td>
    </tr>
    <tr>
        <td>
            <g:if test="${category.getId() == null}">
                <g:submitButton name="Save"></g:submitButton>
            </g:if>
            <g:else>
                <g:submitButton name="Update"></g:submitButton>
            </g:else>
        </td>
    </tr>
</table>