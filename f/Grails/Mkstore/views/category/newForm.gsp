<html>
<head>
    <title>My Category</title>
</head>
<body>
<table width="100%">
    <tr>
        <td>
            <a href="<g:createLink action="index"/>">Back</a>
        </td>
    </tr>
    <g:form action="create" method="POST">
        <g:render template="form" model="[category:category]"></g:render>
    </g:form>
</table>
</body>
</html>