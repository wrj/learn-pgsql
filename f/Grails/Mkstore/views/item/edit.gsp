<html>
<head>
    <title>My Item</title>
</head>
<body>
<table width="100%">
    <tr>
        <td>
            <a href="<g:createLink action="index"/>">Back</a>
        </td>
    </tr>
    <g:form action="update" id="${item.getId()}" method="POST">
        <g:render template="form" model="[item:item,category:category]"></g:render>
    </g:form>
</table>
</body>
</html>