<table>
    <tr>
        <td>Name :</td>
        <td><g:textField name="name" value="${item.getName()}"/> </td>
    </tr>
    <tr>
        <td>Price :</td>
        <td><g:textField name="price" value="${item.getPrice()}"/> </td>
    </tr>
    <tr>
        <td>Category : </td>
        <td>
            <select name="category">
                <g:each in="${category}">
                    <option value="${it.getId()}" <g:if test="${it.getId() == item.getCategoryId()}">selected</g:if> >${it.getName()}</option>
                </g:each>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <g:if test="${item.getId() == null}">
                <g:submitButton name="Save"></g:submitButton>
            </g:if>
            <g:else>
                <g:submitButton name="Update"></g:submitButton>
            </g:else>
        </td>
    </tr>
</table>