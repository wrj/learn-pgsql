<html>
<head>
    <title>My Item</title>
</head>
<body>
<table width="100%">
    <tr>
        <td>
            <a href="<g:createLink action="newForm"/>">New</a>
        </td>
    </tr>
    <tr>
        <td>Item</td>
    </tr>
    <tr>
        <g:each in="${item}">
            <tr>
                <td>${it.getName()}</td>
                <td>${it.getPrice()}</td>
                <td>
                    <a href="<g:createLink action="edit" id="${it.getId()}"/>">edit</a>
                    <a href="<g:createLink action="delete" id="${it.getId()}"/>">delete</a>
                </td>
            </tr>
        </g:each>
    </tr>
</table>
</body>
</html>