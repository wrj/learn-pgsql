<html>
<head>
    <title>My Branch</title>
</head>
<body>
<table width="100%">
    <tr>
        <td>
            <a href="<g:createLink action="index" />">Back</a>
        </td>
    </tr>
    <g:form action="update" id="${branch.getId()}" method="POST">
        <g:render template="form" model="[branch:branch]"></g:render>
    </g:form>
</table>
</body>
</html>