<table width="100%">
    <tr>
        <td>Name :</td>
        <td><g:textField name="name" value="${branch.getName()}"/> </td>
    </tr>
    <tr>
        <td>Address :</td>
        <td><g:textField name="address" value="${branch.getAddress()}"/> </td>
    </tr>
    <tr>
        <td>Phone :</td>
        <td><g:textField name="phone" value="${branch.getPhone()}"/> </td>
    </tr>
    <tr>
        <td>
            <g:if test="${branch.getId() == null}">
                <g:submitButton name="Save"></g:submitButton>
            </g:if>
            <g:else>
                <g:submitButton name="Update"></g:submitButton>
            </g:else>
        </td>
    </tr>
</table>