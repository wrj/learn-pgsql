<html>
<head>
    <title>My Branch</title>
</head>
<body>
<table width="100%">
    <tr>
        <td>
            <a href="<g:createLink action="newForm" />">Add New</a>
        </td>
    </tr>
    <tr>
        <td>Branch</td>
    </tr>
    <tr>
        <g:each in="${branch}">
            <tr>
                <td>${it.getName()}</td>
                <td>${it.getAddress()}</td>
                <td>
                    <a href="<g:createLink action="edit" id="${it.getId()}"/>">Edit</a>
                    <a href="<g:createLink action="delete" id="${it.getId()}"/>">Delete</a>
                </td>
            </tr>
        </g:each>
    </tr>
</table>
</body>
</html>