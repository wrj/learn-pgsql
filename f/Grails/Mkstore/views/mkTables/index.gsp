<html>
<head>
    <title>MK Table</title>
</head>
<body>
<table>
    <tr>
        <td><a href="<g:createLink action="newForm" />">New Table</a></td>
    </tr>
    <tr>
        <td>Mk TableNo.</td>
    </tr>
    <tr>
        <g:each in="${table}">
            <tr>
                <td>${it.getTableNo()}</td>
                <td>
                    <a href="<g:createLink action="edit" id="${it.getId()}"/>">Edit</a>
                    <a href="<g:createLink action="delete" id="${it.getId()}"/>">Delete</a>
                </td>
            </tr>
        </g:each>
    </tr>
</table>
</body>
</html>