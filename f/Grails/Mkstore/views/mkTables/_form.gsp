<table>
    <tr>
        <td>Table No :</td>
                       %{--กำหนดชื่อให้ html        tableนี้มาจาก key ของ model หน้า newform มันถึงเรียก tableNo ได้--}%
        <td><g:textField name="tableNo" value="${table.getTableNo()}"/></td>
    </tr>
    <tr>
        <td>
            <g:if test="${table.getId() == null}">
                <g:submitButton name="Save"></g:submitButton>
            </g:if>
            <g:else>
                <g:submitButton name="Update"></g:submitButton>
            </g:else>
        </td>
    </tr>
</table>