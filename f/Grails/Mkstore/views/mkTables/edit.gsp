<html>
<head>
    <title>Mk Table No</title>
</head>
<body>
<table width="100%">
    <tr>
        <td>
            <a href="<g:createLink action="index"/>">Back</a>
        </td>
    </tr>
    <g:form action="update" id="${table.getId()}" method="POST">
        <g:render template="form" model="[table:table]"></g:render>
    </g:form>
</table>
</body>
</html>