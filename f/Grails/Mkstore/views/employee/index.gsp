<html>
<head>
    <title>Employee</title>
</head>
<body>
    <table>
        <tr>
            <td><a href="<g:createLink action="newForm" />">Add New</td>
        </tr>
        <tr>
            <td>Employee</td>
        </tr>
        <tr>
            <g:each in="${employee}">
                <tr>
                    <td>${it.getTitleName()}</td>
                    <td>${it.getFirstName()}</td>
                    <td>${it.getLastName()}</td>
                    <td>${it.getAddress()}</td>
                    <td>${it.getPhone()}</td>
                    <td>${it.getSalary()}</td>
                    <td>
                        <a href="<g:createLink action="edit" id="${it.getId()}" />">Edit</a>
                        <a href="<g:createLink action="delete" id="${it.getId()}" />">Delete</a>
                    </td>
                </tr>
            </g:each>
        </tr>
    </table>
</body>
</html>