<table>
    <tr>
        <td>Titlename</td>
        <td>
            <g:textField name="titleName" value="${employee.getTitleName()}" />
        </td>
    </tr>
    <tr>
        <td>Firstname</td>
        <td>
            <g:textField name="firstName" value="${employee.getFirstName()}" />
        </td>
    </tr>
    <tr>
        <td>Lastname</td>
        <td>
            <g:textField name="lastName" value="${employee.getLastName()}" />
        </td>
    </tr>
    <tr>
        <td>Address</td>
        <td>
            <g:textArea name="address" value="${employee.getAddress()}" rows="5" cols="40"/>
        </td>
    </tr>
    <tr>
        <td>Phone</td>
        <td>
            <g:textField name="phone" value="${employee.getPhone()}" />
        </td>
    </tr>
    <tr>
        <td>Salary</td>
        <td>
            <g:textField name="salary" value="${employee.getSalary()}" />
        </td>
    </tr>
    <tr>
        <td>Branch : </td>
        <td>
            <select name="branch">
                <g:each in="${branch}">
                    <option value="${it.getId()}" <g:if test="${it.getId() == employee.getBranchId()}">selected</g:if> >${it.getName()}</option>
                </g:each>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <g:if test="${employee.getId() == null}">
                <g:submitButton name="Save"></g:submitButton>
            </g:if>
            <g:else>
                <g:submitButton name="Update"></g:submitButton>
            </g:else>
        </td>
    </tr>
</table>