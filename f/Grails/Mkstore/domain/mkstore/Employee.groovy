package mkstore

class Employee {

    static belongsTo = [branch:Branch]

    String titleName
    String firstName
    String lastName
    String phone
    String address
    Double salary
    Date createDate
    Date lastUpdate


    static constraints = {
        titleName blank: false
        firstName blank: false
        lastName blank: false
        address blank: false
        phone blank: false
        createDate nullable: true
        lastUpdate nullable: true
    }

    def beforeInsert() {
        this.createDate = new Date()
        this.lastUpdate = new Date()
    }

    static mapping = {
        id generator: 'sequence',params: [sequence:'seq_employee_id']
    }
}
