package mkstore

class Branch {

    static hasMany = [employees:Employee,payments:Payment]

    String name
    String address
    String phone
    Date createDate
    Date lastUpdate

    static constraints = {
        name blank: false
        address blank: false
        phone blank: false
        createDate nullable: true
        lastUpdate nullable: true
    }

    def beforeInsert() {
        this.createDate = new Date()
        this.lastUpdate = new Date()
    }

    static mapping = {
        id generator: 'sequence',params: [sequence:'seq_branch_id']
    }
}
