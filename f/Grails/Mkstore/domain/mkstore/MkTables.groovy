package mkstore

class MkTables {

    Integer tableNo
    Date createDate
    Date lastUpdate

    static constraints = {
        createDate nullable: true
        lastUpdate nullable: true
    }

    def beforeInsert() {
        this.createDate = new Date()
        this.lastUpdate = new Date()
    }

    static mapping = {
        id generator: 'sequence',params: [sequence:'seq_mk_table_id']
    }
}
