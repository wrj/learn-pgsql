package mkstore

class CategoryController {

    def index() {
        List category = Category.list()
        [category:category]
    }

    def list(){
        List item = Item.findAllByCategory(Category.get(params.id))
        [item:item]
    }

    def newForm(){
        Category category = new Category(params.id)
        category.setName(params.name)
        if (!category.save()){
            category.errors.allErrors.each {
                println(it)
            }
        }
    }

    def create(){
        Category c = new Category(params.id)
        c.setName(params.name)
        if (!c.save()){
            c.errors.allErrors.each {
                println(it)
            }
        }
    }

    def update(){
        Category c = Category.get(params.id)
        c.setName(params.name)
        if (!c.save()){
            c.errors.allErrors.each {
                println(it)
            }
        }
    }

    def delete(){
        Category c = Category.get(params.id)
        c.delete()
    }
}
