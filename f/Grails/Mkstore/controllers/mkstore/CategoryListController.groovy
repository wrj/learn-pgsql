package mkstore

class CategoryListController {

    def index() {
        List category = Category.list()
        [category: category]
    }


    def newForm() {
        Category category = new Category()
        [category: category]
    }


    def edit(){
        Category category = Category.get(params.id)
        [category:category]
    }

    def create() {
        Category c = new Category(params.id)
        c.setName(params.name)
        if (!c.save()) {
            c.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }



    def update() {
        Category c = Category.get(params.id)
        c.setName(params.name)
        if (!c.save()) {
            c.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }

    def delete() {
        Category c = Category.get(params.id)
        c.delete()
        redirect(action:"index")
    }

}
