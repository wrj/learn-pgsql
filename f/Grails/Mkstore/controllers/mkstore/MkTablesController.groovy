package mkstore

class MkTablesController {

    def index() {
        List table = MkTables.list()
        [table:table]
    }

    def newForm(){
        MkTables table = new MkTables()
        [tablekey:table]//[key:value]
    }

    def edit(){
        MkTables table = MkTables.get(params.id)
        [table:table]
    }

    def create(){
        MkTables table = new MkTables(params.id)
        table.setTableNo(params.tableNo.toInteger())
        if (!table.save()){
            table.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }

    def update(){
        MkTables table = MkTables.get(params.id)
        table.setTableNo(params.tableNo.toInteger())
        if (!table.save()){
            table.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }

    def delete(){
        MkTables table = MkTables.get(params.id)
        table.delete()
        redirect(action:"index")
    }
}
