package mkstore

class BranchController {

    def index() {
        List branch = Branch.list()
        [branch:branch]
    }

    def newForm(){
        Branch branch = new Branch()
        [branch:branch]
    }

    def edit(){
        Branch branch = Branch.get(params.id)
        [branch:branch]
    }

    def create(){
        Branch branch = new Branch(params.id)
        branch.setName(params.name)
        branch.setAddress(params.address)
        branch.setPhone(params.phone)
        if (!branch.save()){
            branch.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }

    def update(){
        Branch branch = Branch.get(params.id)
        branch.setName(params.name)
        branch.setAddress(params.address)
        branch.setPhone(params.phone)
        if (!branch.save()){
            branch.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }

    def delete(){
        Branch branch = Branch.get(params.id)
        branch.delete()
        redirect(action:"index")
    }
}
