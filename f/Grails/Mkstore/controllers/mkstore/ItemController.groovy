package mkstore

class ItemController {

    def index() {
        List item = Item.list()
        [item:item]
    }

    def newForm(){
        Item item = new Item()
        List category = Category.list()
        [item: item,category:category]
    }

    def edit() {
        Item item = Item.get(params.id)
        List category = Category.list()
        [item: item,category:category]
    }

    def create(){
        println(params)
        Item i = new Item(params.id)
        Category c = Category.get(params.category)
        i.setName(params.name)
        i.setPrice(params.price.toDouble())
        i.setCategory(c)
        if (!i.save()){
            i.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }

    def update(){
        Item i = Item.get(params.id)
        Category c = Category.get(params.category)
        i.setName(params.name)
        i.setPrice(params.price.toDouble())
        i.setCategory(c)
        if (!i.save()){
            i.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }

    def delete(){
        Item i = Item.get(params.id)
        i.delete()
        redirect(action:"index")
    }

}
