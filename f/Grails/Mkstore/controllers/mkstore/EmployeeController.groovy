package mkstore

class EmployeeController {

    def index() {
        List employee = Employee.list()
        [employee:employee]
    }

    def newForm(){
        Employee employee = new Employee()
        List branch = Branch.list()
        [employee:employee,branch:branch]
    }

    def edit(){
        Employee employee = Employee.get(params.id)
        List branch = Branch.list()
        [employee:employee,branch:branch]
    }


    def create(){
        Employee employee = new Employee(params.id)
        Branch branch = Branch.get(params.branch)
        employee.setTitleName(params.titleName)
        employee.setFirstName(params.firstName)
        employee.setLastName(params.lastName)
        employee.setAddress(params.address)
        employee.setPhone(params.phone)
        employee.setSalary(params.salary.toDouble())
        employee.setBranch(branch)
        if (!employee.save()){
            employee.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }

    def update(){
        Employee employee = Employee.get(params.id)
        Branch branch = Branch.get(params.branch)
        employee.setTitleName(params.titleName)
        employee.setFirstName(params.firstName)
        employee.setLastName(params.lastName)
        employee.setAddress(params.address)
        employee.setPhone(params.phone)
        employee.setSalary(params.salary.toDouble())
        employee.setBranch(branch)
        if (!employee.save()){
            employee.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }

    def delete(){
        Employee employee = Employee.get(params.id)
        employee.delete()
        redirect(action:"index")
    }
}
