package lgrails

import grails.converters.JSON

class PersonController {

    def index() {

    }

    def newForm(){

    }

    def updateForm(){

    }

    def create(){ //การ Create ข้อมูล
        //Get Nose
        Nose n = Nose.get(params.nose_id)//select * from nose where id = ? //สร้าง instance ของ class nose โดยให้ class nose ไปดึงข้อมูลมาใส่ใน n
        //Get Face
        Face f = Face.get(params.face_id)//select * from face where id = ? //สร้าง instance ของ class face โดยให้ class face ไปดึงข้อมูลมาใส่ใน f
        //save
        Person p = new Person() // สร้าง instance class person
        p.setName("") //ป้อนค่าให้กับ Property name
        p.setAge() // ป้อนค่าให้กับ Property age
        p.setLastVisit(new Date()) // ป้อนค่าให้กับ Property lastvisit โดยใส่ให้เป็น เวลาปัจจุบัน
        p.setNose(n)// เป็นการป้อนค่าให้กับ Property nose
        p.setFace(f)// เป็นการป้อนค่าให้กับ Property face
        p.save() // คำสั่งเซฟข้อมุล
        redirect(controller: "person",action: "index")
    }

    def update(){ //การ Update ข้อมูล
        //save
        Person p = Person.get(params.id)//select * from person where id = ? // สร้าง instance ของ class person โดยให้ class person ไปดึงข้อมุลมา
        p.setName("")// ป้อนค่าให้ Property  name
        p.setAge()// ป้อนค่าให้ Property age
        p.setLastVisit(new Date())//ป้อนค่าให้ Property lastvisit เป็นแบบ เวลาปัจจุบัน
        p.setNose(Nose.get(params.nose_id))//ป้อนค่า Property nose โดยให้ class nose ไปดึงข้อมุลมา
        p.setFace(Face.get(params.face_id))//ป้อนค่า Property face โดยให้ class face ไปดึงข้อมุลมา
        p.save()//update person
        redirect(controller: "person",action: "index")
    }

    def delete(){ //การ Delete ข้อมูล
        Person p = Person.get(params.id)//select * from person where id = ? // สร้าง instance ของ class person ดดยให้ class person ไปดึงข้อมูลมา
        p.delete()//delete from person where id = ? //การลบข้อมุล
        redirect(controller: "person",action: "index")
    }
}
