package lgrails

class NoseController {

    def index() {

    }

    def newForm(){

    }

    def updateForm(){

    }

    def create(){
        Nose n = new Nose()// สร้าง instance ให้กับ class nose
        n.setNoseColor(params.noseColor) // ป้อนค่าให้ Property noseColor
        n.setNoseShap(params.noseShap) // ป้อนค่าให้ Property noseShap
        n.save()//update nose set nose_shap = '',nose_color='' where id = ? //เซฟข้อมุล
        redirect(controller: "nose",action: "index") //
    }

    def update(){
        Nose n = Nose.get(params.id)//select * from nose where id = ? //สร้าง instance ของ class nose โดยให้ class nose ไปดึงข้อมุลมา
        n.setNoseColor(params.noseColor) //ป้อนค่าให่ Property noseColor
        n.setNoseShap(params.noseShap)// ป้อนค่าให้ Property noseShap
        n.save()//update nose set nose_shap = '',nose_color='' where id = ? // เซฟข้อมูล
        redirect(controller: "nose",action: "index")
    }

    def delete(){
        Nose n = Nose.get(params.id)//select * from nose where id = ? //สร้าง instance ของ class nose ใโยให้ class nose ไปดึงข้อมูลมา
        n.delete()//delete from nose where id = ? //ลบข้อมูล
        redirect(controller: "nose",action: "index")
    }
}
