package lgrails

class Person {

    String name //สร้าง Property name เป็น string
    Integer age // สร้าง Property age เป็น Integer
    Date lastVisit // สร้าง Property lastVisit เป็น Date
    Nose nose // สร้าง Property nose โดยใช้ class nose เป็น datatype
    Face face // สร้าง Property face โดยใช้ class face เป็น datatype

    static constraints = {
    }

    static mapping = {//เป็นการสร้าง id ให้ class person
        id generator: 'sequence', params: [sequence:'seq_person_id']
    }
}
