package lgrails

class Face {

    String hairColor //สร้าง Property hairColor เป็น String

    static belongsTo = [person:Person] //การทำ foriegn Key กับ class person

    static constraints = {
    }

    static mapping = { //สร้าง id ให้ class face
        id generator: 'sequence', params: [sequence:'seq_face_id']
    }
}
