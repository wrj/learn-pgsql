package lgrails

class Nose {

    String noseColor // สร้าง Property noseColor
    String noseShap // สร้าง Property noseShap

    static belongsTo = [person:Person] // การทำ foriegn key กับ Class person

    static constraints = {
    }

    static mapping = { // การสร้างไอดีให้ Class nose
        id generator: 'sequence', params: [sequence:'seq_nose_id']
    }
}
