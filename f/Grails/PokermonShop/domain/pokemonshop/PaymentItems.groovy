package pokemonshop

class PaymentItems {

    static belongsTo = [item:Items,payment:Payment]

    Integer amount

    static constraints = {
    }

    static mapping = {
        id generator:'sequence', params: [sequence:'seq_payment_items_id']
    }
}
