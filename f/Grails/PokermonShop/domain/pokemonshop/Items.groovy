package pokemonshop

class Items {
    static belongsTo = [Category]
    static hasMany = [category: Category,paymentItems:PaymentItems]

    String name
    Double price
    Date createDate
    Date lastUpdate

    static constraints = {
        name blank: false
        createDate nullable: true
        lastUpdate nullable: true
    }


    def beforeInsert(){
        this.createDate = new Date()
        this.lastUpdate = new Date()
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_items_id']
    }
}
