package pokemonshop

class Category {
    static belongsTo = [branch:Branch]
    static hasMany = [items:Items]

    String name
    Date createDate
    Date lastUpdate

    static constraints = {
        name blank: false
        createDate nullable: true
        lastUpdate nullable: true
    }

    def beforeInsert(){
        this.createDate = new Date()
        this.lastUpdate = new Date()
    }


    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_category_id']
    }
}
