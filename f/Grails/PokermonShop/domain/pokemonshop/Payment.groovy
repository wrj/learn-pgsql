package pokemonshop

class Payment {
    static belongsTo = [branch:Branch]
    static hasMany = [paymentItems:PaymentItems]

    Double totalPrice
    Date createDate
    Date lastUpdate

    static constraints = {
        createDate nullable: true
        lastUpdate nullable: true
    }


    def beforeInsert(){
        this.createDate = new Date()
        this.lastUpdate = new Date()
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_payment_id']
    }
}
