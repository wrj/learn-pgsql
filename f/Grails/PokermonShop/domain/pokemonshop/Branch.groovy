package pokemonshop

class Branch {

    static hasMany = [category:Category,payments:Payment]

    String name
    Date createDate
    Date lastUpdate

    static constraints = {
        name blank: false
        createDate nullable: true
        lastUpdate nullable: true
    }

    def beforeInsert(){
        this.createDate = new Date()
        this.lastUpdate = new Date()
    }

    static mapping = {
        id generator:'sequence', params: [sequence:'seq_branch_id']
    }
}
