package pokemonshop

import grails.transaction.Transactional

import javax.persistence.criteria.CriteriaBuilder

@Transactional
class SampleService {

    Double bmi
    Double pi
    Double radius
    Integer triangle
    Integer square
    Integer result
    Integer h


    def serviceMethod() {

    }


    def bmiCalculate(Double height,Double kilograms) {
        bmi = Math.floor(kilograms / (height * height))
        println("My BMI ${bmi}")
    }

    def circle(Double num1,Double num2){
        radius = Math.pow(num1,num2)
        pi = Math.PI
        println(pi * radius)
    }



    def fever (Double num) {
        if (num >= 30.0 && num <= 42.0) {
            switch (num) {
                case {it <= 35.0}:
                    println("Body Over Cold");
                    break;
                case {it >= 36.5 && it <= 37.5}:
                    println("Normal");
                    break;
                case {it >= 37.5 && it <= 42.0}:
                    println("Sick");
                    break;
            }
        }

        else if (num >=86.0 && num <=107.60){
            switch (num){
                case {it >= 86.0 && it <= 95.0}:
                    println("Body Over Cold");
                    break;
                case {it >= 98.0 && it <= 100.0}:
                    println("Normal")
                    break;
                case {it >= 100.0 && it <= 107.60}:
                    println("Sick")
                    break;
            }
        }
    }


    def age(Integer Date,Integer month, Integer year) {
        if (year <= 2100){
            result = 2017 - year
            println("My age ${this.result}")
        }


        else if (year >= 2400 && year <= 2600){
            result = 2560 - year
            println("My age ${this.result}")
        }
    }


    def area (Integer width,Integer height,Integer heightAll){
        h = heightAll - height
        triangle = Math.floor((1 * width * h) / 2)
        square = width * height
        //วิธีที่ 1
        result = triangle + square
        println(this.result)
        //วิธีที่ 2
        println("My area = ${triangle + square}")
    }
}
