package pokemonshop

import grails.converters.JSON

import javax.swing.BoundedRangeModel

class CategoryController {

    def index() {
        List c = Category.list() // assign ค่าให้ c ไปดึงข้อมูลทั้งหมดของ Category ออกมา เป็นแบบ list
        c.each {//ทำ loop c
            println(it.getName())// แสดงข้อมุลชื่อ category
            Branch b = it.getBranch() // assign ค่าให้ b โดยให้ it(หรือ class category) ไปดึงข้อมูลของ Branch ออกมา
            println(b.getName()) //แสดงข้อมุลชื่อของ branch
            def i = it.getItems()//assign ค่าให้ i โดยให้ category ไปดึงข้อมูลของ Items ออกมาเป็นแบบ list
            i.each {//ทำ loop i
                println(it.getName())//แสดงชื่อที่ Items ดึงมา
                println(it.getPrice())//แสดงราคาที่ Items ดึงมา
            }
        }
    }

    def json(){//สร้าง action
        List b = Branch.list()//assign ค่าให้ b โดยให้ Branch ไปดึงข้อมูลออกมาเป็น list
        def listJ = []//สร้างตัวแปร List
        b.each {//ทำ loop b
            def mapJ = [:]// สร้างตัวแปร map
            Integer countItems = 0 //สร้าง property ให้ countItems เป็น Integer ให้มีค่าเท่ากับ 0
            def catList = []//สร้างตัวแปร list ไว้เก็บ Category
            def c = it.getCategory()//assign ค่าให้ c โดยให้ Branch ไปดึงข้อมูลของ Class Category ออกมา
            mapJ.put("Branch Name",it.getName())// assign ค่า ที่ Branch ไปดึงข้อมูลชื่อมาใส่ใน mapJ
            c.each {// ทำ loop c
                def catMap = [:]//สร้างตัวแปร map
                catMap.put("Category Name",it.getName())//assign ค่า ที่ Category ไปดึงข้อมูลชื่อออกมา ใสใน map
                catList.add(catMap)//นำ catMap ไปใส่ใน catList
                def itemsList = []//สร้างตัวแปร List
                def i = it.getItems()// assign ค่าให้ i โดยให้ Category ไปดึงข้อมูลของ Items
                i.each {//ทำ loop i
                    def itemsMap = [:]//สร้างตัวแปร map
                    countItems += 1 //ทำ loop ให้ countItems + 1 ไปเรื่อยๆ
                    itemsMap.put("Item Name",it.getName())//assign ค่า ที่ Item ไปดึงข้อมูลรายชื่ออกมาใส่ใน map
                    itemsMap.put("Item Price",it.getPrice())//assign ค่า ที่ Item ไปดึงข้อมูลราคาอกมาใส่ใน map
                    itemsList.add(itemsMap)//นำ itemsMap ไปใส่ใน itemsList
                }
                catMap.put("Item Amount",itemsList.size())//assign ค่า โดยให้ ItemsList ที่หาจำนวน Items ไปใส่ใน catMap
                catMap.put("Items",itemsList)//assign ค่า โดยให้ ItemsList ที่ทำข้อมูลไว้ไปใส่ใน catMap
            }
            mapJ.put("Count Items",countItems)//assign ค่าโดยหาจำนวนของ items ทั้งหมดใน Branch
            mapJ.put("Category Name",catList)//assign ค่า โดยให้ catList ที่ทำข้อมูลไว้ไปใส่ใน mapJ
            listJ.add(mapJ)//นำ mapJ ไปใส่ใน listJ
        }
        render listJ as JSON//render ไฟล์ JSON
    }





    def create(){
        //City 1
        Category c = new Category()
        Branch b = Branch.get(1)
        c.setName("Pokeball city1")
        c.setBranch(b)
        if (!c.validate()){
            c.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c.save()
        }

        Category c2 = new Category()
        Branch b2 = Branch.get(1)
        c2.setName("Potion city1")
        c2.setBranch(b2)
        if (!c2.validate()){
            c2.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c2.save()
        }



//        City2
        Category c3 = new Category()
        Branch b3 = Branch.get(2)
        c3.setName("Pokeball city2")
        c3.setBranch(b3)
        if (!c3.validate()){
            c3.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c3.save()
        }

        Category c4 = new Category()
        Branch b4 = Branch.get(2)
        c4.setName("Potion city2")
        c4.setBranch(b4)
        if (!c4.validate()){
            c4.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c4.save()
        }

        Category c5 = new Category()
        Branch b5 = Branch.get(2)
        c5.setName("TMs city2")
        c5.setBranch(b5)
        if (!c5.validate()){
            c5.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c5.save()
        }


//        City3
        Category c6 = new Category()
        Branch b6 = Branch.get(3)
        c6.setName("Pokeball city3")
        c6.setBranch(b6)
        if (!c6.validate()){
            c6.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c6.save()
        }

        Category c7 = new Category()
        Branch b7 = Branch.get(3)
        c7.setName("Potion city3")
        c7.setBranch(b7)
        if (!c7.validate()){
            c7.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c7.save()
        }

        Category c8 = new Category()
        Branch b8 = Branch.get(3)
        c8.setName("TMs city3")
        c8.setBranch(b8)
        if (!c8.validate()){
            c8.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c8.save()
        }


//        City4
        Category c9 = new Category()
        Branch b9 = Branch.get(4)
        c9.setName("Pokeball city4")
        c9.setBranch(b9)
        if (!c9.validate()){
            c9.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c9.save()
        }

        Category c10 = new Category()
        Branch b10 = Branch.get(4)
        c10.setName("Potion city4")
        c10.setBranch(b10)
        if (!c10.validate()){
            c10.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c10.save()
        }


        Category c11 = new Category()
        Branch b11 = Branch.get(4)
        c11.setName("Stone city4")
        c11.setBranch(b11)
        if (!c11.validate()){
            c11.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c11.save()
        }

        Category c12 = new Category()
        Branch b12 = Branch.get(4)
        c12.setName("Equipment city12")
        c12.setBranch(b12)
        if (!c12.validate()){
            c12.errors.allErrors.each {
                println(it)
            }
        }
        else {
            c12.save()
        }
    }





    def catItems(){
//       City1
        Category c = Category.get(1)
        Items i = Items.get(1)
        c.addToItems(i)
        if (!c.save()){
            c.errors.allErrors.each {
                println(it)
            }
        }

        Category c2 = Category.get(2)
        Items i2 = Items.get(5)
        c2.addToItems(i2)
        if (!c2.save()){
            c2.errors.allErrors.each {
                println(it)
            }
        }


//      City2
        Category c3 = Category.get(3)
        Items i3 = Items.get(1)
        c3.addToItems(i3)
        if (!c3.save()){
            c3.errors.allErrors.each {
                println(it)
            }
        }

        Category c4 = Category.get(3)
        Items i4 = Items.get(2)
        c4.addToItems(i4)
        if (!c4.save()){
            c4.errors.allErrors.each {
                println(it)
            }
        }

        Category c5 = Category.get(4)
        Items i5 = Items.get(5)
        c5.addToItems(i5)
        if (!c5.save()){
            c5.errors.allErrors.each {
                println(it)
            }
        }

        Category c6 = Category.get(4)
        Items i6 = Items.get(6)
        c6.addToItems(i6)
        if (!c6.save()){
            c6.errors.allErrors.each {
                println(it)
            }
        }

        Category c7 = Category.get(5)
        Items i7 = Items.get(9)
        c7.addToItems(i7)
        if (!c7.save()){
            c7.errors.allErrors.each {
                println(it)
            }
        }

//      City3
        Category c8 = Category.get(6)
        Items i8 = Items.get(2)
        c8.addToItems(i8)
        if (!c8.save()){
            c8.errors.allErrors.each {
                println(it)
            }
        }

        Category c9 = Category.get(6)
        Items i9 = Items.get(3)
        c9.addToItems(i9)
        if (!c9.save()){
            c9.errors.allErrors.each {
                println(it)
            }
        }

        Category c10 = Category.get(7)
        Items i10 = Items.get(5)
        c10.addToItems(i10)
        if (!c10.save()){
            c10.errors.allErrors.each {
                println(it)
            }
        }

        Category c11 = Category.get(7)
        Items i11 = Items.get(6)
        c11.addToItems(i11)
        if (!c11.save()){
            c11.errors.allErrors.each {
                println(it)
            }
        }

        Category c12 = Category.get(8)
        Items i12 = Items.get(10)
        c12.addToItems(i12)
        if (!c12.save()){
            c12.errors.allErrors.each {
                println(it)
            }
        }

        Category c13 = Category.get(8)
        Items i13 = Items.get(11)
        c13.addToItems(i13)
        if (!c13.save()){
            c13.errors.allErrors.each {
                println(it)
            }
        }


//      City4
        Category c21 = Category.get(9)
        Items i21 = Items.get(3)
        c21.addToItems(i21)
        if (!c21.save()){
            c21.errors.allErrors.each {
                println(it)
            }
        }

        Category c14 = Category.get(9)
        Items i14 = Items.get(4)
        c14.addToItems(i14)
        if (!c14.save()){
            c14.errors.allErrors.each {
                println(it)
            }
        }

        Category c15 = Category.get(10)
        Items i15 = Items.get(6)
        c15.addToItems(i15)
        if (!c15.save()){
            c15.errors.allErrors.each {
                println(it)
            }
        }

        Category c16 = Category.get(10)
        Items i16 = Items.get(7)
        c16.addToItems(i16)
        if (!c16.save()){
            c16.errors.allErrors.each {
                println(it)
            }
        }

        Category c17 = Category.get(11)
        Items i17 = Items.get(12)
        c17.addToItems(i17)
        if (!c17.save()){
            c17.errors.allErrors.each {
                println(it)
            }
        }

        Category c18 = Category.get(11)
        Items i18 = Items.get(13)
        c18.addToItems(i18)
        if (!c18.save()){
            c18.errors.allErrors.each {
                println(it)
            }
        }

        Category c19 = Category.get(11)
        Items i19 = Items.get(14)
        c19.addToItems(i19)
        if (!c19.save()){
            c19.errors.allErrors.each {
                println(it)
            }
        }

        Category c20 = Category.get(12)
        Items i20 = Items.get(15)
        c20.addToItems(i20)
        if (!c20.save()){
            c20.errors.allErrors.each {
                println(it)
            }
        }


        Category c22 = Category.get(12)
        Items i22 = Items.get(16)
        c22.addToItems(i22)
        if (!c22.save()){
            c22.errors.allErrors.each {
                println(it)
            }
        }
    }



}









