package pokemonshop

import grails.converters.JSON


class PaymentItemsController {

    def index() {
        List p = PaymentItems.list()//assign ค่าให้ p โดยให้ PaymentItems ไปดึงข้อมุลทั้งหมดเป็นแบบ list
        p.each {//ทำ loop p
            println(it.getAmount())//แสดงข้อมุลจำนวน ที่ it(คือ p ที่เป็น instance) ไปดึงข้อมูลออกมา
            Items i = it.getItem()//สร้าง instance เป็น i โดยให้ it ไปดึงข้อมุลของ class items
            Payment pa = it.getPayment()//สร้าง instance เป็น pa โดยให้ it ไปดึงข้อมุลของ class Payment
            Branch b = pa.getBranch()////สร้าง instance เป็น b โดยให้ pa (ที่เป็น instance ของ Payment) ไปดึงข้อมุลของ class Branch
            println(i.getName())//ให้ i แสดงข้อมุลชื่อออกมา
            println(i.getPrice())//ให้ i แสดงข้อมุลราคาออกมา
            println(pa.getTotalPrice())//ให้ pa แสดงข้อมุลยอดรวมออกมา
            println(b.getName())//ให้ b แสดงข้อมุลชื่อออกมา
        }
    }



    def Json(){//ทำ action Json
        List p = Payment.list()//assign ค่าให้ p โดยให้ Payment ไปดึงข้อมูลออกมา เป็น list
        def list = []//กำหนดตัวแปรเป็น list
        p.each {//ทำ loop p
            def mapJ = [:]// กำหนดตัวแปรเป็น map
            Branch b = it.getBranch()//สร้าง instance ของ Branch โดยให้ Payment ไปดึงข้อมูลของ class Branch
            mapJ.put("Branch Name",b.getName())//assign key:value ของ branch ลงตัวแปร map
            mapJ.put("TotalPrice",it.getTotalPrice())// assign key:value โดยให้ Payment ไปดึงข้อมูล ยอดรวมออกมาใส่ในตัวแปร map
            def itemList = []//สร้างตัวแปรเป็น list
            def i = it.getPaymentItems()//assign ค่าให้ i โดยให้ Payment ไปดึงข้อมูลของ Class PaymentItems ออกมาเป็น list
            i.each {//ทำ loop i
                def mapItems = [:] // สร้างตัวแปร map
                Items items = it.getItem()//สร้าง instance ของ items โดยให้ PaymentItems ไปดึงข้อมุลของ Items ออกมา
                mapItems.put("Items Name",items.getName())//assign key;value ของ Items ให้กับตัวแปร map
                mapItems.put("Items Price",items.getPrice())//assign key:value ของ Items ให้ตัวแปร map
                mapItems.put("Amount",it.getAmount())// assign key:value โดยให้ PaymentItems ไปดึงข้อมูล amount มาใส่ตัวแปร map
                itemList.add(mapItems)//เอา mapItems ไปใส่ใน itemList
            }
            mapJ.put("Items",itemList)//assign key:value ของ Items ใส่ในตัวแปร map

            list.add(mapJ)//นำ mapJ ไปใส่ใน ตัวแปร List
        }

        render list as JSON//render ไฟล์เป็น Json
    }




    def create() {
        Payment p = Payment.get(1)
        Items i = Items.get(1)
        PaymentItems pi = new PaymentItems()
        pi.setAmount(4)
        pi.setPayment(p)
        pi.setItem(i)
        if (!pi.save()){
            pi.errors.allErrors.each {
                println(it)
            }
        }

        Payment p2 = Payment.get(1)
        Items i2 = Items.get(5)
        PaymentItems pi2 = new PaymentItems()
        pi2.setAmount(1)
        pi2.setPayment(p2)
        pi2.setItem(i2)
        if (!pi2.save()){
            pi2.errors.allErrors.each {
                println(it)
            }
        }


        Payment p3 = Payment.get(2)
        Items i3 = Items.get(1)
        PaymentItems pi3 = new PaymentItems()
        pi3.setAmount(2)
        pi3.setPayment(p3)
        pi3.setItem(i3)
        if (!pi3.save()){
            pi3.errors.allErrors.each {
                println(it)
            }
        }

        Payment p4 = Payment.get(2)
        Items i4 = Items.get(2)
        PaymentItems pi4 = new PaymentItems()
        pi4.setAmount(2)
        pi4.setPayment(p4)
        pi4.setItem(i4)
        if (!pi4.save()){
            pi4.errors.allErrors.each {
                println(it)
            }
        }

        Payment p5 = Payment.get(2)
        Items i5 = Items.get(5)
        PaymentItems pi5 = new PaymentItems()
        pi5.setAmount(2)
        pi5.setPayment(p5)
        pi5.setItem(i5)
        if (!pi5.save()){
            pi5.errors.allErrors.each {
                println(it)
            }
        }


        Payment p6 = Payment.get(3)
        Items i6 = Items.get(1)
        PaymentItems pi6 = new PaymentItems()
        pi6.setAmount(1)
        pi6.setPayment(p6)
        pi6.setItem(i6)
        if (!pi6.save()){
            pi6.errors.allErrors.each {
                println(it)
            }
        }

        Payment p7 = Payment.get(3)
        Items i7 = Items.get(2)
        PaymentItems pi7 = new PaymentItems()
        pi7.setAmount(5)
        pi7.setPayment(p7)
        pi7.setItem(i7)
        if (!pi7.save()){
            pi7.errors.allErrors.each {
                println(it)
            }
        }


        Payment p8 = Payment.get(4)
        Items i8 = Items.get(3)
        PaymentItems pi8 = new PaymentItems()
        pi8.setAmount(2)
        pi8.setPayment(p8)
        pi8.setItem(i8)
        if (!pi8.save()){
            pi8.errors.allErrors.each {
                println(it)
            }
        }

        Payment p9 = Payment.get(4)
        Items i9 = Items.get(6)
        PaymentItems pi9 = new PaymentItems()
        pi9.setAmount(3)
        pi9.setPayment(p9)
        pi9.setItem(i9)
        if (!pi9.save()){
            pi9.errors.allErrors.each {
                println(it)
            }
        }

        Payment p10 = Payment.get(4)
        Items i10 = Items.get(10)
        PaymentItems pi10 = new PaymentItems()
        pi10.setAmount(1)
        pi10.setPayment(p10)
        pi10.setItem(i10)
        if (!pi10.save()){
            pi10.errors.allErrors.each {
                println(it)
            }
        }
    }
}
