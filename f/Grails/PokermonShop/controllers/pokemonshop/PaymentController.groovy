package pokemonshop

class PaymentController {

    def index() {
        List p = Payment.list()// assign ค่าให้ p โดยให้ Payment ไปดึงข้อมูลออกมาเป็น list
        p.each {// ทำ loop p
            println(it.getTotalPrice())//แสดงข้อมูลของยอดรวม ของ Payment
            Branch b = it.getBranch()// สร้าง instance ให้ b โดยให้ it (คือ p ที่เป็น instance ของ Payment) ไปดึงข้อมูลของ Class Branch
            println(b.getName())//แสดงข้อมูลชื่อของ Branch
        }
    }



    def create(){
        Payment p = new Payment()
        Branch b = Branch.get(1)
        p.setTotalPrice(2500)
        p.setBranch(b)
        if (!p.save()){
            p.errors.allErrors.each {
                print(it)
            }
        }


        Payment p2 = new Payment()
        Branch b2 = Branch.get(2)
        p2.setTotalPrice(5000)
        p2.setBranch(b2)
        if (!p2.save()){
            p2.errors.allErrors.each {
                print(it)
            }
        }

        Payment p3 = new Payment()
        Branch b3 = Branch.get(2)
        p3.setTotalPrice(8000)
        p3.setBranch(b3)
        if (!p3.save()){
            p3.errors.allErrors.each {
                print(it)
            }
        }

        Payment p4 = new Payment()
        Branch b4 = Branch.get(3)
        p4.setTotalPrice(12500)
        p4.setBranch(b4)
        if (!p4.save()){
            p4.errors.allErrors.each {
                print(it)
            }
        }
    }
}
