package pokemonshop

class ItemsController {

    def index() {
        List i = Items.list() // assign ค่าให้ i โดยใให้ไปดึงข้อมูลของ Item ออกมา เป็นแบบ List
        i.each {//ทำ loop i
            println(it.getName())//แสดงข้อมูลรายชื่อของ Items
            println(it.getPrice())//แสดงข้อมูลราคาของ Items
        }
    }

    def create(){
        Items i = new Items()
        i.setName("Poke ball")
        i.setPrice(500)
        if (!i.save()){
            i.errors.allErrors.each {
                println(it)
            }
        }

        Items i2 = new Items()
        i2.setName("Great ball")
        i2.setPrice(1500)
        if (!i2.save()){
            i2.errors.allErrors.each {
                println(it)
            }
        }


        Items i3 = new Items()
        i3.setName("Fast ball")
        i3.setPrice(2500)
        if (!i3.save()){
            i3.errors.allErrors.each {
                println(it)
            }
        }

        Items i4 = new Items()
        i4.setName("Master ball")
        i4.setPrice(5000)
        if (!i4.save()){
            i4.errors.allErrors.each {
                println(it)
            }
        }

        Items i5 = new Items()
        i5.setName("Potion")
        i5.setPrice(500)
        if (!i5.save()){
            i5.errors.allErrors.each {
                println(it)
            }
        }

        Items i6 = new Items()
        i6.setName("Max Potion")
        i6.setPrice(2000)
        if (!i6.save()){
            i6.errors.allErrors.each {
                println(it)
            }
        }

        Items i7 = new Items()
        i7.setName("Super Potion")
        i7.setPrice(1500)
        if (!i7.save()){
            i7.errors.allErrors.each {
                println(it)
            }
        }

        Items i8 = new Items()
        i8.setName("Revive")
        i8.setPrice(1500)
        if (!i8.save()){
            i8.errors.allErrors.each {
                println(it)
            }
        }

        Items i9 = new Items()
        i9.setName("TM01")
        i9.setPrice(1500)
        if (!i9.save()){
            i9.errors.allErrors.each {
                println(it)
            }
        }

        Items i10 = new Items()
        i10.setName("TM03")
        i10.setPrice(1500)
        if (!i10.save()){
            i10.errors.allErrors.each {
                println(it)
            }
        }

        Items i11 = new Items()
        i11.setName("TM06")
        i11.setPrice(2000)
        if (!i11.save()){
            i11.errors.allErrors.each {
                println(it)
            }
        }

        Items i12 = new Items()
        i12.setName("Ligning Stone")
        i12.setPrice(3000)
        if (!i12.save()){
            i12.errors.allErrors.each {
                println(it)
            }
        }

        Items i13 = new Items()
        i13.setName("Fire Stone")
        i13.setPrice(3000)
        if (!i13.save()){
            i13.errors.allErrors.each {
                println(it)
            }
        }

        Items i14 = new Items()
        i14.setName("Water Stone")
        i14.setPrice(3000)
        if (!i14.save()){
            i14.errors.allErrors.each {
                println(it)
            }
        }

        Items i15 = new Items()
        i15.setName("Rope")
        i15.setPrice(1500)
        if (!i15.save()){
            i15.errors.allErrors.each {
                println(it)
            }
        }

        Items i16 = new Items()
        i16.setName("Magnet")
        i16.setPrice(1000)
        if (!i16.save()){
            i16.errors.allErrors.each {
                println(it)
            }
        }

        Items i17 = new Items()
        i17.setName("Plate")
        i17.setPrice(1000)
        if (!i17.save()){
            i17.errors.allErrors.each {
                println(it)
            }
        }
    }
}
