package pokemonshop

class BmiController {

    def sampleService

    def index() {
        sampleService.bmiCalculate(1.65,64)
        sampleService.circle(4,2)
        sampleService.fever(40.0)
        sampleService.age(23,01,2534)
        sampleService.area(8,6,12)
    }
}
