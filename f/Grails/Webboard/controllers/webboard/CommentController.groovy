package webboard

class CommentController {

    def index() {
        List c = Comment.getAll()
        [comment:c]
    }

    def newForm(){
        Comment c = new Comment()
        [comment:c]
    }

    def updateForm(){
        Comment c = Comment.get(params.id)
        [comment:c]

    }

    def create() {
        Comment c = new Comment()
        c.setDescription(params.description)
        if (!c.validate()){
            c.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error"
        }
        else {
            c.save()
            flash.massage = "Update Complete"
        }
    }


    def update(){
        Comment c = Comment.get(params.id)
        c.setDescription(params.id)
        if (!c.save()){
            c.errors.allErrors.each {
                println(it)
            }
            flash.error = 'Update Error'
        }
        else{
            flash.massage = "Update Complete"
        }
    }


    def delete(){
        Comment c = Comment.get(params.id)
        c.delete()
    }
}
