package webboard

import org.jboss.logging.annotations.Param

class CategoryController {

    def index() {
        List c = Category.list()
        render (view: "index", model: [category:c])
    }

    def newForm() {
        Category c = new Category()
        render (view:"newForm", model: [category:c])
    }

    def updateForm() {
        Category c = Category.get(params.id)
        render (view:"updateForm", model: [category:c])
    }

    def create() {
//        Category c = new Category()
//        c.setName(params.name)
//        c.setImage(params.image)
//        c.setDescription(params.description)
//        if(!c.save())
//        {
//            c.errors.allErrors.each {
//                println(it)
//            }
//            flash.error = "Create Error."
//        }else{
//            flash.message = "Create Complete."
//        }
//        redirect(action:"index")




        Category c = new Category()
        c.setName("Movie")
        c.setImage("Movie Hit")
        c.setDescription("Coming soon")
        if(!c.save())
        {
            c.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error."
        }else{
            flash.message = "Create Complete."
        }


        Category c2 = new Category()
        c2.setName("Kitchen")
        c2.setImage("Food")
        c2.setDescription("Food / Bakery")
        if(!c2.save())
        {
            c2.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error."
        }else{
            flash.message = "Create Complete."
        }


        Category c3 = new Category()
        c3.setName("Love")
        c3.setImage("Love")
        c3.setDescription("Romantic")
        if(!c3.save())
        {
            c3.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error."
        }else{
            flash.message = "Create Complete."
        }

        Category c4 = new Category()
        c4.setName("Travel")
        c4.setImage("Fun")
        c4.setDescription("Tours and Travel")
        if(!c4.save())
        {
            c4.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error."
        }else{
            flash.message = "Create Complete."
        }

    }

    def update() {
        Category c = Category.get(params.id)
        c.setName(params.name)
        c.setImage(params.image)
        c.setDescription(params.description)
        if(!c.save())
        {
            c.errors.allErrors.each {
                println(it)
            }
            flash.error = "Update Error."
        }else{
            flash.message = "Update Complete."
        }
        redirect(action:"index")
    }

    def delete() {
        Category c = Category.get(params.id)
        c.delete()
        redirect(action: "index")
    }
}
