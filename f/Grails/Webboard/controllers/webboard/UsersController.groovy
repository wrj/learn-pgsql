package webboard

class UsersController {

    def index() {
        List u = Users.getAll() //assign ค่าให้ u เป็น list เพื่อดึงข้อมูล users ออกมาทั้งหมด
        render (view: "index", model: [u:u])
    }

    def newForm(){
        Users users = new Users()
        render (view: "newForm", model: [users:users])
    }

    def updateForm(){
        Users users = Users.get(params.id)
        render (view: "updateForm", model: [users:users])
    }

    def create() {
        Users u = new Users()
        u.setUsername(params.username)
        u.setPassword(params.password)
        u.setImage(params.image)
        u.setEmail(params.email)
        u.setTitleName(params.titleName)
        u.setFirstName(params.firstName)
        u.setLastName(params.lastName)
        u.setDescription(params.description)
        u.setGender(params.gender)
        u.setBirthDate(new Date())
        u.setCreateDate(new Date())
        u.setLastUpdate(new Date())
        u.setUserlevel(0)
        u.setActive(true)
        if (!u.validate()){
            u.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error"
        }
        else {
            u.save()
            flash.massage = "Create Complete"
        }
        redirect(action:"index")
    }

    def update(){
        Users u = Users.get(params.id)
        u.setPassword(params.password)
        u.setImage(params.image)
        u.setTitleName(params.titleName)
        u.setFirstName(params.firstName)
        u.setLastName(params.lastName)
        u.setDescription(params.description)
        u.setGender(params.gender)
        u.setBirthDate(params.birthDate)
        if (!u.save()){
            u.errors.allErrors.each {
                println(it)
            }
            flash.error = "Update Error"
        }
        else {
            flash.massage = "Update Commplete"
        }
    }
}
