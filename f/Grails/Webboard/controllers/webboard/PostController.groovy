package webboard

class PostController {

    def index() {
        List p = Post.list()
        [post:p]
    }

    def newForm() {
        Post p = new Post()
        List c = Category.list()
        List t = Tag.list()
        [post:p,category:c,tag:t]
    }

    def create() {
        println(params)
        Users u = Users.get(1)
        Post p = new Post()
        Category c = Category.get(params.category)
        p.setTitlePost(params.titlePost)
        p.setDescription(params.description)
        p.setCategory(c)
        p.setUser(u)
        p.setView(1)
        p.setCreateDate(new Date())
        p.setLastUpdate(new Date())
        if (!p.save()){
            p.errors.allErrors.each {
                println(it)
            }
        }
        else{
//            println "save"
            params.tag.each{
                println(it)
                Tag t = Tag.get(it)
                PostTags pt = new PostTags()
                pt.setTag(t)
                pt.setPost(p)
                if (!pt.save()){
                    pt.errors.allErrors.each {
                        println(it)
                    }
                }
            }
//            if(params.containsKey("tag") && params.tag != "") {
//                println "in tag"
//                Tag t = Tag.get(params.tag)
//                PostTags pt = new PostTags()
//                pt.setPost(p)
//                pt.setTag(t)
//                pt.save()
//            }
        }
        redirect(action:"index")
    }
}
