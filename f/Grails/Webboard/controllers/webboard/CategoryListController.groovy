package webboard

import org.h2.engine.User

class CategoryListController {

    def index() { // สร้าง action นี้เพื่อดึงข้อมูล Category
        List category = Category.list() //assign ค่าให้ category โดยให้ Category ไปดึงข้อมุลออกมาเป้น list
        [category:category]// สร้าง view
    }

    def list(){ // สร้าง action นี้เพื่อแสดงข้อมูลของ post
        List post = Post.findAllByCategory(Category.get(params.id))// assign ค่าให้ post โดยให้ Post ไปดึงข้อมุลจาก category มาแสดงแบบ list
        [post:post]//สร้าง view
    }

    def post(){// สร้าง action เพื่อแสดงข้อมูลของ post
        Post post = Post.get(params.id)//assign ค่า post โดยให้ Post ไปดึงข้อมุลของ Post ไปดึงข้อมูล params.id มา
        post.increaseView()//เรียก method มาใช้
        List comment = Comment.findAllByPost(post)//assign ค่าให้ comment โดยให้ Comment ไปดึงข้อมูลของ Post ออกมาเป้น list
        [post:post,comment:comment]//สร้าง view
    }

    def reply(){//สร้าง action นี้เพื่อใช้ comment
        Users user = Users.get(1)// assign ค่าให้ user โดยให้ Users ไปดึงข้อมุลที่ id 1 ออกมา
        Post post = Post.get(params.id)//assign ค่าให้ post โดยให้ Post ไปดึงข้อมูลที่ params.id
        Comment comment = new Comment()// สร้าง instance ของ Comment
        comment.setDescription(params.description)//ป้อนค่า property ให้ comment
        comment.setPost(post)// ป้อนค่าให้ property Post ที่ instance ของ post ดึงค่ามาใส่ ใส่ comment
        comment.setUser(user)// ป้อนค่าให้ property User ที่ instance ของ user ดึงค่ามาใส่ ใส่ comment
        comment.save()//สั่ง save ข้อมูล
        redirect(action:"post",id:params.id)//เมื่อ save แล้วให้ กลับไปที่ action post
    }
}
