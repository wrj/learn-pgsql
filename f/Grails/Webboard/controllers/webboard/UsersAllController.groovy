package webboard

import org.h2.engine.User

class UsersAllController {

    def index() {
        List user = Users.list()
        [users:user]
    }

    def newForm(){
        Users user = new Users()
        [users:user]
    }


    def updateForm(){
        Users user = Users.get(params.id)
        [users:user]
    }



    def create(){
        Users user = new Users(params)
        println(params)
        if (!user.save()){
            user.errors.allErrors.each {
                println(it)
            }
        }
        redirect(action:"index")
    }
}
