package webboard

class BoardController {

    def index() {//สร้าง action
        List c = Category.list() // assign ค่าให้ c เป็น list ไปดึงข้อมูลของ category ทั้งหมด
        [category:c]// create render view ชื่อ Property category เป็น c
    }

    def list() {//สร้าง action
        List p = Post.findAllByCategory(Category.get(params.id))//assign ค่าให้ p เป็น list ไปดึงข้อมุล post โดยหาจาก id category
        [post:p]// create  render view  ชื่อ property post เป็น p
    }

    def post() { // สร้าง action
        Post p = Post.get(params.id) // สร้าง instance ให้ p ไปดึงข้อมุล id ของ post
        p.increaseView() //ให้ p ใช้ method increaseView
        List c = Comment.findAllByPost(p)// assign ค่าให้ c เป็น เป็น list โดยให้ comment ไปดึงข้อมุล id ของ post
        [post:p,comment:c] // create render view ของ post และ comment
    }

    def addReply()// สร้าง action
    {
        Users u = Users.get(1) // สร้าง instance ให้ u ไปดึงข้อมูลของ users id ที่ 1
        Post p = Post.get(params.id) // สร้าง instance ให้ p โดยให้ post ไปดึงข้อมูลของ id ที่ params
        Comment c = new Comment() //สร้าง instance comment เป็น c
        c.setDescription(params.description) //ป้อนค่าให้ Property ของ c โดยเป็นการกรอก form
        c.setUser(u)//ป้อนค่าให้ property ขแง c ดึงข้อมูลของ u(users) มาใส่
        c.setPost(p)// ป้อนค่าให้ Property ของ c ดึงข้อมูลของ p(post) มาใส่
        c.setCreateDate(new Date())// ป้อนค่าให้ property เป้น วันที่ปัจจุบัน
        c.setLastUpdate(new Date())// ป้อนค่าให้ property เป้น วันที่ปัจจุบัน
        c.save()//ให้save
        redirect(action:"post",id:params.id)// สั่งให้กลับไปทำงานที่ action post
    }
}
