package webboard

class BMainController {

    def index() {
        List c = Category.getAll()
        [cat:c]
    }

    def list(){
        List p = Post.findAllByCategory(Category.get(params.id))
        [post:p]
    }

    def post(){
        Post p = Post.get(params.id)
        p.increaseView()
        List c = Comment.findAllByPost(p)
        [post:p,comment:c]
    }
}
