package webboard

class TagController {

    def index() {
        List t = Tag.getAll()
        [tag:t]
    }

    def newForm(){
        Tag t = new Tag()
        [tag:t]
    }

    def updateForm(){
        Tag t = Tag.get(params.id)
        [tag: t]
    }

//    def create() {
//        Tag t = new Tag()
//        t.setName(params.name)
//        if (!t.validate()){
//            t.errors.allErrors.each {
//                println(it)
//            }
//            flash.error = "Create Error"
//        }
//        else {
//            t.save()
//            flash.massage = "Create Complete"
//        }
//    }





    def create() {
        Tag t = new Tag()
        t.setName("Love")
        if (!t.validate()){
            t.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error"
        }
        else {
            t.save()
            flash.massage = "Create Complete"
        }

        Tag t2 = new Tag()
        t2.setName("Happy")
        if (!t2.validate()){
            t2.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error"
        }
        else {
            t2.save()
            flash.massage = "Create Complete"
        }


        Tag t3 = new Tag()
        t3.setName("Book")
        if (!t3.validate()){
            t3.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error"
        }
        else {
            t2.save()
            flash.massage = "Create Complete"
        }

        Tag t4 = new Tag()
        t4.setName("Food")
        if (!t4.validate()){
            t4.errors.allErrors.each {
                println(it)
            }
            flash.error = "Create Error"
        }
        else {
            t4.save()
            flash.massage = "Create Complete"
        }

    }






    def update(){
        Tag t = Tag.get(params.id)
        t.setName(params.name)
        if (!t.save()){
            t.errors.allErrors.each {
                println(it)
            }
            flash.error = "Update Error"
        }
        else {
            flash.massage = "Update Complete"
        }
    }

    def delete(){
        Tag t = Tag.get(params.id)
        t.delete()
    }
}
