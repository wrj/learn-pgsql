<!doctype html>
<html>
<head>
    <title>My Web Board : Category</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
<g:form action="create">
    <div class="form-group">
        <label for="exampleInputTitleName">TitleName</label>
        <input type="input" class="form-control" name="titleName" id="exampleInputTitleName" placeholder="TitleName">
    </div>
    <div class="form-group">
        <label for="exampleInputFirstName">FirstName</label>
        <input type="input" class="form-control" name="firstName" id="exampleInputFirstName" placeholder="FirstName">
    </div>
    <div class="form-group">
        <label for="exampleInputLastName">LastName</label>
        <input type="input" class="form-control" name="lastName" id="exampleInputLastName" placeholder="LastName">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Email">
    </div>
    <div class="form-group">
        <label for="exampleInputUserName">Username</label>
        <input type="input" class="form-control" name="username" id="exampleInputUserName" placeholder="Username">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword">Password</label>
        <input type="password" class="form-control" name="password" id="exampleInputPassword" placeholder="Password">
    </div>
    <div class="form-group">
        <label for="exampleInputFile">File input</label>
        <input type="file" id="exampleInputFile">
        <p class="help-block">Example block-level help text here.</p>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox"> Check me out
        </label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</g:form>

</body>
</html>
