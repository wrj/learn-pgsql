<!doctype html>
<html>
<head>
    <title>My Web Board : Post > ${post.getTitlePost()}</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
<table width="100%">
    <tr>
        <td>
            ${post.getDescription()}
        </td>
    </tr>
    <g:each in="${comment}">
        <tr>
            <td>${it.getDescription()}</td>
        </tr>
    </g:each>
</table>
<g:form action="Reply" id="${post.getId()}" method="POST">
    <g:textArea name="description"/>
    <g:submitButton name="Reply"/>
</g:form>
</body>
</html>
