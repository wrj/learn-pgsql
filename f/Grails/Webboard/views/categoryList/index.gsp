<html>
<head>
    <title>My Web Board : Category2</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
<table>
    <tr>
        <td>Category : </td>
        <td>
            <select multiple name="category">
                <g:each in="${category}">
                    <option value="${it.getId()}">${it.getName()}</option>
                </g:each>
            </select>
        </td>
    </tr>
    <tr>
        <td>Category : </td>
        <td>
            <select name="category">
                <g:each in="${category}">
                    <option value="${it.getId()}">${it.getName()}</option>
                </g:each>
            </select>
        </td>
    </tr>
</table>

<table width="100%">
    <tr>
        <g:each in="${category}">
            <td>
            <a href="<g:createLink action="list" id="${it.getId()}"/>">${it.getName()}</a>
            </td>
        </g:each>
    </tr>
</table>

</body>
</html>