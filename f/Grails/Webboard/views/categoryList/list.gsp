<html>
<head>
    <title>My Web Board : Category2</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
<table width="100%">
    <tr>
        Post List
    </tr>

    <g:each in="${post}">
        <tr>
            <td><a href="<g:createLink action="post" id="${it.getId()}"/>">${it.getTitlePost()}</a></td>
            <td>View : ${it.getView()}</td>
            <td>Reply : ${it.getComments().size()}</td>
        </tr>
    </g:each>
</table>
</body>
</html>