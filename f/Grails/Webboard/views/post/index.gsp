<!doctype html>
<html>
<head>
    <title>My Web Board : Post</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
<style>
    .message {
        border: 1px solid green;
        background-color: #4cae4c;
    }
    .alert-error {
        border: 1px solid red;
        background-color: red;
    }
</style>
    <a href="<g:createLink action="newForm" />">New</a>
<g:if test="${flash.error}">
    <div class="alert alert-error" style="display: block">${flash.error}</div>
</g:if>
<g:if test="${flash.message}">
    <div class="message" style="display: block">${flash.message}</div>
</g:if>
<table width="100%">

        <g:each in="${post}">
        <tr>
            <td>${it.getTitlePost()}</td>
            <td>
                <a href="<g:createLink action="updateForm" id="${it.getId()}"/>">Edit</a>
                <a href="<g:createLink action="delete" id="${it.getId()}"/>">Delete</a>
            </td>
        </tr>
        </g:each>
</table>


</body>
</html>
