<table>
    <tr>
        <td>Title Post : </td>
        <td><g:textField name="titlePost" value="${post.getTitlePost()}"/></td>
    </tr>
    <tr>
        <td>Description : </td>
        <td><g:textArea name="description" value="${post.getDescription()}"/></td>
    </tr>
    <tr>
        <td>Category : </td>
        <td>
            <select name="category">
                <g:each in="${category}">
                    <option value="${it.getId()}">${it.getName()}</option>
                </g:each>
            </select>
        </td>
    </tr>
    <tr>
        <td>Tag : </td>
        <td>
            <select multiple name="tag">
                <g:each in="${tag}">
                    <option value="${it.getId()}">${it.getName()}</option>
                </g:each>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <g:if test="${post.getId() == null}">
                <g:submitButton name="Save"></g:submitButton>
            </g:if>
            <g:else>
                <g:submitButton name="Update"></g:submitButton>
            </g:else>

        </td>
    </tr>
</table>