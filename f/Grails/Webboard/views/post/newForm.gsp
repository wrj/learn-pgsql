<!doctype html>
<html>
<head>
    <title>My Web Board : Form Post</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
<a href="<g:createLink action="index" />">Back</a>
<g:form action="create" method="POST">
    <g:render template="form" model="[post: post,category:category,tag:tag]" />
</g:form>
</body>
</html>
