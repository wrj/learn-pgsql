<table>
    <tr>
        <td>Username : </td>
        <td><g:textField name="username" value="${users.getUsername()}"/></td>
    </tr>
    <tr>
        <td>Password : </td>
        <td><g:passwordField name="password" value=""/></td>
    </tr>
    %{--<tr>--}%
        %{--<td>Image : </td>--}%
        %{--<td><g:textField name="image" value="${users.getImage()}"/></td>--}%
    %{--</tr>--}%
    <tr>
        <td>Email : </td>
        <td><g:textField name="email" value="${users.getEmail()}"/></td>
    </tr>
    %{--<tr>--}%
        %{--<td>Gender : </td>--}%
        %{--<td>--}%
            %{--<g:radioGroup name="gender"--}%
                          %{--labels="['ชาย','หญิง']"--}%
                          %{--values="['male','female']" value="${users.getGender()?:'male'}" >--}%
                %{--${it.label} ${it.radio}--}%
            %{--</g:radioGroup>--}%
        %{--</td>--}%
    %{--</tr>--}%
    <tr>
        <td>Title : </td>
        <td>
            <select name="titleName">
                <option value="Mr." <g:if test="${users.getTitleName() == 'Mr.'}">selected</g:if>>นาย</option>
                <option value="Ms." <g:if test="${users.getTitleName() == 'Ms.'}">selected</g:if>>นาง</option>
                <option value="Miss." <g:if test="${users.getTitleName() == 'Miss.'}">selected</g:if>>นางสาว</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Firstname : </td>
        <td><g:textField name="firstName" value="${users.getFirstName()}"/></td>
    </tr>
    <tr>
        <td>Lastname : </td>
        <td><g:textField name="lastName" value="${users.getLastName()}"/></td>
    </tr>
    %{--<tr>--}%
        %{--<td>Description : </td>--}%
        %{--<td><g:textArea name="description" value="${users.getDescription()}"/></td>--}%
    %{--</tr>--}%
    <tr>
        <td colspan="2">
            <g:if test="${users.getId() == null}">
                <g:submitButton name="Save"></g:submitButton>
            </g:if>
            <g:else>
                <g:submitButton name="Update"></g:submitButton>
            </g:else>

        </td>
    </tr>
</table>