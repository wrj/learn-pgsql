<!doctype html>
<html>
<head>
    <title>My Web Board : Form Users</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<style>
    #myBody {
        background-color: black;
        width: 1024px;
        height: 768px;
    }
</style>
<body id="myBody">
<a href="<g:createLink action="index" />">Back</a>
<g:form action="create" method="POST">
    <g:render template="form" model="[users: users]" />
</g:form>
</body>
</html>
