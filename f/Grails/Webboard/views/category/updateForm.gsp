<!doctype html>
<html>
<head>
    <title>My Web Board : Form Category</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
<a href="<g:createLink action="index" />">Back</a>
<g:form action="update" id="${category.getId()}" method="POST">
    <g:render template="form" model="[category: category]" />
</g:form>
</body>
</html>
