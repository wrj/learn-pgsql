<table>
    <tr>
        <td>Name : </td>
        <td><g:textField name="name" value="${category.getName()}"/></td>
    </tr>
    <tr>
        <td>Description : </td>
        <td><g:textArea name="description" value="${category.getDescription()}"/></td>
    </tr>
    <tr>
        <td>Image : </td>
        <td><g:textField name="image" value="${category.getImage()}"/></td>
    </tr>
    <tr>
        <td colspan="2">
            <g:if test="${category.getId() == null}">
                <g:submitButton name="Save"></g:submitButton>
            </g:if>
            <g:else>
                <g:submitButton name="Update"></g:submitButton>
            </g:else>

        </td>
    </tr>
</table>