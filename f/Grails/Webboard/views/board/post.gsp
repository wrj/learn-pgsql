<!doctype html>
<html>
<head>
    <title>My Web Board : Post > ${post.getTitlePost()}</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
<style>
.message {
    border: 1px solid green;
    background-color: #4cae4c;
}
.alert-error {
    border: 1px solid red;
    background-color: red;
}
</style>
<g:if test="${flash.error}">
    <div class="alert alert-error" style="display: block">${flash.error}</div>
</g:if>
<g:if test="${flash.message}">
    <div class="message" style="display: block">${flash.message}</div>
</g:if>
<table width="100%">
    <tr>
        <td>
            ${post.getDescription()}
        </td>
    </tr>
    <g:each in="${comment}">
        <tr>
            <td>${it.getDescription()}</td>
        </tr>
    </g:each>
</table>
<g:form action="addReply" id="${post.getId()}" method="POST">
<g:textArea name="description"/>
    <g:submitButton name="Reply"/>
</g:form>
</body>
</html>
