package webboard

class PostTags implements Serializable{

    static belongsTo = [tag:Tag,post:Post]// Domain นี้เก็บ id ของ post และ tag

    static constraints = {
    }

    static mapping = {
        id composite: ['tag','post'] //สั่งให้ไปดึง id มาจาก Domain tag และ post
    }

}
