package webboard

class Tag {

    static hasMany = [posttags:PostTags] //หนึ่ง tag อยู่ได้หลาย post (ซึ่งถูกเก็บอยู่ใน posttags)

    String name
    Date createDate
    Date lastUpdate

    static constraints = {
        name blank: false
        createDate nullable: true
        lastUpdate nullable: true
    }

    def beforeInsert()
    {
        this.createDate = new Date() // สั่งให้ Property createDate ของ Domain Category เป็นวันที่ปัจจุบัน
        this.lastUpdate = new Date() // สั่งให้ Property lastUpdate ของ Domain Category เป็นวันที่ปัจจุบัน
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_tags_id']
    }
}
