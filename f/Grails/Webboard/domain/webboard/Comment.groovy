package webboard

class Comment {

    static belongsTo = [user:Users,post:Post] // หนึ่ง Comment มีได้หนึ่ง user กับหนึ่ง post

    String description //สร้าง property description เป็น String
    Date createDate
    Date lastUpdate


    static constraints = {
        description blank: false //บอกว่า description ห้ามว่างใน ("")
        createDate nullable: true
        lastUpdate nullable: true
    }

    def beforeInsert()
    {
        this.createDate = new Date() // สั่งให้ Property createDate ของ Domain Category เป็นวันที่ปัจจุบัน
        this.lastUpdate = new Date() // สั่งให้ Property lastUpdate ของ Domain Category เป็นวันที่ปัจจุบัน
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_comments_id'] //generate id
    }
}
