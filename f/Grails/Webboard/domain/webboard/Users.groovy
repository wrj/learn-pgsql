package webboard

import com.sun.org.apache.xpath.internal.operations.Bool

class Users {


    static hasMany = [posts:Post,comments:Comment] // หนึ่ง users มีได้หลาย posts และ หลาย comment

    String username // สร้าง Property name ให่เป็น String
    String password
//    String image
    String email
    String titleName
    String firstName
    String lastName
//    String gender
//    Date birthDate
//    Boolean active
//    Integer userlevel
//    String description
    Date createDate
    Date lastUpdate

    static constraints = {
        username blank: false
        password blank: false
//        image blank: false
        email email: true
        titleName blank: false
        firstName blank: false
        lastName blank: false
//        gender blank:  false
//        description blank: false
        createDate nullable: true
        lastUpdate nullable: true
    }

    def beforeInsert()
    {
        this.createDate = new Date() // สั่งให้ Property createDate ของ Domain Category เป็นวันที่ปัจจุบัน
        this.lastUpdate = new Date() // สั่งให้ Property lastUpdate ของ Domain Category เป็นวันที่ปัจจุบัน
    }


    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_users_id'] //Generate id
    }
}
