package webboard

class Post {
    static belongsTo = [category:Category,user:Users] // post นึงอยู่ได้หนึ่ง category กับ หนึ่ง users
    static hasMany = [posttags:PostTags,comments:Comment] // หนึ่ง post มีได้หลาย tags (ซึ่งเก็บค่าใน posttags) และหลาย comment


    String titlePost
    String description
    Integer view
    Date createDate
    Date lastUpdate

    static constraints = {
        titlePost blank: false
        description blank: false
        createDate nullable: true
        lastUpdate nullable: true
    }

    def beforeInsert()
    {
        this.createDate = new Date() // สั่งให้ Property createDate ของ Domain Category เป็นวันที่ปัจจุบัน
        this.lastUpdate = new Date() // สั่งให้ Property lastUpdate ของ Domain Category เป็นวันที่ปัจจุบัน
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_posts_id']
    }


    //สร้าง method ให้เพิ่มคนที่เข้ามา view post นี้ แบบไม่ return ค่า
    void increaseView()
    {
        this.view += 1  // เพื่อมีคนเข้ามาดูใน post นี้ view จะ+1 ไปเรื่อย
        this.save() // และสั่งให้มัน save
    }
}
