package webboard

class Category {

    static hasMany = [posts:Post] //Category นึงมีได้หลาย Post

    String name //สร้าง property  name ให้เป็น String
    String image
    String description
    Date createDate
    Date lastUpdate

    static constraints = {
        name blank: false //บอกให้ name ห้ามว่าง ("ต้องใส่ค่า")
        image blank: false
        description blank: false
        createDate nullable: true
        lastUpdate nullable: true
    }

//  เป็น Method นึงที่กำหนดค่าก่อนที่จะ insert ข้อมูลว่าให้มันทำอะไร
    def beforeInsert()
    {
        this.createDate = new Date() // สั่งให้ Property createDate ของ Domain Category เป็นวันที่ปัจจุบัน
        this.lastUpdate = new Date() // สั่งให้ Property lastUpdate ของ Domain Category เป็นวันที่ปัจจุบัน
    }
    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_category_id'] //Generate id
    }
}
