วันนี้เรียนเรื่องคำสั่งและวิธีการใช้งาน และทำแบบฝึกหัด ชื่อไฟล์ คือ Carcare, Calculate, Calculater
- Class
- Method
- Property
- Getter / Setter
- Instance
- Extends

#Class / Method
เรียนสร้าง Class และ class ที่ extends

เรียนสร้าง Method แบบ return กับไม่ return
#Property / Instance
คือการกำหนดตัวแปรเหมือน Vairable แต่อยู่ต่างที่กันเลยเรียกไม่เหมือนกัน
- Variable ถ้าอยู่ใน method จะเรียกว่า Variable
- Property คือ Variable ที่อยู่นอก method
- Instance คือ Variable ที่เอา class มาใช้
#Getter / Setter
คือการตั้งค่าและดึงข้อมูล
- Setter การกำหนดค่า
- Getter การดึงข้อมูล

#Extends
คือการสืบทอด class โดยเป็นการนำ property กับ method มาใช้