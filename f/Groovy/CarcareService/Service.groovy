package CarcareService

import groovy.sql.Sql

/**
 * Created by Lenovo710s on 6/27/2017.
 */
class Service {
//    กำหนด Property
    Integer serviceId;
    String name;
    Double sPrice;
    Double mPrice;
    Double lPrice;
    Double oPrice;


    List getServiceList(Integer limit = 5) //การดึงข้อมูลหมดเป็นแบบ List Array และกำหนดตัวแปร limit เป็นแบบ int ให้เป้น default แสดงแค่ 5 อัน
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/carcare",
                "postgres", "postgres", "org.postgresql.Driver");
        //ที่ให้ def เพราะเป็น array กำหนดให้ serviceList = select ข้อมูลที่ต้องการ
        def serviceList = sql.rows("SELECT service_id,name,small_price,medium_price,large_price,other_price FROM service LIMIT ${limit}");
        sql.close()
        return serviceList //ให้return กลับมา เป็น List array []
    }

    //ใช้ void เพราะ ไม่ให้มัน return ค่ากลับมา
    void getServiceById (Integer pServerId) //กำหนดตัวแปร
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/carcare",
                "postgres", "postgres", "org.postgresql.Driver");
        //การ select จะใช้ rows เลยไม่ต้องมี execute
        def serviceList = sql.rows("SELECT service_id,name,small_price,medium_price,large_price,other_price FROM service WHERE service_id = ${pServerId}");
        sql.close()
        //กำหนดเงื่อนไขให้มัน ถ้า serviceList มีค่า มากกว่า 0 หรือไม่มีค่าว่าง(ก็คือมีข้อมูลนั้นแหละ) ให้ทำงาน
        if (serviceList.size() > 0){
            def serviceObj = serviceList.get(0) //กำหนดค่าตัวแปรเป็น Map = serviceList.get(0) get 0 คือบอกให้มันดึงข้อมูลตัวแรก เพราะ array จะนับตัวแรกเป็น 0
            this.serviceId = serviceObj.get("service_id")
            this.name = serviceObj.get("name")
            this.sPrice = serviceObj.get("small_price")
            this.mPrice = serviceObj.get("medium_price")
            this.lPrice = serviceObj.get("large_price")
            this.oPrice = serviceObj.get("other_price")
        }
            // else คือถ้าไม่เข้าเงื่อนไขข้างบนหรือ ไม่มีการทำ ให้แสดงเป็น Error
        else {
            println("Error")
        }
    }

    void getUpdateService()
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/carcare",
                "postgres", "postgres", "org.postgresql.Driver");
        String updateService = "UPDATE service SET name = '${this.name}', small_price = ${this.sPrice}, medium_price = ${this.mPrice}, large_price = ${this.lPrice}, other_price = ${this.oPrice} WHERE service_id = ${this.serviceId}";
        //ที่ใช้คำสั่ง execute (เพราะไม่มี rows) เป็นคำสั่ง Run จะใช้กับ Insert / Update / Delete
        sql.execute(updateService)
        sql.close()
    }

    void getCreateService()
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/carcare",
                "postgres", "postgres", "org.postgresql.Driver");
        String createService = "INSERT INTO service (name) VALUES ('${this.name}')";
        sql.execute(createService)
        sql.close()
    }
    void getDeleteService()
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/carcare",
                "postgres", "postgres", "org.postgresql.Driver");
        String deleteService = "DELETE FROM service WHERE service_id = ${this.serviceId}";
        sql.execute(deleteService)
        sql.close()
    }
}
