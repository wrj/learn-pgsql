package DatabaseDvd

import groovy.sql.Sql

/**
 * Created by Admin on 26/6/2560.
 */
class Category {
    Integer categoryId;
    String name;

    List categoryList(Integer limit = 5) {
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/dvdrental',
                'postgres', 'password', 'org.postgresql.Driver')
        def categoryList = sql.rows("SELECT category_id,name FROM category WHERE category limit ${limit}");
        sql.close()
        return categoryList
    }
    void getCategoryById(Integer pCategoryId) {
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/dvdrental',
                'postgres', 'password', 'org.postgresql.Driver')
        def categoryList = sql.rows("SELECT category_id,name FROM category WHERE category_id = ${pCategoryId}");
        sql.close()
        println("categoryList > ${categoryList}")
        println("categoryList.size() > ${categoryList.size()}")
        if (categoryList.isEmpty() == false) {
            def categoryObj = categoryList.get(0);
            println("categoryObj > ${categoryObj}")
            this.categoryId = categoryObj.get("category_id");
            this.name = categoryObj.get("name");
        }
    }

    void updateCategory() {
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/dvdrental',
                'postgres', 'password', 'org.postgresql.Driver')


        String sqlText = "UPDATE category SET name = '${this.name}' where category_id = ${this.categoryId}"
        println(sqlText)
        sql.execute(sqlText)
        sql.close()
    }
}
