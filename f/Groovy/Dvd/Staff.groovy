package Dvd

import groovy.sql.Sql

/**
 * Created by Lenovo710s on 6/27/2017.
 */
class Staff {
    Integer staffId
    String firstname
    String lastname
    Integer addressId
    String email
    Integer storeId
    Boolean active


    void getStaffById(Integer pStaffById) {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        def staffList = sql.rows("SELECT staff_id,first_name,last_name,address_id,email,store_id,active FROM staff WHERE staff_id = ${pStaffById}");
        sql.close()
        if (staffList.size() > 0) {
            def staffObj = staffList.get(0)
            this.staffId = staffObj.get("staff_id")
            this.firstname = staffObj.get("first_name")
            this.lastname = staffObj.get("last_name")
            this.addressId = staffObj.get("address_id")
            this.email = staffObj.get("email")
            this.storeId = staffObj.get("store_id")
            this.active = staffObj.get("active")
        }
    }

    def getAddress() {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        def addressList = sql.rows("SELECT a.address_id,a.address,a.address2,a.district,a.city_id,a.postal_code,a.phone,c.city FROM address a INNER JOIN city c ON a.city_id = c.city_id WHERE a.address_id = ${this.addressId}");
        sql.close()
        def addressObj = addressList.get(0)
        return addressObj
    }

    Integer getPaymentCount() {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        def paymentList = sql.rows("SELECT count(payment_id) FROM payment WHERE payment.staff_id = ${this.staffId}");
        sql.close()
        def paymentObj = paymentList.get(0)
        return paymentObj.get("count")
    }

    Double getPaymentSum() {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver");
        def paymentList = sql.rows("SELECT sum(amount) FROM payment WHERE payment.staff_id = ${this.staffId}");
        sql.close()
        def paymentObj = paymentList.get(0)
        return paymentObj.get("sum")
    }

}


