package Carcare

import groovy.sql.Sql

/**
 * Created by F on 6/27/2017.
 */
class Employee {
    //กำหนด Property
    Integer employeeId
    String nameTitle
    String firstname
    String lastname
    Integer age
    String gender
    String telephone
    String address
    Double salary
    String email
    Integer branchId

    String dbUrl = "jdbc:postgresql://localhost:5433/carcare";
    String dbUsername = "postgres"
    String dbPassword = "postgres"
    String dbDriver = "org.postgresql.Driver"

    //สร้าง Method โยใช้ Void เพื่อไม่ให้มัน return กลับมาและกำหนดตัวแปรให้มารับค่าเป็น Integer เพราะเป็น id
    void getEmployeeById(Integer pEmployee)
    {
        def sql = Sql.newInstance(this.dbUrl,
                this.dbUsername, this.dbPassword, this.dbDriver);
//        กำหนดค่าให้เป็น array เราใช้ List หรือ def ก็ได้ สร้างให้มันเหมือนกับกล่องเพื่อเก็บข้อมูล เพราะ sql.rows อ่านค่าเป็น array
        List employeeList = sql.rows("SELECT * FROM employee WHERE employee.employee_id = ${pEmployee}");
        //เป็นคำสั่งหยุดการทำงานของ sql ด้านบน
        sql.close()
        //สร้างเงื่อนไขให้มันถ้าเข้าเงื่อนไขให้ทำงาน (ถ้าตัวแปรนี้มีค่ามากกว่า 0 หรือมีข้อมุลจะเท่ากับค่าไม่ว่างให้ทำงาน)
        if (employeeList.size() > 0) {
            //สร้างตัวแปรมารับค่าใช้ def เพราะเป็น array แบบ map ให้ get = 0 เพราะให้มันดึงข้อมุล Index ตัวที่ 0 เพราะ array จะนับตัวแรก เป็น {0,1,2,3 ไปเรื่อย}
            def employeeObj = employeeList.get(0)
            //เป็นการดึงข้อมุลจาก map หรือ ค่าตัวแปรด้านขวามือมาใส่ให้ Property ด้านซ้ายมือให้เป็นผลลัพธ์ (หลัง get จะเป้น String ให้เราใส่ค่า field ที่จะให้มันแสดง)
            this.employeeId = employeeObj.get("employee_id")
            this.nameTitle = employeeObj.get("name_title")
            this.firstname = employeeObj.get("firstname")
            this.lastname = employeeObj.get("lastname")
            this.age = employeeObj.get("age")
            this.gender = employeeObj.get("gender")
            this.telephone = employeeObj.get("telephone")
            this.address = employeeObj.get("address")
            this.email = employeeObj.get("email")
            this.salary = employeeObj.get("salary")
            this.branchId = employeeObj.get("branch_id")
        }
    }

    // กำหนด Method ให้เป็น array แต่เราจะสั่งให้มัน return ค่ากลับมา เลยไม่ใช้ void
    def getBranch()
    {
        def sql = Sql.newInstance(this.dbUrl,
                this.dbUsername, this.dbPassword, this.dbDriver);
        // กำหนดตัวแปรให้มัน เพื่อบอกว่าตัวแปรนี้ดึงขึ้อมูลอะไรบ้าง (ถ้าเราเขียนหลายบรรทัดหรือ ทำข้อมูลเยอะๆ ใช้เป็น $/...../$ ครอบแทน)
        String sqlText = $/
            SELECT
                b.name as branch_name,
                b.address
            FROM
                branch b
            WHERE b.branch_id = ${this.branchId}
        /$
        List branchId = sql.rows(sqlText);
        sql.close()
        //กำหนดตัวแปรให้รับค่า branchId.get(0) (เป็นแบบ Map ถึงใช้ get)
        def branchObj = branchId.get(0)
        //สั่งให้มัน return ค่ากลับมา
        return branchObj
    }

    //สร้าง method ที่ใช้นับจำนวนเลยใช้ Int แทน เพราะเราต้องการแค่ตัวเลข
    Integer getPaymentCount() {
        def sql = Sql.newInstance(this.dbUrl,
                this.dbUsername, this.dbPassword, this.dbDriver);
        def paymentCount = sql.rows("SELECT count(payment_id) FROM payment WHERE payment.employee_id =  ${this.employeeId}");
        sql.close()
        def paymentObj = paymentCount.get(0)
        return paymentObj.get("count")
    }

    //สร้าง method ที่ใช้นับเกี่ยวกับค่าเงิน เลยใช้ double แทน เพราะเราต้องการ
    Double getPaymentSum() {
        def sql = Sql.newInstance(this.dbUrl,
                this.dbUsername, this.dbPassword, this.dbDriver);
        List paymentSum = sql.rows("SELECT sum(total_price) FROM payment WHERE payment.employee_id =  ${this.employeeId}");
        sql.close()
        def paymentObj = paymentSum.get(0)
        return paymentObj.get("sum")
    }



}
