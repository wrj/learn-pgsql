package Carcare

import groovy.sql.Sql

/**
 * Created by Lenovo710s on 6/29/2017.
 */
class Branch extends Database{
    Integer branchId
    String name
    Date openTime
    Date closeTime
    String telephone
    String workPhone
    String holiday
    String address
    boolean status
    //สร้าง method ชื่อ getBranchI เป็น void ไม่ให้ return ค่ากลับมา
    void getBranchById(Integer bBranchId) {
        def sql = Sql.newInstance(this.dbUrl,this.dbUser, this.dbPass, dbDriver);
        def branchIdList = sql.rows("SELECT * FROM branch WHERE branch.branch_id = ${bBranchId}")
        sql.close()
        if (branchIdList.size() > 0){
            def branchObj = branchIdList.get(0)
            this.branchId = branchObj.get("branch_id")
            this.name = branchObj.get("name")
            this.openTime = branchObj.get("open_time")
            this.closeTime = branchObj.get("close_time")
            this.telephone = branchObj.get("telephone")
            this.workPhone = branchObj.get("work_phone")
            this.holiday = branchObj.get("holiday")
            this.address = branchObj.get("address")
            this.status = branchObj.get("status")
        }
    }

    List getBranchService() {
        def sql = Sql.newInstance(this.dbUrl,this.dbUser, this.dbPass, dbDriver);
        String branchServiceList = $/
            SELECT
            b.name as branch_name,
            s.name as service_name,
            c.name as category_name
            FROM branch b
            INNER JOIN branch_category bc ON b.branch_id = bc.branch_id
            INNER JOIN category c ON bc.category_id = c.category_id
            INNER JOIN service_category sc ON sc.category_id = c.category_id
            INNER JOIN service s ON s.service_id = sc.service_id
            WHERE b.branch_id = ${this.branchId}
        /$
        def branchService = sql.rows(branchServiceList)
        sql.close()
        return branchService
    }


}
