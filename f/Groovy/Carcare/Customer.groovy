package Carcare

import com.sun.org.apache.bcel.internal.generic.Select
import groovy.sql.Sql
import org.apache.tools.ant.taskdefs.Delete

/**
 * Created by Lenovo710s on 6/28/2017.
 */
class Customer extends Database {
//    สร้าง Property ให้
    Integer customerId
    String nameTitle
    String firstName
    String lastName
    String brand
    String serie
    String licensePlate
    String color
    String email
    String telephone
    String gender
    String address


    //สร้าง Method ใช้ void เพื่อไม่ให้มัน return ค่ากลับกลับมาและกำหนด parameter ใน id เป็นแบบ Integer
    void getCustomerById(Integer cCustomerId) {
        def sql = Sql.newInstance(this.dbUrl,this.dbUser, this.dbPass, dbDriver);
        //สร้างตัวแปรมารับค่าเป็นแบบ List หรือ def ให้ดึงข้อมูลเป็น Array
        def customerList = sql.rows("SELECT * FROM customer WHERE customer_id = ${cCustomerId}")
        sql.close()//ปืดการทำงาน sql ด้านบน
        if (customerList.size() > 0) { //กำหนดเงื่อนไขในการดึงข้อมูล ถ้า  customerList มีค่ามากกว่า 0 คือค่าไม่ว่าง ให้มันทำงาน
            def customerObj = customerList.get(0) // ให้ customerList.get(0) ดึงข้อมุลตัวที่ 0 หรือตัวแรก มาเก็บใน customerObj เป็นแบบ Map
            this.customerId = customerObj.get("customer_id") // ดึงข้อมูล customer_id มาเก็บที่ Property
            this.nameTitle = customerObj.get("name_title") //ดึงข้อมูล name_title มาเก็บที่ Property
            this.firstName = customerObj.get("firstname") //ดึงข้อมูล firstname มาเก็บที่ Property
            this.lastName = customerObj.get("lastname") //ดึงข้อมูล lastname มาเก็บที่ Property
            this.brand = customerObj.get("brand") //ดึงข้อมูล brand มาเก็บที่ Property
            this.serie = customerObj.get("serie") //ดึงข้อมูล serie มาเก็บที่ Property
            this.licensePlate = customerObj.get("license_plate") //ดึงข้อมูล license_plate มาเก็บที่ Property
            this.color = customerObj.get("color") //ดึงข้อมูล color มาเก็บที่ Property
            this.email = customerObj.get("email") //ดึงข้อมูล email มาเก็บที่ Property
            this.telephone = customerObj.get("telephone") //ดึงข้อมูล telephone มาเก็บที่ Property
            this.gender = customerObj.get("gender") //ดึงข้อมูล gender มาเก็บที่ Property
            this.address = customerObj.get("address") //ดึงข้อมูล address มาเก็บที่ Property
        }
    }

    //Insert ใช้ void เพราะไม่ต้องการให้มัน return ค่ากลับมาหรือเอาค่าไปทำอะไรต่อ สร้าง parameter ให้มัน
    void getInsertCustomer(String cNameTitle,String cFirstName,String cLastname,String cGender,String cAdress) {
        def sql = Sql.newInstance(this.dbUrl,this.dbUser, this.dbPass, dbDriver);
        //กำหนดค่าตัวแปร sqlText เป็น String เพื่อให้ข้อมูลฝั่งซ้ายมาเก็บที่ฝั่งขวา
        String sqlText = "INSERT INTO customer (name_title,firstname,lastname,gender,address) VALUES ('${cNameTitle}','${cFirstName}','${cLastname}','${cGender}','${cAdress}')"
        sql.execute(sqlText) //สั่งให้ sqlText ทำงาน
        sql.close()//ปิดการทำงาน sql
    }

    //Update ข้อมูลใช้ void เพราะไม่ต้องหารให้มัน return ค่ากลับมา
    void getUpdateCusrtomerById() {
        def sql = Sql.newInstance(this.dbUrl,this.dbUser, this.dbPass, dbDriver);
        //กำหนดค่าตัวแปรเพื่อเอาข้อมูล Update มาใส่ตัวแปรที่ฝั่งซ้าย
        String sqlText = "UPDATE customer SET firstname = '${this.firstName}',lastname = '${this.lastName}' WHERE customer_id = ${this.customerId}"
        sql.execute(sqlText) //Run sqltext ข้างบน
        sql.close() // ปิดการทำงาน sql
    }

    //Delete ข้อมูล ให้สร้าง Method ขึ้นมา ใช้ void เพราะไม่ต้องการให้มัน retuen ค่ากลับมา
    void deleteCustomerById() {
        def sql = Sql.newInstance(this.dbUrl,this.dbUser, this.dbPass, dbDriver);
        //กำหนดค่าตัวแปรขึ้นมา เพื่อดึงข้อมูลจากซ้ายมาขวา Delete
        String sqlText = "DELETE FROM customer WHERE customer_id = ${this.customerId}"
        sql.execute(sqlText)//Run sql ด้านบน
        sql.close()//ปิดการทำงาน sql
    }
    //สร้าง Method การนับจำนวนของลูกค้าคนนี้มาใช้บริการกี่ครั้ง
    Integer getPaymentCount() {
        def sql = Sql.newInstance(this.dbUrl,this.dbUser, this.dbPass, dbDriver);
        //กำหนดตัวแปร paymentCount ให้รับค่าข้อมุลของฝั่งขวามาใส่
        def paymentCount = sql.rows("SELECT count(payment_id) FROM payment WHERE payment.customer_id = ${this.customerId}")
        sql.close()
        //กำหนดค่า paymentCountObj รับค่าจาก paymentCount.get(0)ให้ดึงข้อมุลเริ่มตั้งแต่ลำดับที่ 0 เป็นแบบ Map array
        def paymentCountObj = paymentCount.get(0)
        return paymentCountObj.get("count")  //ให้มัน return ค่ากลับมาเป็น count
    }
    //สรา้ง Method เพื่อหาผลรวมของจำวนเงินที่ลูกค้าคนนี้จ่ายไป ให้แสดงเป็น Double
    Double getPaymentSum() {
        def sql = Sql.newInstance(this.dbUrl,this.dbUser, this.dbPass, dbDriver);
        //สร้างตัวแปรให้เก็บข้อมุลจากฝั่งขวา ที่แสดงเป็นตัวเลข
        def paymentSum = sql.rows("SELECT sum(total_price) FROM payment WHERE payment.customer_id = ${this.customerId}")
        sql.close()//ปิดการทำงาน
        //สร้างตัวแปร paymentSumObj เพื่อเก็บข้อมูลของ paymentSum.get(0)
        def paymentSumObj = paymentSum.get(0)
        return paymentSumObj.get("sum")//ให้ return ค่าเป็นผลรวมของราคา
    }
}
