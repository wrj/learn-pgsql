วันนี้ทบทวนเรื่อง Class / Method และลองทำแบบแบบฝึกหัดดู เรียนเรื่องการดึง Database เข้ามาโดยใช้ไฟล์ของ dvdrental

##คำสั่งในการดึง Database 
    
    def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/dvdrental',
                'postgres', 'postgres', 'org.postgresql.Driver')
##Select
    def list = sql.rows("SELECT * FROM customer");
##Insert
##Update
    def list = sql.rows("Update .... SET .... Where ....");
##Delete

    def list = sql.rows("Delete From....  Where ....");

