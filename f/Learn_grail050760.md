เรียนคำสั่งของ Grails และลองสร้างดู
- การสร้าง Domain และ Run บน Web Browser
   - Create
   - Update
   - Delete

- การทำ Relation
   - belong to
   - Many to Many
      - ใส่ has many ทั้งสอง Domain
   - One to Many
      - ใส่ has many ที่ 1 Domain อีก Domain นึงใส่ belong to แทน
   - One to One
      - ใส่ has one แค่ 1 Domain อีก Domain นึงใส่ belong to แทน

   
- การทำ Validate 
   - การใช้ Constraints
   - การใช้ hasError
   