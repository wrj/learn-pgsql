DROP TRIGGER barcode_changes ON product;
DROP TABLE IF EXISTS product_audits;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS category;

CREATE TABLE category (
  category_id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(100) NOT NULL,
  description TEXT,
  created_at TIMESTAMP DEFAULT now(),
  updated_at TIMESTAMP DEFAULT current_timestamp
);
COMMENT ON COLUMN category.name IS 'ชื่อประเภทสินค้า';
COMMENT ON TABLE category IS 'ตารางประเภทสินค้า';

CREATE TABLE product (
  product_id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(100) NOT NULL,
  barcode VARCHAR(100) NOT NULL,
  description TEXT,
  price NUMERIC NOT NULL DEFAULT 0,
  amount INTEGER DEFAULT 0,
  category_id INTEGER NOT NULL,
  created_at TIMESTAMP DEFAULT now(),
  updated_at TIMESTAMP DEFAULT current_timestamp
);
CREATE UNIQUE INDEX product_name_uindex ON product (barcode);
COMMENT ON COLUMN product.name IS 'ชื่อสินค้า';
COMMENT ON TABLE product IS 'ตารางสินค้า';

ALTER TABLE product
ADD CONSTRAINT category_product_category_id_fk
FOREIGN KEY (category_id) REFERENCES category (category_id);

CREATE TABLE product_audits(
  id SERIAL PRIMARY KEY ,
  product_id int4 NOT NULL ,
  barcode VARCHAR(200) NOT NULL ,
  changed_on timestamp(6) NOT NULL
);

CREATE OR REPLACE FUNCTION log_barcode_changes()
  RETURNS trigger AS
$BODY$
BEGIN
  IF NEW.barcode <> OLD.barcode THEN
    INSERT INTO product_audits(product_id,barcode,changed_on)
    VALUES(OLD.product_id,OLD.barcode,now());
  END IF;

  RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER barcode_changes
BEFORE UPDATE
ON product
FOR EACH ROW
EXECUTE PROCEDURE log_barcode_changes();

INSERT INTO category (name, description) VALUES ('mobile','');
INSERT INTO product
(name, barcode, description, price, amount, category_id) VALUES
  ('xperia','1111','',1000,10,1);