#สร้าง Database

query สำหรับสร้าง database

    CREATE DATABASE database_name;

สร้าง database ให้มี COLLATE เป็นภาษาไทย

    CREATE DATABASE database_name  WITH OWNER "postgres"
    TEMPLATE “template0" ENCODING 'UTF8' LC_COLLATE 'th_TH.UTF-8' LC_CTYPE 'th_TH.UTF-8'
    TABLESPACE "pg_default" CONNECTION LIMIT -1;


## คำสั่งสร้าง Table

    CREATE TABLE table_name (
        column_name TYPE column_constraint,
        table_constraint table_constraint
    ) INHERITS existing_table_name;

Ex.

    CREATE TABLE product (
        product_id SERIAL PRIMARY KEY NOT NULL,
        name VARCHAR(100) NOT NULL,
        description TEXT,
        price NUMERIC NOT NULL DEFAULT 0,
        amount INTEGER DEFAULT 0,
        category_id INTEGER NOT NULL,
        created_at TIMESTAMP DEFAULT now(),
        updated_at TIMESTAMP DEFAULT current_timestamp,
        CONSTRAINT category_category_id_fkey FOREIGN KEY (category_id)
              REFERENCES category (category_id)
    );
    CREATE UNIQUE INDEX product_name_uindex ON product (name);
    COMMENT ON COLUMN product.name IS 'ชื่อสินค้า';
    COMMENT ON TABLE product IS 'ตารางสินค้า';

##การแก้ไข Table

1.  เพิ่ม column


    ALTER TABLE table_name ADD column TYPE column_constraint;

2. ลบ column


    ALTER TABLE table_name DROP column;

3. ทำ Foreign Key หลังจากสร้าง table แล้ว


    ALTER TABLE table_name
    ADD CONSTRAINT foreign_key_name
    FOREIGN KEY (column_in_table) REFERENCES reference_table_name (column_in_reference_table_name);

*   หลังจากคำสั่ง add foreign key จะมีคำสั่งต่อไป 3 คำสั่ง คือ

    *   RESTRICT คือ ไม่อนุญาติให้ ลบ หรือ แก้ไข ค่าข้อมูล ใน PRIMARY KEY ได้ถ้ายังมีค่าข้อมูลใน FOREIGN KEY อยู่ ต้องไปลบ row ที่ใช้ ข้อมูลจาก Primary key นั้นๆก่อน
    *   NO ACTION คล้ายๆกับ RESTRICT ครับ คือไม่อนุญาติให้ ลบ หรือ แก้ไข ค่าข้อมูล ใน PRIMARY KEY ได้ถ้ายังมีค่าข้อมูลใน FOREIGN KEY อยู่ จะมีการตรวจสอบแถวที่มีการอ้างอิงถึงในตอนท้ายของ statement
    *   CASCADE คือ ถ้ามีการลบหรือแก้ไขค่าข้อมูลใน PRIMARY KEY แล้วค่าข้อมูลใน FOREIGN KEY จะเปลี่ยนแปลงตาม เช่น ลบข้อมูล category ข้อมูลใน product จะถูกลบไปด้วย

Ex.

    ALTER TABLE product
    ADD CONSTRAINT category_product_category_id_fk
    FOREIGN KEY (category_id) REFERENCES category (category_id) ON DELETE CASCADE;

    * ถ้าลบ category จะลบข้อมูลที่ product ด้วย

##ลบ Table

    DROP TABLE IF EXISTS table_name;

*   การใช้คำสั่ง drop table ต้อง drop table ที่มีการ reference ก่อน เช่น table product มีการใช้ id จาก category ถ้า drop table category จะไม่สามารถลบได้ ต้อง drop table product ก่อน