#Sub Query

*   ใช้ Sub Query ใน SELECT จะต้องเป็น query ที่มีข้อมูลแค่ค่าเดียว เช่น ค่าเฉลี่ย ผลรวม หรือ เลือก 1 row 1 column จาก query


    Ex.
        SELECT
            first_name,
            last_name,
             (SELECT count(payment_id) FROM payment WHERE payment.customer_id = customer.customer_id),
             (SELECT sum(amount) FROM payment WHERE payment.customer_id = customer.customer_id)
        FROM customer;
จะแสดงข้อมูล ชื่อ,นามสกุล,จำนวนใบ payment ของลูกค้า,ค่ารวมจาก payment ของลูกค้า

*   ใช้ Sub Query ใน WHERE ค่าของ query ที่มีข้อมูลแค่ 1 row หรือ หลาย row ก็ได้ แต่ต้องมีแค่ 1 column


    Ex.
        SELECT
            film_id,
            title
        FROM
            film
        WHERE
            rental_rate > (
                SELECT
                    AVG (rental_rate)
                FROM
                    film
            );

จะหมายความว่า ดึงข้อมูลชื่อหนังที่ค่าเฉลี่ยมากกว่า ค่าเฉลี่ยทั้งหมดของหนัง ซึ่งคำสั่ง

    SELECT
        AVG (rental_rate)
    FROM
        film

จะได้ข้อมูลค่าเฉลี่ยแค่ 1 ค่า


    Ex.
        SELECT
            film_id,
            title
        FROM
            film
        WHERE
            film_id IN (
                SELECT
                    inventory.film_id
                FROM
                    rental
                INNER JOIN inventory ON inventory.inventory_id = rental.inventory_id
                WHERE
                    return_date BETWEEN '2005-05-29' AND '2005-05-30'
            );
จะหมายความว่า ดึงข้อมูลชื่อหนังที่ file_id มีใน sub query ซึ่งคำสั่ง

    SELECT
        inventory.film_id
    FROM
        rental
    INNER JOIN inventory ON inventory.inventory_id = rental.inventory_id
    WHERE
        return_date BETWEEN '2005-05-29' AND '2005-05-30'
จะได้ข้อมูล file_id ที่มีการคืนในระหว่างวันที่ 29/5/2005 ถึง 30/5/2005
