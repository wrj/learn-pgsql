#Operator

<table>
    <tr>
        <td>Operator</td>
        <td>Description</td>
    </tr>
    <tr>
        <td>=</td>
        <td>Equal</td>
    </tr>
    <tr>
        <td>></td>
        <td>Greater than</td>
    </tr>
    <tr>
        <td><</td>
        <td>Less than</td>
    </tr>
    <tr>
        <td>>=</td>
        <td>Greater than or equal</td>
    </tr>
    <tr>
        <td><=</td>
        <td>Less than or equal</td>
    </tr>
    <tr>
        <td><> or !=</td>
        <td>Not equal</td>
    </tr>
    <tr>
        <td>AND</td>
        <td>Logical operator AND</td>
    </tr>
    <tr>
        <td>OR</td>
        <td>Logical operator OR</td>
    </tr>
</table>

##LIKE

ใช้เหมือน = แต่สามารถค้นหาเป็น pattern ได้ดูควาแตกต่างระหว่าง camelcase

    *   Percent ( %)  ตรงกับคำที่ต้องการหา
    *   Underscore ( _)  ตรงกับตัวอักษรบางตัว
    Ex.
        'foo' LIKE 'foo' = true
        'foo' LIKE 'f%' = true
        'foo' LIKE '_o_' = true
        'foo' LIKE 'f_' = false

##ILIKE

ใช้เหมือน LIKE แต่สามารถค้นหาเป็น pattern ค้าหาได้โดยไม่สน camelcase

* ทั้ง LIKE และ ILIKE สามารถใช้ NOT นำหน้าเพื่อค้นหาคำที่ไม่ตรงกับ pattern ได้

##BETWEEN

ใช้ค้นหาค่าเป็นช่วง

    column_name BETWEEN min_value AND max_value