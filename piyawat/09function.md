#Function

##function เกี่ยวกับผลรวม หรือ ผลสรุปตัวเลข

    *   AVG()
    *   COUNT()
    *   SUM()
    *   MAX()
    *   MIN()

##function ที่เกี่ยวกับเงื่อนไข
###CASE
หาเงื่อนไขของ query

    CASE
        WHEN condition_1  THEN result_1
        WHEN condition_2  THEN result_2
        [ELSE result_n]
    END
    Ex.
        SELECT
        customer_id,
        first_name,
        CASE
        WHEN activated THEN 'activate'
        ELSE ''
        END
        FROM customer

###COALESCE
แสดงค่าที่ไม่ใช่ค่า null จาก argument ตัวแรก

    COALESCE (argument_1, argument_2, …);
    Ex.
        SELECT
            COALESCE(NULL,1)

        จะมีผลคือ 1

###CAST
แปลง data type เช่น string to integer,string to date

    CAST ( expression AS type );
    Ex.
        SELECT
         CAST ('100' AS INTEGER);

##function เกี่ยวกับ string
###CONCAT
นำ string มาต่อกัน

    CONCAT(str_1,str_2,...)
    Ex.
        SELECT
         CONCAT  (first_name, ' ', last_name) AS "Full name"
        FROM
         customer;

###LENGTH
หาความยาวตัวอักษร

    LENGTH(string);
    EX.
        SELECT
         LENGTH ('PostgreSQL Tutorial'); -- 19

###LOWER,UPPER,INITCAP
ทำตัวอักษรเป็นตัว ใหญ่ ตัวเล็ก และ ตัวแรกของตัวอักษรเป็นตัวใหญ่

    LOWER(string_expression)
    UPPER(string_expression)
    INITCAP(string_expression)
    Ex.
        LOWER('HELLO') -- hello
        UPPER('hello') -- HELLO
        INITCAP('hello') - Hello

###TRIM
ตัวอักษรว่าง whitespace ออก

    TRIM(string)
    Ex.
        TRIM(' Hello ') -- 'Hello'

##function วันที่
###ใส่เวลาปัจจุบัน

จะใช้คำสั่ง now() ที่ values

###คำนวนอายุ AGE()

ใช้สำหรับคำนวนหาอายุ จะแสดงผลเป็นแบบ interval คือ 17 years 2 mons 19 days

    SELECT AGE(timestamp เวลาที่ต้องการหา,timestamp วันเกิด)
    Ex.
        AGE(now(),birthdate)

###วิธีดึงข้อมูลจาก interval

    EXTRACT(field FROM interval)
    Ex.
        EXTRACT (
                MINUTE
                FROM
                    INTERVAL '5 hours 21 minutes'
            );

        จะได้ 21

###DATE_PART
ดึงข้อมูลบางส่วนของ date มาแสดง

    DATE_PART(field,source)
    Ex.
        SELECT date_part('year',TIMESTAMP '2017-01-01'); -- 2017

*   field สามารถดูเพิ่มได้จาก [link](http://www.postgresqltutorial.com/postgresql-date_part/) นี้

###to_date
แปลงจากตัวอักษรเป็นวันที่

    to_date(text,format);
    Ex.
        SELECT to_date('20170103','YYYYMMDD'); -- 2017-01-03

*   format สามารถดูเพิ่มได้จาก [link](http://www.postgresqltutorial.com/postgresql-to_date/) นี้
