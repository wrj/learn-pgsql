package dvd

import groovy.sql.Sql

/**
 * Created by piyawat on 6/30/2017 AD.
 */
class Actor extends DbProperties {
    Integer actorId;
    String firstName;
    String lastName;

    void getById(Integer id){
        def sql = Sql.newInstance(this.dbUrl,this.dbUsername,this.dbPassword,this.dbDriver);
        String sqlText = $/
            SELECT actor_id,first_name,last_name FROM actor WHERE actor_id = ${id}
        /$
        List actorList = sql.rows(sqlText);
        sql.close();
        if(!actorList.isEmpty())
        {
            def actorObj = actorList.get(0);
            this.actorId = actorObj.get("actor_id")
            this.firstName = actorObj.get("first_name")
            this.lastName = actorObj.get("last_name")
        }
    }

    List getActorList(Integer limit)
    {
        def sql = Sql.newInstance(this.dbUrl,this.dbUsername,this.dbPassword,this.dbDriver);
        String sqlText = $/
            SELECT actor_id,first_name,last_name FROM actor LIMIT ${limit}
        /$
        def acList = sql.rows(sqlText)
        sql.close()
        return acList
    }

    List getFilm(Integer actorId)
    {
        def sql = Sql.newInstance(this.dbUrl,this.dbUsername,this.dbPassword,this.dbDriver);
        String sqlText = $/
            SELECT
                f.film_id,
                f.title,
                f.rating
            FROM film_actor fa
            INNER JOIN film f ON fa.film_id = f.film_id
            WHERE fa.actor_id = ${actorId}
        /$
        List filmList = sql.rows(sqlText);
        sql.close()
        return filmList
    }
}
