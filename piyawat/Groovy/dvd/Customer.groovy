package dvd

import groovy.sql.Sql

/**
 * Created by piyawat on 6/26/2017 AD.
 */
class Customer {
    Integer customerId;
    String firstName;

    List getCustomerList(Integer limit,String columnName = "customer_id",String sortBy = "asc")
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver")

        String sqlText = "SELECT customer_id,first_name FROM customer ORDER BY ${columnName} ${sortBy} limit ${limit}"
        println(sqlText)
        def customerList = sql.rows(sqlText);
        sql.close()
        return customerList
    }

    void getCustomerById(Integer customerId)
    {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/dvdrental",
                "postgres", "postgres", "org.postgresql.Driver")

        def customerList = sql.rows("SELECT customer_id,first_name FROM customer WHERE customer_id = ${customerId}");
        sql.close()
        def customerI = customerList.get(0);
        this.customerId = customerI.get("customer_id");
        this.firstName = customerI.get("first_name");
    }
}
