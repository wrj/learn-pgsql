package dvd

import groovy.json.JsonOutput

/**
 * Created by piyawat on 6/23/2017 AD.
 */
class Dvdrental {

    static void main(String[] args)
    {
        Category c = new Category()
//        Create New Category
        c.setName("New Category 1")
        c.createCategory();
        c.createCategoryByName("New Category 2");
//        End Create New Category
//        Get Category By Id
        c.getCategoryById(1);
        println(c.getName());
        //Set New Name
        c.setName("Hello");
        //Update Category Name
        c.updateCategory();
        c.updateCategoryById(1,"Hello")
        //Delete Category
        c.deleteCategory()
        c.deleteCategoryById(1)


        //ตัวอย่างสร้าง json
        Actor actor = new Actor()
        List actorList = actor.getActorList(5)//ดึงข้อมูล actor
        List actorListJson = []//สร้าง array สำหรับเก็บข้อมูลของ actor สำหรับทำ json
        actorList.each {//เอาข้อมูล actor จากการใช้ method getActorList มาวนลูป
            def actorObj = [:]//สร้าง map สำหรับเก็บข้อมูล actor
            actorObj.put("actorId",it.get("actor_id"))//ใส่ค่า key actorId ให้ map โดยดึงข้อมูลจาก actor_id
            actorObj.put("firstName",it.get("first_name"))
            actorObj.put("lastName",it.get("last_name"))
            actorObj.put("film",actor.getFilm(it.get("actor_id")))
            actorListJson.add(actorObj)//นำข้อมูล actor ใส่ใน actorListJson
        }
        println(JsonOutput.toJson(actorListJson))
    }
}
