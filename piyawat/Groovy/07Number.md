#Method เกี่ยวกับ Number

##xxxValue
การแปลง type ของ number

    byte byteValue()
    short shortValue()
    int intValue()
    long longValue()
    float floatValue()
    double doubleValue()
    Ex.
    Integer x = 5;

    // Converting the number to double primitive type
    println(x.doubleValue());

    // Converting the number to int primitive type
    println(x.intValue());

##compareTo
การเปรียบเทียบค่าตัวเลข return values มี 3 ค่า

    0 คือ เท่ากัน
    1 คือ มากกว่า
    -1 คือ น้อยกว่า
    Ex.
    Integer x = 5;
    x.compareTo(5); //0
    x.compareTo(4); //1
    x.compareTo(6); //-1

##equals
เทียบค่าเท่ากันหรือไม่ return values คือ true กับ false

    Ex.
    Integer x = 5;
    x.equals(5); //true
    x.equals(4); //false

##toString
แปลง integer เป็น string

    Ex.
    Integer x = 5;

    x.toString(); //5
    Integer.toString(12); //12

##parseXXX
แปลง string เป็นค่าตัวเลข

    Ex.
    String x = "5";
    Integer i = Integer.parseInt(x); //5
    Double i = Double.parseDouble(x); //5.0

##ceil
ปัดค่าที่มีทศนิยมขึ้น

    Ex.
    Double x = 5.2;
    println(Math.ceil(x)); //6

##floor
ปัดค่าที่มีทศนิยมลง

    Ex.
    Double x = 5.2;
    println(Math.floor(x)); //5

##round
ถ้าจุดทศนิยมต่ำกว่า 5 จะปัดลง มากกว่าหรือเท่ากับ 5 จะปัดขึ้น

    Ex.
    Double x = 5.4;
    Double a = 5.5;

    println(Math.round(x)) ;//5
    println(Math.round(a)); //6

##random
เป็นการสุ่มค่าตัวเลขระหว่าง 0 ถึง 1

    Ex.
    println(Math.random());
    println(Math.random());