#List หรือ Array
เป็น variable ที่สามารถเก็บค่าได้หลายๆ  values ใน groovy จะนับ index ของ array โดยเริ่มจาก 0

การสร้าง array ใน groovy จะสามารถบอก datatype ของ array หรือใน def ก็ได้

    Ex.
    String[] listString = ["a","b","c"];
    Integer[] listInteger = [1,2,3];
    def listArray = [];

##add
เพิ่ม values เข้าใน array

    Ex.
    def listString = [];
    listString.add("a");

##contains
หาว่าใน array มี values ที่ต้องการหรือไม่ จะได้ค่าเป็น true หรือ false

    Ex.
    def listInt = [1,2,3];
    println(listInt.contains(2));//true

##get
เป็นการดึงค่า values ออกมาจาก array โดยใช้ index ของ array

    Ex.
    def listInt = [1,2,3];
    println(listInt.get(1));//2

##isEmpty
เช็คค่า array ว่าว่างหรือไม่

    Ex.
    def a = [1,2,3];
    def b = [];
    println(a.isEmpty());//false
    println(b.isEmpty());//true

##pop
เป็นการนำ values ตัวสุดท้ายออกจาก array

    Ex.
    def a = [1,2,3];
    println(a.pop());//[1,2]

##remove
เป็นการนำค่า value ออกจาก array โดยระบุ index

    Ex.
    def a = [1,2,3];
    println(a.remove(1));//[1,3]

##size
เป็นการหาขนาดของ array

    Ex.
    def a = [1,2,3];
    println(a.size());//3

