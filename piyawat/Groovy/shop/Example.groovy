package shop
/**
 * Created by piyawat on 6/23/2017 AD.
 */
class Example {
    static void main(String[] args){
        Cart c = new Cart();
        c.addToCart(1,35);
        c.addToCart(3,15);
//        c.addToCart(1,50);
        println(c.getSummary());
        Double totalPrice = c.getTotal();
        Promotion p = new Promotion();
        Double totalDiscount = p.calculateDiscount(c.getAmount(),c.getTotal());
        println("ส่วนลด ${totalDiscount} บาท")
        println("ยอดรวมราคาสินค้า ${totalPrice - totalDiscount} บาท")
    }
}
