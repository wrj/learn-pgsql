package shop
/**
 * Created by piyawat on 6/23/2017 AD.
 */
class Cart {
    Integer amount = 0;
    Double total = 0;

    void addToCart(Integer pAmount,Double price){
        this.amount += pAmount;
        this.total += pAmount*price;
    }

    void removeCart(Integer pAmount,Double price){
        if(this.amount > 0)
        {
            this.amount -= pAmount;
            this.total -= pAmount*price;
        }
    }

    String getSummary()
    {
        return "มีสินค้าทั้งหมด ${this.amount} ชิ้น จำนวนเงิน ${this.total} บาท"
    }
}
