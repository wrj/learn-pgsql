#Method เกี่ยวกับ String
##Concatenation of Strings
การต่อ string

    Ex.
        String a = "hello";
        String b = "world";

        println("hello" + "world");
        println(a + b);

##Length
หาขนาดของ string

    Ex.
    String a = "hello";
    println(a.length()); //5

##concat
การต่อ string

    Ex.
    String a = "Hello";
    a.concat("World");
    println(a); //HelloWorld

##replaceAll
การแทนค่า

    replaceAll(String regex, String replacement)
    Ex.
    String a = "Hello name";
    println(a.replaceAll("name","World"));//Hello World

##split
การตัด string เป็น array

    split(String regex)
    *regex - สามารถใส่ regular expression ได้
    Ex.
        String a = "Hello-World";
        String[] str;
        str = a.split("-");

        for(String name in str)
        {
            println(name)
        }

##substring
เป็นคำสั่งดึงตัวอักษรจาก string

    substring(int beginIndex, int endIndex)
    Ex.
    String a = "HelloWorld";
    println(a.substring(4)); //oWorld
    println(a.substring(4,8)); //oWorl

##toUpperCase
แปลงตัวอักษรเป็นตัวพิมพ์ใหญ่

    Ex.
    String a = "HelloWorld";
    println(a.toUpperCase()); //HELLOWORLD

##toLowerCase
แปลงตัวอักษรเป็นตัวพิมพ์เล็ก

    Ex.
    String a = "HelloWorld";
    println(a.toLowerCase()); //helloworld