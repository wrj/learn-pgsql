#Date Time


    Ex.
    Date today = new Date();
    Date olddate = new Date("05/11/2015");
    Date newdate = new Date("05/12/2015");

##before
เปรียบเทียบวันที่ ผลที่ได้จะได้ true หรือ false

    Ex.
    Date olddate = new Date("05/11/2015");
    Date newdate = new Date("05/12/2015");
    Date latestdate = new Date();

    println(olddate.before(newdate));//true
    println(latestdate.before(newdate));//false

##after
เปรียบเทียบวันที่ ผลที่ได้จะได้ true หรือ false

    Ex.
    Date olddate = new Date("05/11/2015");
    Date newdate = new Date("05/12/2015");
    Date latestdate = new Date();

    println(olddate.after(newdate));//false
    println(latestdate.after(newdate));//true

##equals
เปรียบเทียบวันที่ว่าเท่ากันหรือไม่ ผลที่ได้จะเป็น true หรือ false

    Ex.
    Date olddate = new Date("05/11/2015");
    Date newdate = new Date("05/11/2015");
    Date latestdate = new Date();

    println(olddate.equals(newdate));//true
    println(latestdate.equals(newdate));//false

##compareTo
เปรียบเทียบวันที่ 0 คือเท่ากัน 1 คือ มากกว่า -1 คือน้อยกว่า

    Ex.
    Date olddate = new Date("05/11/2015");
    Date newdate = new Date("05/11/2015");
    Date latestdate = new Date();

    println(olddate.compareTo(newdate));//0
    println(latestdate.compareTo(newdate));//1

##toString
แปลงค่าจาก date เป็น string

    Ex.
    Date today = new Date();
    println(today.toSting());
