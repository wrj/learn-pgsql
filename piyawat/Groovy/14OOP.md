##Class

    class className {

    }

##Method

    class className {
        returnType methodName(parameter){
            return returnType
        }
    }
    Ex.
    class Person {
        String fullname(String firstname,String lastname)
        {
            return "${firstname} ${lastname}";
        }
    }


ถ้า return type เป็น void ไม่ต้องมี return

    class Person {
        void fullname(String firstname,String lastname)
        {
            String fullname = "${firstname} ${lastname}";
            println(fullname);
        }
    }

##Properties
ค่า variable ที่ประกาศใน class

    class className {
        dataType varialble;
    }
    Ex.
    class Person {
        int personID;
        String firstname;
        String lastname;
    }

##Getter Setter
เป็นการ เซ็ตค่า กับ ดึงค่า properties ของ class ใช้ this นำหน้าชื่อ variable

    class Person {
        int personID;
        Sting firstname;

        //Setter
        void setPersonID(Int pId)
        {
            this.personID = pId;
        }

        //Setter
        void setFirstname(String pFirstname)
        {
            this.firstname = pFirstname;
        }

        //Getter
        Int getPersonID()
        {
            return this.personID;
        }

        //Getter
        String getFirstname()
        {
            return this.firstname;
        }
    }

##Instance
การประกาศ variable เพื่อนำ class มาใช้

    Person p = new Person();

##Extends
เป็นการสืบทอด class โดยจะมี properties และ method ของ class มาด้วย

    class Example {
       static void main(String[] args) {
          Student st = new Student();
          st.StudentID = 1;

          st.Marks1 = 10;
          st.name = "Joe";

          println(st.name);
       }
    }

    class Person {
       public String name;
       public Person() {}
    }

    class Student extends Person {
       int StudentID
       int Marks1;

       public Student() {
          super();
       }
    }

class Student extends class person มาใช้งานโดยเพิ่ม properties 2 ตัวคือ StudentID และ Marks1 ถ้านำ class Student ไปใช้ จะสามารถ get หรือ set properties name ของ person ได้

##Interfaces
เป็น class ที่กำหนด method โดย class ที่ใช้ interfaces จะต้องเขียน method ให้เหมือน interfaces

    class Example {
       static void main(String[] args) {
          Student st = new Student();
          st.StudentID = 1;
          st.Marks1 = 10;
          println(st.DisplayMarks());
       }
    }

    interface Marks {
       void DisplayMarks();
    }

    class Student implements Marks {
       int StudentID
       int Marks1;

       void DisplayMarks() {
          println(Marks1);
       }
    }