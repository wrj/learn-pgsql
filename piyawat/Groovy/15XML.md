#XML
โครงสร้างของ XML

    <root>
        <element>
            <tag></tag>
        </element>
    </root>
    Ex.
    <bookstore>

      <book>
        <title>Everyday Italian</title>
        <author>Giada De Laurentiis</author>
        <year>2005</year>
        <price>30.00</price>
      </book>

      <book>
          <title>Harry Potter</title>
          <author>J K. Rowling</author>
          <year>2005</year>
          <price>29.99</price>
        </book>

    </bookstore>

#Attribute
เป็นการเพิ่มข้อมูลลงไปใน element หรือ tag

    Ex.
    <bookstore>

      <book category="cooking">
        <title lang="en">Everyday Italian</title>
        <author>Giada De Laurentiis</author>
        <year>2005</year>
        <price>30.00</price>
      </book>

      <book category="children">
          <title lang="en">Harry Potter</title>
          <author>J K. Rowling</author>
          <year>2005</year>
          <price>29.99</price>
        </book>

    </bookstore>