#Method
การสร้าง method ของ groovy จะมี syntax

    def methodName(){
        //Method Code
    }
    Ex.
    class Example {
       static def DisplayName() {
          println("This is how methods work in groovy");
          println("This is an example of a simple method");
       }

       static void main(String[] args) {
          DisplayName();
       }
    }

ถ้าต้องการเรียก method ใน class เดียวกัน ต้องใส่ static นำหน้า method ถ้าจะเรียกใช้งานได้ ถ้าไม่ใส่ต้องสร้าง object ของ class ขึ้นมาเพื่อเรียกใช้งาน

    class Example {
           def DisplayName() {
              println("This is how methods work in groovy");
              println("This is an example of a simple method");
           }

           static void main(String[] args) {
                Example exampleObj = new Example()
                exampleObj.DisplayName()
           }
        }

##Method Parameters
การกำหนดค่า parameters ที่ส่งเข้าไปที่ method เพื่อใช้งาน

    def methodName(parameter1, parameter2, parameter3) {
       // Method code goes here
    }
    Ex.
    class Example {
       static void sum(int a,int b) {
          int c = a+b;
          println(c);
       }

       static void main(String[] args) {
          sum(10,5);
       }
    }

##Default Parameters
เป็นการกำหนดค่า default ให้ตัว parameters ซึ่งการกำหนดค่า default ตอนเรียก method ไม่จำเป็นต้องใส่ค่า parameter ก็ได้

    def someMethod(parameter1, parameter2 = 0, parameter3 = 0) {
       // Method code goes here
    }
    Ex.
    class Example {
       static void sum(int a,int b = 5) {
          int c = a+b;
          println(c);
       }

       static void main(String[] args) {
          sum(6);
       }
    }

##Method return values
เป็น method ที่มีการ return ค่ากลับมา จะต้องกำหนดประเภท variable ที่จะ return กลับมาก่อนชื่อ method และใน method ต้องใส่ return values; ปิดท้าย

    class Example {
       static int sum(int a,int b = 5) {
          int c = a+b;
          return c;
       }

       static void main(String[] args) {
          println(sum(6));
       }
    }
