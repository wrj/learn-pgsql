#Database
คำสั่งสร้าง instance สำหรับติดต่อ postgres database

    Sql.newInstance(URL Database,username, password, 'org.postgresql.Driver')
    EX.
    def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/dvdrental',
                    'postgres', 'postgres', 'org.postgresql.Driver')

##คำสั่ง Select ข้อมูล

    sql.rows(SQL Syntax);
    EX.
    def list = sql.rows("SELECT * FROM customer");