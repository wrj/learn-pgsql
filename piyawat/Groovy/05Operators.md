#Operators
ที่ใช้ใน groovy [Operators](https://www.tutorialspoint.com/groovy/groovy_operators.htm)

##ทางคณิตศาสตร์

<table>
<tr><td>Operators</td><td>ความหมาย</td></tr>
<tr><td>++</td><td>เพิ่มค่าที่ละ 1</td></tr>
<tr><td>--</td><td>ลดค่าทีละ 1</td></tr>
</table>

    Ex.
    Integer a = 0;
    a++ // เท่ากับ 1
    a-- // เท่ากับ 0

##การเปรียบเทียบ

<table>
<tr><td>Operators</td><td>ความหมาย</td></tr>
<tr><td>==</td><td>เท่ากับ</td></tr>
<tr><td>!=</td><td>ไม่เท่ากับ</td></tr>
<tr><td><</td><td>ค่าด้านซ้าย น้อยกว่า ค่าด้านขวา</td></tr>
<tr><td><=</td><td>ค่าด้านซ้าย น้อยกว่าหรือเท่ากับ ค่าด้านขวา</td></tr>
<tr><td>></td><td>ค่าด้านซ้าย มากกว่า ค่าด้านขวา</td></tr>
<tr><td>>=</td><td>ค่าด้านซ้าย มากกว่าหรือเท่ากับ ค่าด้านขวา</td></tr>
</table>

##Logical

<table>
<tr><td>Operators</td><td>ความหมาย</td></tr>
<tr><td>&&</td><td>และ</td></tr>
<tr><td>||</td><td>หรือ</td></tr>
<tr><td>!</td><td>ไม่</td></tr>
</table>

##การแทนค่า

<table>
<tr><td>Operators</td><td>ความหมาย</td></tr>
<tr><td>+=</td><td>บวก</td></tr>
<tr><td>-=</td><td>ลบ</td></tr>
</table>

    Ex.
    Integer a = 5
    a += 3 //มีค่าคือ 8
    a -= 3 //มีค่าคือ 5