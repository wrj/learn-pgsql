#Groovy

##Class
การสร้าง class ของ groovy ชื่อ class จะต้องตั้งชื่อให้เหมือนชื่อไฟล์ เช่น Example.groovy

    class Example {

    }

แต่ถ้าทำเป็น class แบบ execute ได้ต้องใส่ main ให้ด้วย เช่น

    class Example {
        static void main(String[] args){
            println "Hello"
        }
    }

##Method
การสร้าง method ของ groovy

    String fullname(){
        String fullname = ""
        return fullname
    }