##if...else
การใช้ if.....else เป็นการกำหนดการทำงานแบบมีเงื่อนไข

    if(condition){

    }else if(condition){

    }else{

    }
    Ex.
    int x = 1;
    if(x = 0){
        print("in x = 0")
    }else if(x = 1){
        print("in x = 1")
    }else{
        print("in else")
    }

##switch
เป็นการกำหนดการทำงานโดยใช้เงื่อนไขในการหา

    switch (values){
        case condition:
            //code
            break;
        default:
            //case default
            break;
    }
    Ex.
    int x = 2;
    switch (x) {
        case 0:
            print("in 0");
            break;
        case 1:
            print("in 1");
            break;
        case 2:
            print("in 2");
            break;
        case {it > 2 && it < 5}:
            print("between 3 and 5")
            break;
        default:
            print("in default");
            break;
    }