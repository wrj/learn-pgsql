#Maps
เป็น variable ที่สามารถเก็บค่าได้หลายๆ values ในรูปแบบ key : values

การสร้าง maps ใน groovy

    Ex.
    def mp = ["firstname":"John","lastname":"Doe"];
    def mp = [:];//Empty Maps

##containsKey
ใช้สำหรับ check ว่า maps มี key หรือไม่ จะได้ค่าเป็น true หรือ false

    Ex.
    def mp = ["firstname":"John","lastname":"Doe"];
    println(mp.containsKey("firstname"));//true
    println(mp.containsKey("nickname"));//false

##get
ใช้ดึง values จาก maps

    get(Object key)
    Ex.
    def mp = ["firstname":"John","lastname":"Doe"];
    println(mp.get("firstname"));//John

##keySet
ใช้ list key ทั้งหมดจาก maps จะได้ออกมาเป็น array

    Ex.
    def mp = ["firstname":"John","lastname":"Doe"];
    println(mp.keySet());//["firstname","lastname"]

##put
ใช้เพิ่ม key และ value ลงใน maps

    put(Object key, Object value)
    Ex.
    def mp = ["firstname":"John","lastname":"Doe"];
    println(mp.put("nickname","ball"));//["firstname":"John","lastname":"Doe","nickname":"ball"]

##size
หาขนาดของ maps

    Ex.
    def mp = ["firstname":"John","lastname":"Doe"];
    println(mp.size());//2

##values
ดึงข้อมูลของ maps ออกมาทั้งหมดในรูปแบบ array

    Ex.
    def mp = ["firstname":"John","lastname":"Doe"];
    println(mp.values());//["John","Doe"]