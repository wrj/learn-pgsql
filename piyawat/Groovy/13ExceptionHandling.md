#try{}catch(ExceptionName e1){}
เป็นการดัก error โดยไม่หยุด process ของ program

    try {
        def arr = new int[3];
        arr[5] = 5;
    } catch(Exception ex) {
        println("Catching the exception");
    }

    println("Let's move on after the exception");