#Loop

##while loop

เป็นการ Loop แบบกำหนดเงื่อนไข

    while(condition){
        //code
    }
    Ex.
    Integer count = 5
    while(count < 5){
        println(count)
        count++;
    }

##for loop

เป็นการ loop แบบกำหนดค่า

    for(variable;expression;Increment) {
        //code
    }
    Ex.
    for(Integer i = 0;i < 5;i++){//กำหนดให้ i = 0 loop ถ้า i น้อยกว่า 5 ให้เพิ่มจำนวน i ทีละ 1
        println(i)
    }

##for in
การ loop array

    for(datatype variable in array[])
    {
        println(variable)
    }
    Ex.
    int[] array = [0,1,2,3];
    for(int i in array)
    {
        println(i);
    }

##each

เป็นการ loop array หรือ list

    array.each{
        //code
    }

ในการใช้ each ค่าใน array เราจะเรียกใช้ด้วยคำว่า it

    Integer[] a = [1,2,3,4]
    a.each{
        println(it)
    }

##break
ใช้เพื่อหยุดการ loop

    for(Integer i = 1;i < 5;i++){
        println(i)
        if(i == 2)
        {
            break;
        }
    }

loop จะหยุดทำงานเมื่อ i เท่ากับ 2