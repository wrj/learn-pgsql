#Variable
การประกาศค่าตัวแปรที่ใช้ใน groovy

    class Example {
       static void main(String[] args) {
          // x is defined as a variable
          String x = "Hello";

          // The value of the variable is printed to the console
          println(x);
       }
    }

ชื่อตัวแปร ตัวอักษรตัวเล็ก ตัวใหญ่ จะเก็บคนละค่า

    class Example {
       static void main(String[] args) {
          // Defining a variable in lowercase
          int x = 5;

          // Defining a variable in uppercase
          int X = 6;

          // Defining a variable with the underscore in it's name
          def _Name = "Joe";

          println(x);
          println(X);
          println(_Name);
       }
    }

##Printing Variables
การ print ค่า variables ออกมาทาง console จะมี 2 แบบคื่อ

    println(Variable) จะแสดงผลตัวตัดบรรทัด
    print(Variable) จะแสดงผลต่อกัน

##การกำหนดค่าให้ String
การกำหนดค่า values ให้ String จะมี 2 แบบ

แบบที่ 1

    String x = "Hello"
    String y = "${x} world"
    print(y) จะได้ Hello world

การใช้ ${Variable} จะเป็นการแทนค่าตัวแปลลงไป สามารถใช้ในการคำนวนได้ด้วย เช่น

    Integer x = 5
    Integer y = 3
    String answer = "ผลรวมของ ${x} และ ${y} คือ ${x+y}"

แบบที่ 2

    String x = "Hello"
    String y = '${x} world'
    print(y) จะได้ ${x} world

การใช้ '' แทน "" จะไม่สามารถแทนค่า variable ได้ จะเห็นเป็น text เท่านั้น