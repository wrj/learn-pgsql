#JSON
โครงสร้างของ JSON แบบ 1 object จะมีโครงสร้างเหมือน Maps

    {key:value}

ถ้ามีหลายๆ object จะมีลักษณะ Array ที่มี Maps เป็น values

    [
        {key:value},
        {key:value}
    ]

ตัวอย่าง

    [
        {"firstname":"John","lastname":"Dou"},
        {"firstname":"Alan","lastname":"Smit"}
    ]

values ของ JSON สามารถเป็น array ได้ ตัวอย่าง

    {
        "firstname":"John",
        "lastname":"Dou"},
        "computer":["HP","LENOVO"]
    }
    หรือ
    {
        "firstname":"John",
        "lastname":"Dou"},
        "computer":[
            {"brand":"HP"},
            {"brand":"LENOVO"}
        ]
     }

##การสร้าง JSON ด้วย GROOVY
การสร้างjson จะใช้ function ของ groovy ที่ชื่อ JsonOutput

    Syntax
        JsonOutput.toJson(datatype obj)
    Ex.
        def person = ["name":"John","id":1]
        def output = JsonOutput.toJson(person)
        println(output);

##การนำ text มาทำเป็น json ใช้ใน groovy

    Syntax
        JsonSlurper.parseText()
    Ex.
        def jsonSlurper = new JsonSlurper()
        def object = jsonSlurper.parseText('{ "name": "John", "ID" : "1"}')

        println(object.get("name");
        println(object.get("ID");