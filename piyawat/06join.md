#JOIN

##INNER JOIN

เป็นการดึงข้อมูลโดยที่ ทั้ง 2 table ต้องมีข้อมูลเหมือนกัน

    SELECT
        table_name_1.column_name,
        table_name_2.column_name
    FROM
        table_name_1 INNER JOIN table_name_2
    ON
        table_name_1.primary_key = table_name_2.foreign_key

##LEFT JOIN

เป็นการดึงข้อมูลโดยที่ใช้ข้อมูลจาก table หลักเป็นหลัก อีก table ถ้าไม่มีข้อมูลจะแสดงข้อมูลเป็น null

    SELECT
        table_name_1.column_name,
        table_name_2.column_name
    FROM
        table_name_1 LEFT JOIN table_name_2
    ON
        table_name_1.primary_key = table_name_2.foreign_key