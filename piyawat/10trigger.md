#Trigger
เป็น function ที่สามารถสร้างเองได้

    CREATE OR REPLACE FUNCTION trigger_function() RETURN trigger AS
    $BODY$

    $BODY$
    LANGUAGE plpgsql;
    Ex.
        CREATE OR REPLACE FUNCTION log_last_name_changes()
        RETURNS trigger AS
        $BODY$
            BEGIN
                IF NEW.last_name <> OLD.last_name THEN
                    INSERT INTO employee_audits(employee_id,last_name,changed_on)
                    VALUES(OLD.id,OLD.last_name,now());
                END IF;

        RETURN NEW;
        END;
        $BODY$
        LANGUAGE plpgsql;

เป็นคำสั่งสร้าง function เพื่อนำไปใช้

    CREATE TRIGGER trigger_name {BEFORE | AFTER | INSTEAD OF} {event [OR ...]}
       ON table_name
       [FOR [EACH] {ROW | STATEMENT}]
       EXECUTE PROCEDURE trigger_function

เป็นคำสั่ง Blind function กับ table โดยมีการบอก Event ที่จะทำงาน