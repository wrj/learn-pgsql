#View

เป็น Table ที่สร้างจากคำสั่ง query

    CREATE VIEW view_name AS query;

Ex. ใช้ตัวอย่าง database ของ dvdrental

     DROP VIEW IF EXISTS customer_list;
     CREATE VIEW customer_list AS
     SELECT cu.customer_id AS id,
        ((cu.first_name || ' ') || cu.last_name) AS name,
        a.address,
        a.postal_code AS zip_code,
        a.phone,
        city.city,
        country.country,
            CASE
                WHEN cu.activebool THEN 'active'
                ELSE ''
            END AS notes,
        cu.store_id AS sid
       FROM customer cu
         JOIN address a ON cu.address_id = a.address_id
         JOIN city ON a.city_id = city.city_id
         JOIN country ON city.country_id = country.country_id