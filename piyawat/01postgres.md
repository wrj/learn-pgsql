* web สอน tutorial [postgresqltutorial](http://postgresqltutorial.com/)

#Primary Key และ Foreign Key
1. Primary Key คือ columns ที่ใช้ระบุ record ต้องเป็น uniquely คือห้ามซ้ำและห้ามว่าง primary key สามารถเป็นได้ทั้ง integer varchar หรือ UUID
2. Foreign Key คือ columns ที่ใช้ refer ถึง primary key จาก table อื่น

#Type

1.  SERIAL เป็นการสร้าง columne ที่มีค่า auto-increment
2.  VARCHAR เก็บข้อมูลที่เป็นตัวอักษร มีขนาดกำกับสูงสุดคือ 255 ตัวอักษร
3.  TEXT เก็บตัวอักษรขนาดเท่าไรก็ได้
4.  NUMERIC เก็บตัวเลขสามารถมีจุดทศนิยมได้เล่น 100.50
5.  INTEGER เก็บตัวเลขจำนวนเต็ม
6.  BOOLEAN เก็บ true,false
7.  DATE เก็บ วัน เดือน ปี
8.  TIME เก็บเวลา
9.  TIMESTAMP เก็บวัน เวลา

#ประเภทของ Relationships
##One to One
เป็น Relation แบบ 1 ต่อ 1 เช่น พนักงาน 1 คน มีอัตราเงินเดือนได้ 1 อัตรา
##One to Many
เป็น Relation แบบ 1 ต่อ กลุ่ม เช่น 1 บริษัท มีพนักงานได้หลายคน
##Many to Many
เป็น Relation แบบ กลุ่ม ต่อ กลุ่ม เช่น พนักงาน 1 คน อยู่ได้หลายตำแหน่ง 1 ตำแหน่งมีได้หลายคน