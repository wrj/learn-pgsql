#JSON และ XML
ใน grails controller สามารถระบุว่าจะให้ return เป็นแบบ html json หรือ xml ได้

    Ex.
    import grails.converters.JSON
    import grails.converters.XML

    class BookController {
        def list() {
            def books = Book.list()

            withFormat {
                html bookList:books
                json { render books as JSON }
                xml { render books as XML }
            }
        }
    }

    URL ที่ใช้เรียกคือ
        http://domain/books?format=json
        http://domain/books?format=xml
