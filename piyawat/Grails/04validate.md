#Validate
การสร้าง validate ทำหรับ properties โดยจะใส่ไว้ใน constraints ของ domain [validate](http://docs.grails.org/3.1.6/guide/single.html#validation)

    blank: boolean //ค่าว่าง
    nullable: boolean //ค่า null
    size: range //ค่าความยาวตัวอักษร
    email: boolean //pattern email
    unique: boolean //ห้ามซ้ำ
    max: [integer,date] //ค่ามากสุด
    min: [integer,date] //ค่าน้อยสุด
    range: range //กำหนดค่าเป็นช่วง

    *ค่า default ของ grails คือ nullable: false

    Ex.
    static constraints = {
        username blank:false,nullable:false,size: 4..12
    }

##validate และ Error
เป็นการเช็คค่าของ domain ว่ามี error หรือไม่

    validate() return true ถ้าไม่มี error,false ถ้ามี error
    hasError() return true ถ้ามี error,false ถ้าไม่มี error ต้องใช้หลังจากเรียก method validate()

    def b = new Book(title: "The Shining")
    if (!b.validate()) {
        b.errors.allErrors.each {
            println it
        }
    }
