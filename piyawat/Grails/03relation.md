#Relation
##One to One

    class Nose {
        Face face
    }
    class Face {
        static hasOne = [nose:Nose]
    }
    หมายความว่า หน้า 1 หน้ามีได้ 1 จมูก

##One to Many

    class Author {
        static hasMany = [books: Book]

        String name
    }
    class Book {
        String title
    }
    หมายความว่า ผู้แต่ง 1 คน แต่งหนังสือหลายเล่ม

##Many to Many

    class Author {
        static hasMany = [books: Book]

        String name
    }
    class Book {
        static belongTo = Author
        static hasMany = [authors: Author]

        String title
    }
    หมายความว่าผู้แต่ง 1 คน แต่งหนังสือได้หลายเล่ม และ หนังสือ 1 เล่มมีผู้แต่งได้หลายคน

##belongTo
เป็นการสร้าง Cascading behaviour ต้องใส่ที่ class ที่ไม่ใช่ class หลัก

    แบบ one to one,one to many
    class Author {
        static hasMany = [books:Book]

        String name
    }

    class Book {
        String title

        static belongTo = [author:Author]//properties:Class
    }

    แบบ Many to Many
    class Author {
        static hasMany = [books:Book]
        String name
    }
    class Book {
        static belongTo = Author
        static hasMany = [authors:Author]
        String title
    }