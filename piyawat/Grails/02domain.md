#Domain
ใช้แทน table ของ database

##การ map domain กับ table
โดยปกติ domain ของ grails จะสร้าง table ให้ตามชื่อของ domain แต่เราสามารถเปลี่ยนชื่อ table ได้ โดยใช้ properties table ที่ mapping

    static mapping = {
        table 'ชื่อ table ที่ต้องการ'
    }

##การสร้าง id serial ของ postgresql ใน grails

    static mapping = {
        id  generator: 'sequence', 
            params: [sequence:'sequence_name'],
            column:"ชื่อ column ของ id"
    }
    ต่อจาก static constraints = {}

##การสร้าง id แบบ Composite Key
เป็นการสร้าง id โดยดึง primary key จาก 2 table มาใช้งาน โดยกำหนดที่ mapping

    ที่ class จะใช้ implements Serializable
    class BasketProduct implements Serializable {
        Basket basket
        Product product
        static mapping = {
            id composite:['basket','product']
        }
    }

##version ของ record
ถ้าไม่ต้องการให้มี column version ให้ใส่ version false ที่ mapping

    static mapping = {
        version false
    }

##การสร้าง created_at และ updated_at แบบ auto
ถ้าต้องการให้ table มี column สำหรับเก็บวันที่สร้าง record และวันที่ update record แบบ timestamp ให้ใส่ properties

    Date dateCreated
    Date lastUpdated

##Data Type ของ Properties

    String  firstName
    Integer age
    Double  weight
    Class   face
    Date    birthDay
    Boolean status

    //ถ้าต้องการทำ column TEXT
    String  description

    static mapping = {
        description type:"text"
    }

##การกำหนดค่า default ให้ column

    String  firstName
    Integer age
    Double  weight
    Class   face
    Date    birthDay
    Boolean status

    //ถ้าต้องการทำ column TEXT
    String  description

    static mapping = {
        description type:"text"
        status defaultValue:"true"
    }


##สร้าง Comment ของ Table
การสร้าง comment ของ table และ column จะทำใน mapping

    static mapping = {
        comment "" //comment ของ table
        properties_name comment:""// comment ของ column
    }