package lgrails

import grails.converters.JSON

class PersonController {

    def index() {

    }

    def newForm(){

    }

    def updateForm(){

    }

    def create(){
        //Get Nose
        Nose n = Nose.get(params.nose_id)//select * from nose where id = ?
        //Get Face
        Face f = Face.get(params.face_id)//select * from face where id = ?
        //save
        Person p = new Person()
        p.setName("")
        p.setAge()
        p.setLastVisit(new Date())
        p.setNose(n)
        p.setFace(f)
        n.save()
        redirect(controller: "person",action: "index")
    }

    def update(){
        //save
        Person p = Person.get(params.id)//select * from person where id = ?
        p.setName("")
        p.setAge()
        p.setLastVisit(new Date())
        p.setNose(Nose.get(params.nose_id))
        p.setFace(Face.get(params.face_id))
        n.save()//update person
        redirect(controller: "person",action: "index")
    }

    def delete(){
        Person p = Person.get(params.id)//select * from person where id = ?
        p.delete()//delete from person where id = ?
        redirect(controller: "person",action: "index")
    }
}
