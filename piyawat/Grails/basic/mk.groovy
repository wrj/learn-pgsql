import groovy.sql.Sql

class Employee {

    static belongsTo = [branch:Branch]
    static hasMany = [payments:Payment]

    String employeeCode
    String firstName
    String lastName

    static constraints = {
    }

    def dataSource

    Double getTotalPayment() {
        Sql s = new Sql(dataSource)
        def result = s.rows("SELECT SUM(TOTAL) FROM PAYMENT WHERE employee_id = ${this.getId()}")
        return result.get(0).get("SUM")
    }
}

class mainController {

    def dataSource

    def branch() {
//        หารายชื่อพนักงาน
        Branch b = Branch.get(1)
        List e = Employee.findAllByBranch(b)//array [[Employee],[Employee],[Employee]]
        List e1 = b.getEmployees()//array [[Employee],[Employee],[Employee]]
//        หารายการ payment
        List p = Payment.findAllByBranch(b)//array [[Payment],[Payment],[Payment]]
        List p1 = b.getPayments()//array [[Payment],[Payment],[Payment]]
//        หาจำนวนพนักงาน
        Integer totalEmployee = Employee.countByBranch(b)

        String sqlText = "SELECT count(id) FROM employee WHERE branch_id = ${b.getId()}"
        Sql s = new Sql(dataSource)
        List result = s.rows(sqlText)//[["count":10]]
        println(result)
        Integer totalEmployeeSql = result.get(0).get("count")
    }

    def employee() {
        Employee e = Employee.get(1)
//        หาว่าอยู่สาขาไหน
        Branch b = e.getBranch()//Branch b = Branch.get()
//        หาว่าพนักงานออก Payment ไปกี่ใบ
        List p = e.getPayments()//array [[Payment],[Payment],[Payment]]
        Double total = 0.0
        p.each {
            total += it.getTotal()
        }
        for(Payment payment in p){
            total += payment.getTotal()
        }
        Double totalByMethod = e.getTotalPayment()
//        หาว่าออกใบเสร็จกี่ใบ
        Integer totalPayment = Payment.countByEmployee(e)
    }
}