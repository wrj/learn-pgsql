package lgrails

class Person {

    String name
    Integer age
    Date lastVisit
    Nose nose
    Face face

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_person_id']
    }
}
