package lgrails

class Nose {

    String noseColor
    String noseShap

    static belongsTo = [person:Person]

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_nose_id']
    }
}
