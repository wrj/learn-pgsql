package lgrails

class Face {

    String hairColor

    static belongsTo = [person:Person]

    static constraints = {
    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_face_id']
    }
}
