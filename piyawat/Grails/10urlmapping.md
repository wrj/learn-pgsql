##URLMAPPING
การ config path ของ url ที่เราจะใช้งาน โดยตั้งค่าที่ไฟล์ UrlMappings

    Ex.
        "/product"(controller: "product", action: "list")
        จะได้ url > http://domain/product/list
        "/product"(controller: "product")
        จะได้ url > http://domain/product

การ Group url

    group "/product", {
        "/apple"(controller:"product", id:"apple")
        "/htc"(controller:"product", id:"htc")
    }
    จะได้ url
        http://domain/product/apple
        http://domain/product/htc

การส่งค่า parameter

    "/blog/$year/$month/$day/$id"(controller: "blog", action: "show")
    จะได้ url http://domain/blog/2007/01/10/my_funky_blog_entry
