#Session
คือตัวแปรที่ถูกเก็บไว้ในฝั่ง server จะมีอายุกำหนดอยู่

##การใส่ข้อมูลลง session

    Syntax:
    session.setAttribute(key,value)
    Ex.
        session.setAttribute("username","myusername")

##การลบข้อมูลออกจาก session

    Syntax:
        session.removeAttribute(key)
    Ex.
        session.removeAttribute("username")

##การเช็คว่า session มี key อยู่หรือไม่

    Syntax:
        session.getAttribute(key)
        ถ้ามีจะได้ value ของ key ถ้าไม่มีจะได้ null
    Ex.
        session.setAttribute("username","myusername")
        println session.getAttribute("username") //จะได้ myusername
        println session.getAttribute("password") //จะได้ null
