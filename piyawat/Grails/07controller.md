#Controller
คือส่วนที่เอาไว้เขียน logic การทำงานของ web เช่นการดึงข้อมูล insert update delete การคำนวนต่างๆ
และเป็นส่วนจัดการ url ของ web ด้วย

##โครงสร้าง url

    http://localhost/controller/action

##โครงสร้างของ Controller

    class name_controller {

        def action_name() {

        }
    }

    Ex.
    class BranchController {

        def index() {
            //url = http://localhost/branch/index
        }

        def create() {
            //url = http://localhost/branch/create
        }
    }

##การใช้ Domain
method ของ domain มีรายละเอียดบอกตามนี้ [document](http://docs.grails.org/latest)

###การดึงข้อมูล
มีการดึงข้อมูลแบบ list และแบบ instances แบบ

- list จะเป็นแบบ list instances ของ domain class

    - findAll
    - getAll
    - list

- แบบ instances จะเป็นการดึงข้อมูลแบบเดี่ยว

    - get(id)
    - first()
    - last()

    Ex.
    def customerList = Customer.list()
    //จะได้ list ที่มี Domain customer
    def customerObj = Customer.get(1)
    //ได้ข้อมูลของ Domain Customer

###การ insert

    Customer customer = new Customer()
    customer.setFirstname('firstname')
    if(customer.validate())
    {
        customer.save()
    }else{
        customer.error.allError.each {
            println it
        }
    }

###การ update

    Customer customer = Customer.get(1)
    customer.setFirstname('firstname')
    if(customer.validate())
    {
        customer.save()
    }else{
        customer.error.allError.each {
            println it
        }
    }

###การ delete

    Customer customer = Customer.get(1)
    customer.delete()

###การดึงข้อมูลแบบ list

    def customerList = Customer.list()
    customerList.each {
        println it.getFirstname() //it คือ Class Customer
    }

###การดึงข้อมูล Relation

####แบบ one to many

    Branch branch = Branch.get(1)
    def staffList = branch.getStaff()
    //branch hasMany staff จะใช้ getStaff ดึงข้อมูลของ staff ออกมาเป็นแบบ list staff
    staffList.each {
        it.getFirstname()// it คือ Class staff
    }

####แบบ one to one

    Staff staff = Staff.get(1)
    Branch branch = staff.getBranch()
    //staff belongTo branch สามารถใช้ getBranch จะได้ class branch

