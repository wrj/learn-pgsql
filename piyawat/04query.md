#Query ข้อมูล

## คำสั่ง insert record

    INSERT INTO table_name (column_name,column_name) VALUES (values 1,value 2)

## คำสั่ง update record

    UPDATE table_name SET column_name = value 1,column_name = value 1 WHERE condition

*   การใช้คำสั่ง update ถ้าต้องการ update ข้อมูลทั้งหมด ไม่ต้องใส่ where แต่ถ้า update record ต้องใส่ where เพื่อระบุ record ที่ต้องการ update

## คำสั่ง delete record

    DELETE FROM table_name WHERE condition

*   การใช้คำสั่ง delete ถ้าต้องการ delete ข้อมูลทั้งหมด ไม่ต้องใส่ where แต่ถ้า delete record ต้องใส่ where เพื่อระบุ record ที่ต้องการ delete