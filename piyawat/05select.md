#Query ข้อมูล

##คำสั่ง select ข้อมูลทั้งหมด

    SELECT column_name,column_name FROM table_name;

##คำสั่ง select ข้อมูลโดยมีเงื่อนไข

    SELECT column_name,column_name FROM table_name WHERE conditions;

##คำสั่งเรียงลำดับข้อมูล

    SELECT column_name,column_name FROM table_name ORDER BY column_name (asc,desc);

##คำสั่งจำกัดการแสดงผลข้อมูล

    SELECT column_name,column_name FROM table_name LIMIT number_row;

## คำสั่ง skip ข้อมูล

    SELECT column_name,column_name FROM table_name OFFSET number_row;

##คำสั่ง Distinct

เป็นคำสั่งให้แสดงข้อมูลที่ซ้ำกัน ออกมาแค่ record เดียว

    SELECT DISTINCT
     column_1
    FROM table_name

##คำสั่ง GROUP BY

การทำงานจะคล้ายๆ distinct แต่จะสามารถใช้คำสั่ง SUM,COUNT ข้อมูลใน GROUP ได้

    SELECT column_name,count(column_name) FROM table_name GROUP BY column_name;

##คำสั่ง AS

เป็นคำสั่งที่เปลื่ยนชื่อ column ที่ select ขึ้นมา

    EX.
        SELECT name AS first_name FROM users GROUP BY name;

##การกำหนด alias ของ table

เป็นการกำหนดชื่อ table ในคำสั่ง query

    EX.
        SELECT u.name FROM users u
