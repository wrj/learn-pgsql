package mk

class Payment {

    Double totalPrice
    Date paymentDate

    static belongsTo = [staff:Staff,branch:Branch]
    static hasMany = [paymentItems:PaymentItem]

    static constraints = {
    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_payment_id']
    }


    Double getTotalPrice(){
        Double total = 0.0
        this.getPaymentItems().each {
            total += it.getTotal()
        }
        return total
    }

}
