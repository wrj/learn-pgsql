package mk

class Mcategory {

    String name
    Date lastUpdate

    static hasMany = [menus:Menu]

    static constraints = {
        name blank: false
    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_mcategory_id']
    }
}
