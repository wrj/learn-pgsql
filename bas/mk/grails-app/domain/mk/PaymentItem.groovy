package mk

class PaymentItem {

    Integer amount
    Double total
    Date lastUpdate

    static belongsTo = [payment:Payment,menu:Menu]

    static constraints = {
        amount min: 0
    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_payment_item_id']
    }
}
