package mk

class Menu {

    String name
    Integer amount
    Double price
    Date lastUpdate

    static belongsTo = [mcategory:Mcategory]
    static hasMany = [paymentItems:PaymentItem]

    static constraints = {
        name blank: false
        amount min: 0
    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_menu_id']
    }
}
