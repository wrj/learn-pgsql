package mk

class Staff {

    String firstName
    String lastName
    Boolean active
    Date lastUpdate

    static belongsTo = [branch:Branch]
    static hasMany = [payments:Payment]

    static constraints = {
        firstName blank: false
        lastName blank: false
    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_staff_id']
    }

    Integer amountPayment(){
        return this.getPayments().size()

    }


}
