package mk

class Branch {

    String name
    Boolean active
    Date lastUpdate

    static hasMany = [staff:Staff,payment:Payment]

    static constraints = {
        name blank: false
    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_branch_id']
    }

    Double getTotalPrice(){
        Double total = 0.0
        this.getPayment().each {
            total += it.getTotalPrice()
        }
        return total
    }
}
