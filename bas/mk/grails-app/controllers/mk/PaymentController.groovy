package mk

class PaymentController {

    def index() {
        new Payment(totalPrice: 170,paymentDate: new Date()).save()
    }

    def create(){
        Branch branch = Branch.get(1)
        Payment payment = new Payment()
        payment.setTotalPrice(350.00)
        payment.setPaymentDate(new Date())
        payment.setBranch(branch)
        if(payment.save())
        {
            PaymentItem paymentItem = new PaymentItem()
            paymentItem.setAmount(2)
            paymentItem.setTotal(70)
            paymentItem.setLastUpdate(new Date())
            paymentItem.setPayment(payment)
            paymentItem.save()
        }else{
            payment.errors.allErrors.each {
                println(it)
            }
        }
    }
}
