package mk

class McategoryController {

    def index() {
        new Mcategory(name: "ชองหวาน",lastUpdate: new Date()).save()
    }

    def create(){
        Mcategory mcategory = new Mcategory()
        mcategory.setName("ของคาว")
        mcategory.setLastUpdate(new Date())
        mcategory.save()
    }
}
