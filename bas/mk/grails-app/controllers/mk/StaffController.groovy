package mk

class StaffController {

    def index() {
        new Staff(firstName: "Leo",lastName: "Vinci",active: true,lastUpdate: new Date()).save()

    }


    //ไว้เพิ่มข้อมูลลงดาต้าเบส
    def create(){
        Branch branch = Branch.get(1)//เก็ทค่าข้อมูลของสาขาที่ไอดีที่1 ไว้ที่ branch
        Staff staff = new Staff() //สร้างอินแสตนของ staff
        staff.setFirstName("Mona")//เซ็ทข้อมูลเฟิสเนมของสตาฟ
        staff.setLastName("Lisa")
        staff.setActive(true)
        staff.setBranch(branch)
        staff.setLastUpdate(new Date())
        staff.save()
    }

    Double getTotalPrice(){
        Double total = 0.0
        this.getPaymentItems().each {
            total += it.getTotal()
        }
        return total
    }

}
