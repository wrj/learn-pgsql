package mk
import grails.converters.JSON


class BranchController {

    def index() {
        new Branch(name: "พระราม9",active: true,lastUpdate: new Date()).save()
    }

    def create(){
        Branch branch = new Branch()
        branch.setName("พระราม2")
        branch.setActive(true)
        branch.setLastUpdate(new Date())
        branch.save()
    }

    def update(){
        Branch branch = Branch.get(1)
        branch.setName("พระราม3")
        branch.save()

    }

    def list(){
        def b = Branch.getAll()
        List listJ = []
        b.each {
            def mapJ = [:]
            mapJ.put("Date",new Date())
            mapJ.put("id",it.getId())
            mapJ.put("name",it.getName())
            mapJ.put("totalPrice",it.getTotalPrice())
            def listEmployee = []
            def e = it.getStaff()
            e.each {
                def mapEmployeeJson = [:]
                mapEmployeeJson.put("staff",it.getFirstName())
                listEmployee.add(mapEmployeeJson)
            }
            mapJ.put("employee",listEmployee)
            def listPayment = []
            def p = it.getPayment()
            p.each {
                def mapPaymentJson = [:]
                mapPaymentJson.put("payment",it.getTotalPrice())
                listPayment.add(mapPaymentJson)
            }
            mapJ.put("payment",listPayment)
            listJ.add(mapJ)
        }
        render listJ as JSON
    }
}
