package gameshop

class Brand {

    String name

    static hasMany = [product:Product]

    static constraints = {
        name blank: false
    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_brand_id']
    }
}
