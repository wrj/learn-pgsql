package gameshop

class Address {

    String description
    String province
    String destrict
    String zipcode
    Customer customer

    static belongsTo = [Customer]

    static constraints = {
        description type:"text",nullable: true,blank: false
        province blank: false
        destrict blank: false
        zipcode blank: false

    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_address_id']
    }
}
