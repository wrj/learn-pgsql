package gameshop

class Category {

    String name
    Category parent

    static belongsTo = [mainCategory:Category]

    static hasMany = [products:Product]
    static constraints = {
        mainCategory nullable: true
    }

    static mappedBy = [ mainCategory: "id", parent: "id" ]

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_category_id']
    }
}
