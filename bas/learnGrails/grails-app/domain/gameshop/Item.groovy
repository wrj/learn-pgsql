package gameshop

class Item {

    Integer amout
    Double totalPrice
    Payment payment
    Product product

    static belongsTo = [Payment,Product]

    static constraints = {
        amout min: 0,maxSize: 999
        totalPrice scale: 2
    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_item_id']
    }
}
