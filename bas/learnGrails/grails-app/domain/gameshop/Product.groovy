package gameshop

class Product {

    String name
    String description
    Double price
    Integer amount
    Brand brand
    Category category

    static hasMany = [item:Item]

    static belongsTo = [Category,Brand]
    static constraints = {
        name blank: false
        description type: "text",blank: false
    }

    static mapping = {
        id generator: 'sequence' , params: [sequence:'seq_product_id']
    }
}
