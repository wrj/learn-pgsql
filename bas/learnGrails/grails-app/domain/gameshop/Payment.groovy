package gameshop

class Payment {

    Double total_price
    Date paymentDate
    Customer customer

    static belongsTo = [Customer]

    static hasMany = [items:Item]

    static constraints = {

    }

    static mapping = {
        id generator:'sequence',params: [sequence:'seq_payment_id']
    }
}
