package gameshop

class Customer {

    String firstName
    String lastName
    String email
    String phoneNumber

    static hasMany = [payments:Payment,address:Address]

    static constraints = {
        firstName blank: true
        lastName blank: true
        email email: true , blank: false
        phoneNumber blank: false

    }

    static mapping = {
        id generator: 'sequence', params: [sequence:'seq_customer_id']
    }
}
