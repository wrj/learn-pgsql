package gameshop


import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(UrlMappingInterceptor)
class UrlMappingInterceptorSpec extends Specification {

    def setup() {
    }

    def cleanup() {

    }

    void "Test urlMapping interceptor matching"() {
        when:"A request matches the interceptor"
            withRequest(controller:"urlMapping")

        then:"The interceptor does match"
            interceptor.doesMatch()
    }
}
