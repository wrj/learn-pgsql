##04/07/2016

ทบทวนความรุ้เรื่อง Groovy 
ติดตั้ง Grail และลองใช้
set data source
เรียน GORM ลองสร้าดาต้าเบสด้วย Grail 
เรียนวิธีสร้าง class สร้าง Domain สร้าง Controller

##05/07/2016

เรัียยน Domain
เรียน Validate ค่าก่อนที่จะเก็บลงไปใน Database
เรียนรู้ ORM การสร้าง อ่าน ลบ และแก้ไข one to one one to many many to many

###ORM

     static hasMany = [films:Film]
     static hasOne = [films:Film]
     static belongTo = Film

###Validate

        class User {
            String login
            String password
            String email
            Integer age
            static

            constraints = {
                login size: 5..15, blank: false, unique: true  //บอกว่าให้ ตัวอักษรมีค่าตั้งแต่ 5 - 15 ตัวอักษร และห้ามว่าง และห้ามซ้ำกับตัวอื่น
                password size: 5..15, blank: false // บอกให้ ตัวอักษรมีค่าได้แค่ 5 -15 ตัวอักษร และห้ามว่าง
                email email: true, blank: false  // เช็คให้ email ต้องพิมตาม format email เท่าทั้นและ ห้ามว่าง
                age min: 18 // กำหนดให้ขั้นต่ำ คือห้ามต่ำกว่า 18
            }

        }
        
##06/07/2016

ฝึกทำ relational domain มีความเข้าใจในการสร้าง domain แบบมีความสัมพันธ์กันมากขึ้น
ลองใช่ init เพืือทดลองว่าโค้ดที่เราเขียนลงไปใช้ได้หรือเปล่าแอดค่าเข้าไปในเทเบิ้ลได้ไหม


