# ให้ออกแบบฐานข้อมูลดังนี้
## ร้านเกมส์ [gameshop]

1.	เก็บข้อมูลต่อไปนี้
    
    * ประเภทสินค้า    หมวดหรือประเภทของสินค้า ต้องมีหมวดย่อยด้วย ออกแบบยังไง
    * สินค้าที่มี    สินค้าทั้งหมดของร้านมีอะไรบ้าง
    * ลูกค้า    รายละเอียดรถ/ลูกค้าที่เข้ามารับบริการ
    * รายการขาย    แต่ละ transaction ที่ให้ขายให้ลูกค้า ขายเมื่อไหร่ ให้ใคร ขายอะไรบ้าง รวมกี่บาท
    
2.	ให้ออกแบบ table และเขียน comment ใน sql / er diagram
3.	เลือก data type, relationship ที่เหมาะสม

---
# Database gameshop summary
1.TABLE customer เป็นตารางเก็บข้อมูลส่วนตัวของลูกค้า
    
2.TABLE address เป็นตารางเก็บที่อยู่ของลูกค้า

    * Column description จะเป็นการใส่รายละเอียดที่อยู่ที่ลูกค้าต้องกรอก เช่น (Street Address, P.O. Boxes, c/o, apartment, suite, unit, building, floor, etc)

3.TABLE product ตารางเก็บสินค้าของร้านขายเกมทั้งหมด ทุกสินค้าจะอยู่ที่ตารางนี้

4.TABLE brand ตารางเก็บนี่ห้อของตัวสินค้าเพราะสินค้าแต่ละตัวจะมีแบรนด์ต่างกันไป 

5.TABLE category เป็นตารางเก้บประเภทของสินค้าจะมีทั้งสินค้าหัวข้อใหญ่และสินค้าหัวข้อย่อยอยู่ในตารางนี้
    
    * Column root จะเป็น Boolean เพื่อเช็คว่าตัว category ตัวไหนถ้าเป็นประเภทสินค้าหมวดใหญ่จะให้เป็น true และสินค้าหมวดย่อยลงมาจะเป็น false

6.TABLE sub_category เป็นตารางที่จะกำหนดชัดเจนว่าประเภทสินค้าอะไร อยู่ในประเภทสินค้าอะไร เช่น ถ้าเรามีประเภทใหญ่คือเกม(game) ประเภทที่ย่อยออกมาก็ประเภทของเกม(Action,Rpg) เป็นต้น 
    
    * Column parent จะเป็น category ที่เป็นประเถทใหญ่
    * Column child จะเป็น  category ที่ย่อยลงมา
       
7.TABLE product_category เป็นตารางความสัมพันธ์ระหว่างตารางสินค้าและประเภทสินค้า 

8.TABLE payment ตารางเก็บข้อมูลการขายสินค้า ใครเป็นคนซื้อ ขายไปตอนไหน ทั้งหมดราคาเท่าไหร่

    * Column total_price ในส่วนของตาราง Payment จเป็นการเก็บยอดรวมทั้งหมดของราคาสินค้าที่ลูกค้าซื้อทั้งหมด

9.TABLE item เป็นตารางเก็บข้อมูลเกี่ยวกับสินค้าที่ลูกค้าซื้อว่าซื้อจำนวนกี่ชิน

    * Column total_price ในส่วนของตาราง item จะเป็นการเก็บยอดรวมราคาสินค้าแต่ละสินค้า

##ความสัมพันธ์ระหว่างตาราง

One to Many

    1. TABLE customer - TABLE address
    2. TABLE customer - TABLE payment
    3. TABLE product  - TABLE brand
    4. TABLE payment  - TABLE item
    5. TABLE item     - TABLE product
    6. TABLE category - TABLE product_category
    7. TABLE product  - TABLE product_category
    
Many to Many
    
    1. TABLE category - TABLE sub_category
    

# 
-- ตารางโชว์ ข้อมูลสินค้าที่อยู่ใน category

    SELECT
        c.category_id,
        c.name as category_name,
        ch.name as child_category_name,
        p.name
    FROM
        sub_category sc
        =INNER JOIN category c ON c.category_id = sc.parent_id
        INNER JOIN category ch ON sc.child_id = ch.category_id
        INNER JOIN product_category pc ON ch.category_id = pc.category_id
        INNER JOIN product p ON pc.product_id = p.product_id
        WHERE  c.root = TRUE;

-- ตารางแสดงบิลที่ลูกค้าได้ชำระเงินไป
    SELECT
        p.payment_id,
        c.first_name,
        pr.name,
        p.total_price,
        p.payment_date
    FROM
        customer c
        INNER JOIN payment p ON p.customer_id = c.customer_id
        INNER JOIN item i ON  i.payment_id = p.payment_id
        INNER JOIN product pr ON  i.product_id = pr.product_id;

-- ใช้วิธี sub query
    SELECT
        p.payment_id,
        c.first_name,
        (SELECT name FROM product WHERE product.product_id = i.product_id ),
        p.total_price,
         p.payment_date
    FROM
        customer c
        INNER JOIN payment p ON p.customer_id = c.customer_id
        INNER JOIN item i ON  i.payment_id = p.payment_id
    

    