package Restaurant

/**
 * Created by ASUS on 6/26/2017.
 */
class ROrder {

    Integer menu1 = 0
    Integer menu2 = 0
    Integer menu3 = 0

    void addMenu(Integer amountMenu1, Integer amountMenu2, Integer amountMenu3){
        this.menu1 += amountMenu1
        this.menu2 += amountMenu2
        this.menu3 += amountMenu3
    }
    void removeMenu(Integer amountMenu1, Integer amountMenu2, Integer amountMenu3){
        if(amountMenu1 > 0 && amountMenu1 <= this.menu1){
            this.menu1 -= amountMenu1
        }
        if(amountMenu2 > 0 && amountMenu2 <= this.menu2){
            this.menu2 -= amountMenu2
        }
        if(amountMenu3 > 0 && amountMenu3 <= this.menu3) {
            this.menu3 -= amountMenu3
        }
        else {
            println("cannot remove item")
        }
    }

    Integer total(){
        Integer menu1Price = 40
        Integer menu2Price = 50
        Integer menu3Price = 60

        Integer totalPrice = 0
        totalPrice += (menu1Price * this.menu1) + (menu2Price * this.menu2) + (menu3Price * this.menu3)

        return totalPrice
    }
}
