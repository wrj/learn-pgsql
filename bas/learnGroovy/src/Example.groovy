import CartPromotion.Cart
import CartPromotion.Promotion

class Example {

    static void main(String[] args) {

        //23/06/2017
        Cart c = new Cart()
        c.total=0
        c.addToCart(1,35);
        c.addToCart(3,35);
        c.removeCart(2,35);
        println(c.getSummary())

        Double totalPrice = c.getTotal();
        Promotion p = new Promotion();
        Double totalDisCount = p.calculateDiscount(c.getAmount(),c.getTotal());
        println("ส่วนลด ${totalDisCount} บาท")
        println("รวมเป็นเงิน ${totalPrice - totalDisCount} บาท")

        //22/06/2017
        //DatabaseExample.customerList()

//        Calculator calculator = new Calculator()
//
//        calculator.setResult(10)
//        println(calculator.cPlus(2)) //12
//        println(calculator.cMiner(3)) //9
//        println(calculator.cMultiply(2)) //18
//        println(calculator.cDivine(18)) //1
//
//        println(calculator.getResult()) //1



    }

    /*static void main(String[] args) {
 Phone Nphone = new  Phone()

       Nphone.setBrand("Sumsung")
       Nphone.setOs("Android")
       Nphone.setName("S8")
       Nphone.setCpu("i7")
       Nphone.setRam("4GB")
       Nphone.setPrice(28900.00)
 }*/
}
