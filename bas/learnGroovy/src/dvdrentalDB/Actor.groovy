package dvdrentalDB

import groovy.sql.Sql

/**
 * Created by ASUS on 6/30/2017.
 */
class Actor {
    String actorId
    String firstName
    String lastName

    List getActorList(Integer limit = 5){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = "SELECT actor_id, first_name, last_name FROM actor limit ${limit}"
        List actorList =  sql.rows(sqlText)
        sql.close()

        return actorList
    }

    void getActorById(Integer actorId){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = "SELECT actor_id, first_name, last_name FROM actor WHERE actor_id = ${actorId}"
        List actorList =  sql.rows(sqlText)

        if(!actorList.isEmpty()){
            def actorObj = actorList.get(0)

            this.actorId = actorObj.get("actor_id")
            this.firstName = actorObj.get("first_name")
            this.lastName = actorObj.get("last_name")
        }
    }

    List getActorFilm(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = $/ SELECT
                                f.title
                            FROM
                                actor a
                                    INNER JOIN film_actor fa ON fa.actor_id = a.actor_id
                                    INNER JOIN film f ON fa.film_id = f.film_id
                            WHERE
                                a.actor_id = ${this.actorId}
                         /$

        List actorFilmList = sql.rows(sqlText)

        return actorFilmList
    }

    Integer countActorFilm(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = $/ SELECT
                                count(f.title)
                            FROM
                                actor a
                                    INNER JOIN film_actor fa ON fa.actor_id = a.actor_id
                                    INNER JOIN film f ON fa.film_id = f.film_id
                            WHERE
                                a.actor_id = ${this.actorId}
                         /$

        List countActor = sql.rows(sqlText)
        sql.close()

        def countActorMap = countActor.get(0)

        return countActorMap.get("count")
    }

}
