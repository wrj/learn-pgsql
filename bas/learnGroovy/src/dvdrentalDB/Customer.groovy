package dvdrentalDB

import groovy.sql.Sql

/**
 * Created by ASUS on 6/26/2017.
 */
class Customer {

    List getCustomer(String searchText,Integer active, Integer limit = 5) {
        def dvdrentalSql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                                                    'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = $/ 
                SELECT 
                    first_name, last_name 
                FROM 
                    customer 
                WHERE first_name ILIKE '%${searchText}%' AND active = ${active}
                LIMIT ${limit}
                /$
        def customerList = dvdrentalSql.rows(sqlText)
        dvdrentalSql.close()
        return  customerList
    }
}
