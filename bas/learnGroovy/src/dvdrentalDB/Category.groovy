package dvdrentalDB

import groovy.sql.Sql

class Category {

    //
    Integer categoryId
    String name

    List getCategoryList(Integer limit) {
        def dvdrentalSql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')
        def categoryList = dvdrentalSql.rows("SELECT category_id,name FROM category ORDER BY name ASC LIMIT ${limit}")
        dvdrentalSql.close()
        return  categoryList
    }

    void getCategoryById(Integer pCategoryId){
        def dvdrentalSql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                                          'postgres', 'ADMIN*', 'org.postgresql.Driver')
        def categoryList = dvdrentalSql.rows("SELECT category_id, name FROM category WHERE category_id = ${pCategoryId}")
        dvdrentalSql.close()
        //ถ้ามีข้อมูลค่าจะเท่ากับ 1
        if(!categoryList.isEmpty()){
            def categoryObj = categoryList.get(0)
            println("category > ${categoryList}")
            this.categoryId = categoryObj.get("category_id")
            this.name = categoryObj.get("name")
        }
    }

    void updateCategory(){
        def dvdrentalSql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                                          'postgres', 'ADMIN*', 'org.postgresql.Driver')
        String sqlText = "UPDATE category SET name = '${this.name}' WHERE category_id = ${this.categoryId}"
        dvdrentalSql.execute(sqlText)
        dvdrentalSql.close()
    }

    void insertCategory(){
        def dvdrentalSql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')
        String sqlText = "INSERT INTO category (name) VALUES ('${this.name}')"
        dvdrentalSql.execute(sqlText)
        dvdrentalSql.close()
    }

    void insertCategoryByName(String categoryName){
        def dvdrentalSql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')
        String sqlText = "INSERT INTO category (name) VALUES ('${categoryName}')"
        dvdrentalSql.execute(sqlText)
        dvdrentalSql.close()
    }
}
