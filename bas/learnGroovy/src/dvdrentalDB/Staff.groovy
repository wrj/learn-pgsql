package dvdrentalDB

import groovy.sql.Sql

/**
 * Created by ASUS on 6/27/2017.
 */
class Staff {

    Integer staffId
    String firstName
    String lastName
    Integer addressId
    String email
    Integer storeId
    String username
    String password

    //เป็น Method สำหรับเรียกดูสตาฟแบบเรียกจาก ID ที่เราต้องการจะเรียก
    List getStaffList(String serchText = "",Boolean active,Integer limit){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        def staffList = sql.rows("SELECT staff_id,first_name,last_name,address_id,email,store_id,username,password FROM staff limit ${limit}")
        sql.close()
        return staffList
    }

    void getStaffById(Integer pStaffId){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        def staffList = sql.rows("SELECT staff_id,first_name,last_name,address_id,email,store_id,username,password FROM staff WHERE staff_id = ${pStaffId}")
        sql.close()

        if(!staffList.isEmpty()){ //เช็คว่าในตารางสตาฟมีข้อมูลอยู่หรือเปล่า ถ้าไม่มีฝห้เข้าไปทำใย if โดยใน if นี้จะเป็นตัวที่เก็บค่าข้อมูลต่างๆไปไว้ใน Properties
            def staffObj = staffList.get(0) //ทำ Map ขึ้นมาเพื่อจะได้แยกว่าในแต่ละข้อมูลเก็บอะไรไว้บ้างโดยเราใช้ Index ที่ 0 เพราะค่าที่เราเรียกมาใน Method นี้คือมีข้อมูลอยุ่แค่ rows เดียวอยู่แล้ว
            this.staffId = staffObj.get("staff_id")
            this.firstName = staffObj.get("first_name")
            this.lastName = staffObj.get("last_name")
            this.addressId = staffObj.get("address_id")
            this.email = staffObj.get("email")
            this.storeId = staffObj.get("store_id")
            this.username = staffObj.get("username")
            this.password = staffObj.get("password")
        }

    }
    //เรียกข้อมูลที่อยู่ของสตาฟ
    def getAddress(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        def addressList = sql.rows("SELECT a.address_id,a.address,a.address2,a.district,c.city,a.postal_code,a.phone FROM address a INNER JOIN city c ON c.city_id = a.city_id WHERE a.address_id = ${this.addressId}")
        sql.close()

        def addressObj = [:]
        if(!addressList.isEmpty()){
            addressObj = addressList.get(0)
        }

        return addressObj
    }

    Integer getPaymentCount(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        def paymentCount = sql.rows("SELECT COUNT(payment_id) as count_payment FROM payment WHERE staff_id = ${this.staffId}")
        sql.close()
        def paymentCountObj = paymentCount.get(0)
        return paymentCountObj.get("count_payment")
    }

    Double getPaymentSum(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        def paymentSum = sql.rows("SELECT SUM(amount) as sum FROM payment WHERE staff_id = ${this.staffId}")
        sql.close()
        def paymentSumObj = paymentSum.get(0)
        return paymentSumObj.get("sum")
    }
}
