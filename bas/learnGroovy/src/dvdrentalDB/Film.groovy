package dvdrentalDB

import groovy.sql.Sql

/**
 * Created by ASUS on 6/29/2017.
 */
class Film {
    Integer filmId
    String title
    String description
    Integer releaseYear
    Integer languageId
    Integer renDuration
    Double renRate
    Integer lenght
    Double replacementCost
    String rating
    String specialFuture
    String fulltext

    List getFilmList(String searchText,Integer rentalRate = 0, Integer limit = 5){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = $/
                        SELECT 
                            title, release_year, rental_rate ,
                        FROM film
                        WHERE title ILIKE '%${searchText}%' AND
                              rental_rate >= ${rentalRate}
                        LIMIT ${limit}/$
        List filmList = sql.rows(sqlText)
        sql.close()

        return  filmList
    }

    void getFilmById(Integer pFilmId){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        List filmList = sql.rows("SELECT film_id, title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features, fulltext FROM film WHERE film_id =  ${pFilmId}")
        sql.close()

        if(!filmList.isEmpty()){
            def filmObj = filmList.get(0)

            this.filmId             = filmObj.get("film_id")
            this.title              = filmObj.get("title")
            this.description        = filmObj.get("description")
            this.releaseYear        = filmObj.get("release_year")
            this.languageId         = filmObj.get("language_id")
            this.renDuration        = filmObj.get("rental_duration")
            this.renRate            = filmObj.get("rental_rate")
            this.lenght             = filmObj.get("length")
            this.replacementCost    = filmObj.get("replacement_cost")
            this.rating             = filmObj.get("rating")
            this.specialFuture      = filmObj.get("special_features")
            this.fulltext           = filmObj.get("fulltext")
        }
    }

    void getFilmActor(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = $/ 
                SELECT 
                    f.title,
                    concat(a.first_name,a.last_name) as fullname
                FROM 
                    film f
                INNER JOIN film_actor fa ON f.film_id = fa.film_id
                INNER JOIN actor a ON a.actor_id = fa.actor_id
                WHERE f.film_id = ${this.filmId}
                /$
        List filmActorList = sql.rows(sqlText)
        sql.close()

        println(filmActorList)

    }

    void getFilmLanguage(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = $/ 
                SELECT 
                    title,
                   (SELECT name FROM language WHERE language_id = ${this.languageId})
                FROM 
                    film 
                WHERE film_id = ${this.filmId}
                /$
        List filmLanguageList = sql.rows(sqlText)
        sql.close()

        println(filmLanguageList)

    }

    def countFilmActor(){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/DVDRental',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = $/ 
                SELECT
                    title,
                    (SELECT count(actor_id) FROM film_actor WHERE film_actor.film_id = film.film_id)
                FROM
                    film
                WHERE film_id = ${this.filmId}
                /$

        List filmActorList = sql.rows(sqlText)
        sql.close()

        def filmActorObj = filmActorList.get(0)

        return filmActorObj.values()
    }



}
