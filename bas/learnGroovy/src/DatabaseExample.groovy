
class DatabaseExample {

    static void customerList() {
        def personList = []

        def person = ["nametitle": "นาย", "firstName": "Pathawi", "lastName": "Wichian", "age": 20]
        personList.add(person)
        person = [:]

        person.put("nametitle", "Mr")
        person.put("firstName", "Dave")
        person.put("lastName", "Johnson")
        person.put("age", 32)
        personList.add(person)

        for(Integer i =0;i<personList.size();i++){

            def pList = personList.get(i)
            println(pList.get("firstName"))
            

        }

        int i = 0

        while(i < personList.size()){
            def pList = personList.get(i)
            println(pList.get("lastName"))

            i++
        }

        personList.each {
            print(it.get("firstName")+ " ")
            println(it.get("lastName"))
        }

        for(def j in personList){
            println(j.get("age"))
        }
    }

}
