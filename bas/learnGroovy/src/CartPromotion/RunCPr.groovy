package CartPromotion

/**
 * Created by ASUS on 6/23/2017.
 */
class RunCPr {
    static void main(String[] args) {

        //23/06/2017
        Cart c = new Cart()
        c.total=0
        c.addToCart(1,35);
        c.addToCart(3,35);
        c.removeCart(2,35);
        println(c.getSummary())

        Double totalPrice = c.getTotal();
        Promotion p = new Promotion();
        Double totalDisCount = p.calculateDiscount(c.getAmount(),c.getTotal());
        println("ส่วนลด ${totalDisCount} บาท")
        println("รวมเป็นเงิน ${totalPrice - totalDisCount} บาท")
    }

}
