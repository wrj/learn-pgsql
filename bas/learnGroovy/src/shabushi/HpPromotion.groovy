package shabushi

/**
 * Created by ASUS on 6/26/2017.
 */
class HpPromotion {

    Integer checkPerson(Integer num) {
        Integer discount = Math.floor(num.doubleValue() / 4);
        return discount;
    }

    Double discount(Double pTotal){
        Double discount = 0;
        switch (pTotal){
            case{it >= 1000 && it < 1500}:
                discount =  (pTotal*5)/100;
                break;
            case {it >= 1500}:
                discount = (pTotal*10)/100;
                break;
        }
        return discount;
    }
}
