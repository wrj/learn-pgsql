package shabushi

/**
 * Created by ASUS on 6/26/2017.
 */
class HpTable {
    String tableNo;
    Integer adult = 0;
    Integer child = 0;

    void addAdult(Integer num)
    {
        this.adult += num;
    }

    void addChild(Integer num)
    {
        this.child += num;
    }

    void removeAdult(Integer num)
    {
        if(this.adult > 0)
        {
            this.adult -= num;
        }
    }

    void removeChild(Integer num)
    {
        if(this.child > 0)
        {
            this.child -= num;
        }
    }

    Double total()
    {
        Double adultPrice = 299;
        Double childPrice = 150;
        HpPromotion p = new HpPromotion();
//        Integer discountAdult = p.checkPerson(this.adult);
//        Integer discountChild = p.checkPerson(this.child);
        Double totalPrice = 0;
//        totalPrice += (this.adult - discountAdult) * adultPrice;
//        totalPrice += (this.child - discountChild) * childPrice;
        totalPrice += (this.child * childPrice) + (this.adult * adultPrice);
        Double discount = p.discount(totalPrice)
        println(totalPrice)
        totalPrice -= discount
        println(discount)
        return  totalPrice;
    }
}
