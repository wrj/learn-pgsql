package gameshop

import groovy.sql.Sql

import javax.print.DocFlavor.STRING
import javax.xml.soap.Text

/**
 * Created by ASUS on 6/27/2017.
 */
class Product {
    Integer productId
    String pName;

    List getProductList(Integer limit = 10){

        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/gameshop',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = "SELECT product_id, name as product_name, price, description, amount,(SELECT name as brand_name FROM brand WHERE brand.brand_id = product.brand_id ) FROM product LIMIT ${limit}"
        def productList = sql.rows(sqlText)
        sql.close()
        return productList
    }

    void getProductById(Integer pProductId){
        def sql = Sql.newInstance('jdbc:postgresql://localhost:5432/gameshop',
                'postgres', 'ADMIN*', 'org.postgresql.Driver')

        String sqlText = "SELECT product_id, name as product_name, price, description, amount,(SELECT name as brand_name FROM brand WHERE brand.brand_id = product.brand_id ) FROM product WHERE product_id = ${pProductId}"
        def productList = sql.rows(sqlText)
        sql.close()

        if(!productList.isEmpty()){
            def productObj = productList.get(0);
            this.productId = productObj.get("product_id");
            this.pName = productObj.get("product_name")
        }
    }

    void updateProductById(Integer pProductId,String pName) {
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/gameshop",
                "postgres", "ADMIN*", "org.postgresql.Driver");

        String sqlText = "UPDATE product SET name = '${pName}' WHERE product_id = ${pProductId}"
        sql.execute(sqlText)
        sql.close()
    }

    void insertProductByValues(String productName,Double productPrice, String description ,Integer amount,Integer brandId){
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/gameshop",
                "postgres", "ADMIN*", "org.postgresql.Driver");

        String sqlText = "INSERT INTO product (name, price, description, amount, brand_id) VALUES ('${productName}',${productPrice},'${description}',${amount},${brandId})"
        sql.execute(sqlText)
        sql.close()
    }

    void deleteProductById(Integer productId){
        def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/gameshop",
                "postgres", "ADMIN*", "org.postgresql.Driver");

        sql.execute("DELETE FROM product WHERE product_id = ${productId}")
        sql.close()
    }
}
