package gameshop

import groovy.sql.Sql

import java.sql.SQLClientInfoException


/**
 * Created by ASUS on 6/28/2017.
 */
class Customer {
    // property เก็บข้อมูลของ customer
    Integer customerId
    String firstName
    String lastName
    String email
    String phoneNumber
    Integer addressId

    //ตัวแปร เพื่อไว้ใช้เก็บข้อมูลเเชิ่อมต่อ database
    String urlDB = 'jdbc:postgresql://localhost:5432/gameshop'
    String userDB = 'postgres'
    String passwDB = 'ADMIN*'
    String driverDB = 'org.postgresql.Driver'

    //เป็นMethod สำหรับไว้เรียกcustomer โดยเจาะจงเรียกมาเพียง id เดียวและเก็บค่าไว้ใน property
    void getCustomerById(Integer pCustomerId){
        //เรียกใช้ดาต้าเบส
        Sql sql = Sql.newInstance(this.urlDB, this.userDB, this.passwDB, this.driverDB)

        //เรียกข้อมูลในตาราง customer มาเก็บเป็น list
        List customerList = sql.rows("SELECT customer_id, first_name, last_name, email, phone_number, address_id  FROM customer WHERE customer_id = ${pCustomerId}")
        sql.close() // ส่งปิดใช้งานดาต้าเบสเพราะเรียกมาแล้ว

        //ถ้ามีข้อมูลอยู่ในตาราง customer จะเข้าไปทำใน if
        if(!customerList.isEmpty()){
            Map customerObj = customerList.get(0) //เก็บข้อมูลเป็นแมพลงใน Index ที่ 0

            //ใส่ค่าลงใน property
            this.customerId = customerObj.get("customer_id")
            this.firstName = customerObj.get("first_name")
            this.lastName = customerObj.get("last_name")
            this.email = customerObj.get("email")
            this.phoneNumber = customerObj.get("phone_number")
            this.addressId = customerObj.get("address_id")
        }
    }

    //Method สำหรับหาว่าCustomer มีการซื้อขายไปกี่ครั้งแล้วและให้รีเทิร์นออกมาเป็นค่า int
    Integer paymentCount(){
        Sql sql = Sql.newInstance(this.urlDB, this.userDB, this.passwDB, this.driverDB)

        //เรียกข้อมูลในตาราง payment มาเก็บเป็น list โกยเก็บเฉพาะจำนวทั้งหมดของ payment_id
        List paymentList = sql.rows("SELECT count(payment_id) as payment_count FROM payment WHERE customer_id = ${this.customerId}")
        sql.close()

        //ทำList ให้กลายเป็น Map โดยเก็บค่าจาก Index ที่ 0 ของ list
        Map countPayment = paymentList.get(0)

        //แปลงค่าจำนวนที่รับมาจาก countPayment ให้เป็น int
        Integer count = countPayment.get("payment_count")

        //รีเทิร์น int ออกไป
        return count
    }

    //Method สำหรับหาว่าCustomer มีการซื้อขายไปทั้งหมดรวมราคาเท่าไหร่แล้วและให้รีเทิร์นออกมาเป็นค่า Double
    Double paymentSum(){
        def sql = Sql.newInstance(this.urlDB, this.userDB, this.passwDB, this.driverDB)

        List paymentList = sql.rows("SELECT sum(total_price) as payment_sum FROM payment WHERE customer_id = ${this.customerId}")
        sql.close()

        //ทำList ให้กลายเป็น Map โดยเก็บค่าจาก Index ที่ 0 ของ list
        def sumPayment = paymentList.get(0)

        //แปลงค่าจำนวนที่รับมาจาก sumPayment ให้เป็น Double
        Double sum = sumPayment.get("payment_sum")

        //รีเทิร์นค่าผลรวมของการช์้อขายออกไปเป็น Double
        return sum
    }

}
