##Groovy summary

 ###Class
 
 ###Method
 วิธีการเขียน Method
 
    def methodName(parameter1, parameter2, parameter3) { 
        // Method code goes here 
     }
    
   Static Method
   
    คือ Method ที่ไม่จำเป็นต้องสร้าง object ของคลาสขึ้นมาเพื่อเรียกใช้งาน Method
    
    static void main(String[] args) {
       ...
    } 
    
   none Static Method
   
    คือ Method ที่ต้องสร้าง Obj ขึ้นมาก่อนจึงจะสามารถเรียกใช้ Method จากคลาสอื่นได้
     static void main(String[] args) {
            Example exampleOBJ = new Example()
            println(exampleOBJ.plus(3,6))
            println(calculate(3,5))
    
        }
    
        static Integer calculate(Integer a, Integer b){
            return a+b
        }
    
        def plus(int a, int b){
            return a+b
        }
    
   void Method
    
    คือ Method ที่ไม่จำเป็นต้อง Return ค่าออกมาจาก Method
    
     static void main(String[] args) {
           ...
        } 
    
   Return Method
   
    คือ Method ที่จำเป็นต้องเขียน Return ค่าให้
    
    static Integer calculate(Integer a, Integer b){
                return a+b
    }
   
 ###Variable
 
    ตัวเลข
    -int เก็บตัวเลขที่ไม่มีทศนิยม ถ้าได้ทศนิยมมาตัดออก
    -float เก็บตัวเลขที่มีจุดทศนิยมได้
    -double เก็บตัวเลขทศนิยมที่มีขนาดใหญ่กว่า float
    
    ตัวอีกษร
    -char เก็บตัวอักษรเป็นตัว 'a'
    -String เก็บตัวอีกษร "Hello Groovy"
    
    จริงเท็จ
    -Boolean เก็บแค่trueและfalse

###Operator

    คณิตศาสตร์
    + คือการเพิ่ม
    - คือการลดบ
    * คือการคูณ
    / คือการหารแบบได้ทศนิยม
    % คือการหารเอาเศษ
    ++ คือการเพิ่มทีละ1
    -- คือการลดทีละ1
    
    ความสัมพันธ์
    == คือค่าที่เท่ากันระหว่างสองตัวแปร
    != คือไม่เท่ากับ
    <,<=  น่อยกว่า และ น้อยกว่าเหรือท่ากับ
    >,>=  มากกว่า และ มากกว่าหรือเท่ากับ
    
    Logical
    && และ
    || หรือ
    !  ไม่
    
    
##20/06/2017 

 สรุป ทบทวน Datatype Integer Double , String , Boolean
 
 เรียนการใช้ Lists , Map
 
 ###List
 
    List จะเก็บข้อมูลโดยอ้างอิงอิงเป็นตัวเลข Index โดยไล่จาก 0,1... ไปเรื่อยๆ โดยใช้สัญลักษณ์คือ []
        
    Method ที่ใช้กับ list ในภาษา Groovy
        add()       ใช้เพื่อเพิ่มข้อมูลเข้าไปในลิส
        contain()   ใช้เช็คว่าลิสมีอะไรอยู่ข้างในหรือเปล่า ถ้ามีจะส่งค่าออกมาเป็น true
        get()       ไว้เรียกข้อมูลจาก List ขึ้นมาโดยใช้ Index
        isEmpty()   ใช้เช็คว่าลิสมีอะไรอยู่ข้างในหรือเปล่า ถ้าไม่มีจะส่งค่าออกมาเป็น true
        minus()     ไว้สร้างลิสขึ้นมาใหม่ โดยลบค่าที่อยู่ใน List ที่กำหนดได้ว่าจะลบที่Index ไหน
        plus()      ไว้สร้างลิสขึ้นมาใหม่ โดยเพิ่มค่าที่อยู่ใน List ต่อจากลิสที่มีอยู่ทั้งหมด
        pop()       ลบค่าสุดท้ายของ List
        remove()    ลบข้อมูลใน List โดยกำหนด Index ได้ว่าจะลบที่ Index ที่เท่าไหร่    
        reverse()   สร้าง List ขึ้นมาใหม่โดยสลับ Index จากท้ายสุดมาอยู่ Index หน้าสุด
        size()      ไว้นับจำนวน Index ที่อยู่ใน list
        sort()      ไว้เรียงลำดับ List
      
 ###Map      
    Map คือการเก็บข้อมูลแบบ key : value โดยสัญลักษณืที่ใช้คือ [:]  ["key":"value"]
    
    Method ที่ใช้กับ map ในภาษา Groovy
        containsKey()   ไว้เช็คว่าใน map มีข้อมูลอยู่หรือเปล่า ถ้ามีจะ return เป็น true
        get()           ไว้โชว์ค่า key : value ที่อยู่ใน map
        keySet()        ไว้โชว์ค่าของ key ทั้งหมดที่อยู่ใน map
        put()           ไว้สำหรับเพิ่มข้อมูล key : value ลงใน map โดยจะเพิ่มโดยการใช้ mapName = [key,value]
        size()          ไว้สำหรับแสดงค่าจำนวนของ[key:value]ที่อยู่ในmap
        values()        ไว้สำหรับโชว์เฉพาะค่า Value ที่อยู่ใน map
        
 ---
##21/06/2017

  เรียนเรื่อง loop
 
 #Loop
    Loop คือการวนซ้ำของโปรแกรมจนกว่าจะครบเงื่อนไขหรือเรามีคำสั่งให้อกจากลูป
    
   ###While 
    เป็นการวนซ้ำโดยมีเงื่อนไขตรวจสอบ ไม่ว่าจะเป็น boolean หรือเป็นตัวเลข โดยวิธีการเขียน While จะเป็นดังนี้
    while(condition){
    
    }
    
    int i = 0
    while(int i > 3){
         i++
    จะวนไปจนกว่า ค่า i ที่เพิ่มขึ้นจะมากกว่าหรือเท่ากับ 3 และจะออกจากลูป
    }
   ###For 
    เป็นลูปที่มีการวนซ้ำโดยสามารถกำหนดเงื่อนไขและรอบจะง่ายกว่า While ลูป
    for(variable declaration;expression;Increment) { 
      
          … 
       }
    for(int i = 0;i<9;i++) { 
         จะวนไปจนกว่า ค่า i ที่เพิ่มขึ้นจะมากกว่าหรือเท่ากับ 9 และจะออกจากลูป
     }
   ###For in
    เป็นลูปย่อยของ For อีกทีโดยวิธีการเขียนสั้นกว่าและจะวนจนกว่าจะครบจำนวน
    for(variable in range) { 
       statement #1 
       … 
    }
    
    int[] array = [0,1,2,3]; 
    		
    for(int i in array) { 
             println(i); 
             จะวนไปจนกว่า ค่า i ที่เพิ่มขึ้นจะมากกว่าหรือเท่ากับ Index ในArray และก็จะออกจากลูป
    } 
 
 
 ##22/06/2017
 
  วันนี้เรียนเรื่อง class ,extend ,interface
  สร้าง method 
  เรียนวิธีสร้าง class และวิธีเรียกใช้ class
  เรียนการเรียกใช้ extend  การสร้าง Instance
  การสร้าง properties 
  การใช้ getter setter
  
    class Example {
     static void main(String[] args) {
         Phone Nphone = new  Phone() การสร้าง Instance
  
         Nphone.setBrand("Sumsung") เรียนการเรียกใช้ extend
         Nphone.setOs("Android")  คำสั่ง setter
         Nphone.setName("S8")
         Nphone.setCpu("i7")
         Nphone.setRam("4GB")
         Nphone.setPrice(28900.00)
         
         println(Nphone.getPrice) คำสั่ง getter
     }
    }

    class Hardware {  สร้าง Properties
      String cpu
      String ram
    }

    class Phone extends Hardware {  สร้างคลาสที่สืบทอดกัน Extend
      String brand
      String os
      String name
      double price
    }
  ##23/06/2017
    
  ทบทวนความรู้ในการใช้ method และคลาส 
  พี่บอลให้ลองทำโจทย์ ตอนนี้ก็ยังสับสนในการสร้างmethodมาใช้ เวลาสร้างคลาสขึ้นมาบางครั้งยังไม่แน่ใจว่าควรจะมีอะไรในคลาสนั้นบ้าง ยังมองภาพรวมไม่ค่อยออกครับ กำลังฝึกและทบทวนอยู่ครับ
  
  เข้าใจเรื่อง properties ,extend มากขึ้นเพราะได้ลองทำโจทย์ในห้อง
  
  เรียนโครงสร้าง xml และ เรียนโครงสร้าง json
  
   ***โครงสร้างของ xml***
   
    <root>
        <element>
            <tag>
            </tag>
        <element>
    </root>
    
    xml สามารภใส่ attribute เข้าไปคล้ายๆกับการใส่ key : value 
    <root>
            <element title = 'attribute'>
                <tag>"1"</tag>
                <tag2>"2"</tag2>
            <element>
        </root>
    
   ***โครงสร้างของ json***
   
    โครงสร้างจะคล้ายกับการเอา map เข้าไปใน list โดยมี key กับ value
    [{}]
   
    [   
        {"key":"value"},
        {"name":"Mark"}
    ]
    

  ##26/06/2017
  
  วันนี้อะไรไปบ้าง
  ทบทวนเรื่อง method กับ class โดยใช้โจทย์ สร้าง method เพื่อหาราคาที่มีส่วนลด และได้ลองเขียนโปรแกรมคำนวณอาหารโดยจะต้องคำนวณว่าทั้งหมดที่โต๊ะนี้มากินจ่ายไปเท่าไหร่
  
  เรียน Groovy sql 
  
  เรียนวิธีเรียกใช้ คำสั่งเพื่อเชื่อมต่อกับ database
  เรียนคำสั่ง rows() execute()
  
  ลองทำ select insert update delete ด้วย groovy
  
  ***คำสั่ง rows() execute()***
    rows() จะใช้สำหรับคำสั่ง select จาก sql โดยจะดึงข้อมูลมาเป็นในรูปแบบของ Map
    execute() จะใช้สำหรับคำสั่งพวก insert update delete
    
   
  
  ##27/06/2017
  
  วันนี้เรียนสร้าง JSON ด้วย GROOVY และ แปลง text เป็น JSON
  
  ฝึกเรื่องการใช้ method และ class และฝึกใช้ groovy ร่วมกับ Database
  
  ฝึกการใช้ list ใช้ map 
  ตอนนี้ทีายังสับสนอยู่คือการใช้ map กับ list ครับกำลังฝึกและทบทวนอยู่ สับสนเรื่องการเรียกข้อมูลขึ้นมาใช้ครับ
  
  ###JSON
    JsonOutput.toJson(opject) ไว้สั่งมห้ค่า  obj ที่รับเข้ามาแปลงเป็น json
    Slurper.parseText('{"person":{"name":"Bas","age":20,"pets":["fish","cat"]}}') จะสามารถรับเข้ามาเป็น list และ map ได้
    
  
  ##28/06/2017
  
  วันนี้ทบทวนและฝึกใช้ method และฝึกใช้ groovy ร่วมกับ Database โดยมีความเข้าใจในการเรียกใช้มากขึ้น สามารถทำได้ดีชึ้น ใช้ map กับ list ได้เข้าใจมากกว่าเดิม 
  
  วันนี้พี่บอลได้สอนให้เขียน method ให้ดูซับซ้อนมากขึ้น มีการ search มีการทำหนวดตัวแปลและวิธีใช้ร่วมกับ database ได้เห็นภาพกว่าเดิม   
  
  ##29/06/2017
  
  วันนี้เรียนวิธีใช้ Debug เพื่อดูว่าโค้ดรันเป็นยังไง โค้ดรันไปตามที่เราต้องการหรือเปล่า และเพื่อเช็คว่าโค้ดทำงานยังไง
  
  ทบทวนและฝึกใช้ method และฝึกใช้ groovy ร่วมกับ Database ทำsql ได้ดีขึ้นกว่าเดิม สามารถแก้เออเร่อได้เอง
  
  
  
##XML
  
  โครงสร้างของ XML จะแบ่งเป็น **The Prolog** และ The **Document Element**
  
  **The Prolog**
  
  * XML declaration คือการระบุเวอชั่นของ XML และประกอยด้วย encoding declaration เพื่อระบุว่าเแผนเข้ารหัสตัวอักษร เช่น UTF-8
        
        ตัวอย่าง
        <?xml version="1.0" encoding="UTF-8"?>
        เป็นการกำหนดเวอชั่นว่าจะใช้เวอชั่น 1.0 และ กำหนดภาษาเข้ารหัสไว้ที่ UTF-8
 
  
  * document type declaration  คือ markup code ที่กำหนดการเขียน document type Difinition
           
           ตัวอย่าง
         <!DOCTYPE note SYSTEM "note.dtd">
         
  
  **Document Element**
  
  * คือ ส่วนของ content ที่ประกอบไปด้วย element และ element จะประกอบด้วย tag เปิด ข้อมูล และ tag ปิด
  
        ตัวอย่าง 
            <root>
                <element>
                   <tag>  </tag>
                <element>
            </root>
        
        โดยทุก element ต้องมี tag ปิด เป็นชื่อเดียวกับ tag เปิด 
        และชื่อ tag เป็น sensituve case ต้องตัวอักษรเดียวกันถ้าพิมเล็กก็พิมเล็กเหมือนกัน 
        ตัวอย่างคือ Tag != tag และทุก element ต้องมี tag ปิด
        
        โดย tag ปิดสามารถใช้เป็น self-closing tag ก็ได้
        
         ตัวอย่าง
            <element />
        
        xml สามารภใส่ attribute เข้าไปคล้ายๆกับการใส่ key : value 
        
            ตัวอย่าง
                <root>
                    <element title = 'attribute'>
                        <tag>"1"</tag>
                        <tag2>"2"</tag2>
                    <element>
                </root>
         ใน attribute จะต้องใส่เครื่องหมาย quote เสมอ
         
         
  **CDATA**
  
   CDATA คือ Character Data ถ้าเรามี text ที่มีตัวอักษรเป็นตัวอักษรเฉพาะในการเขียน markup ใน element เช่น
        
        <exampleOfACDATA>
               Since there is no CDATA section
               I can NOT use all sorts of reserves characters
               like > < " and &
               or write things like
               <foo></bar>
               with XHTML and XML
           </exampleOfACDATA>
                 
   จะทำให้เกิด error เพราะxml จะเห็น  “>”, “<“, “&” เป็น markup แพื่อแก้ปัญหาเลยจำเป็นจะต้องใช้ CDATA แต่ถ้าโค้ดเราไม่มีตัวอักษรเฉพาะก็ไม่จำเป็นที่จะต้องใช้ CDATA
         
        ตัวอย่าง
        <exampleOfACDATA>
        <![CDATA[
            Since this is a CDATA section
            I CAN use all sorts of reserves characters
            like > < " and &
            or write things like
            <foo></bar>
        ]]>
        </exampleOfACDATA>
          