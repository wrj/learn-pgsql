-- gameshop database
-- ให้ทำตามคำสั่งในไฟล์นี้

DROP VIEW IF EXISTS amount_payment;
DROP VIEW IF EXISTS bill;
DROP VIEW IF EXISTS category_view;
DROP VIEW IF EXISTS summary_category;
DROP VIEW IF EXISTS summary_item;

DROP TABLE IF EXISTS item;
DROP TABLE IF EXISTS product_category;
DROP TABLE IF EXISTS sub_category;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS brand;
DROP TABLE IF EXISTS payment;
DROP TABLE IF EXISTS customer;
DROP TABLE IF EXISTS address;

CREATE TABLE customer
(
  customer_id SERIAL PRIMARY KEY NOT NULL,
  first_name VARCHAR(100) NOT NULL,
  last_name VARCHAR(100) NOT NULL,
  email VARCHAR(150) NOT NULL UNIQUE ,
  phone_number VARCHAR(10) NOT NULL,
  address_id INT NOT NULL
);
COMMENT ON COLUMN customer.customer_id IS 'primary key ของตารางลูกค้า';
COMMENT ON COLUMN customer.first_name IS 'ชื่อลูกค้า';
COMMENT ON COLUMN customer.last_name IS 'นามสกุลลูกค้า';
COMMENT ON COLUMN customer.email IS 'อีเมลล์ลูกค้า';
COMMENT ON COLUMN customer.phone_number IS 'เบอร์โทรลูกค้า';
COMMENT ON COLUMN customer.address_id IS 'foreign keys กับตาราง address';
COMMENT ON TABLE customer IS 'ตารางเก็บข้อมูลลูกค้า';


CREATE TABLE address
(
  address_id SERIAL PRIMARY KEY NOT NULL,
  description TEXT NOT NULL,
  province VARCHAR(100) NOT NULL,
  country VARCHAR(100) NOT NULL,
  zipcode INT NOT NULL
);
COMMENT ON COLUMN address.address_id IS 'primarykey ของตารางลูกค้าเพื่อเชื่อมกับ customer.address_id';
COMMENT ON COLUMN address.description IS 'สำหรับกรอกข้อมูลที่อยู่ลูกค้า เช่น Street Address, P.O. Boxes, c/o, apartment, suite, unit, building, floor, etc';
COMMENT ON COLUMN address.province IS 'เก็บข้อมูลจังหวัด';
COMMENT ON COLUMN address.country IS 'เก็บข้อมูลประเทศ';
COMMENT ON COLUMN address.zipcode IS 'เก็บรหัสไปรษณีย์';
COMMENT ON TABLE address IS 'ตารางเก็บข้อมูลที่อยู่ลูกค้า';



CREATE TABLE product
(
  product_id SERIAL PRIMARY KEY NOT NULL ,
  name varchar(100) NOT NULL,
  price INT NOT NULL,
  description TEXT ,
  amount INT NOT NULL DEFAULT 0,
  brand_id INT NOT NULL
);
COMMENT ON TABLE product IS 'ตารางเก้บข้อมูลสินค้าทุกชนิดที่ขาย';
COMMENT ON COLUMN product.name IS 'ชื่อสินค้าที่จะขาย';
COMMENT ON COLUMN product.price IS 'ราคาสินค้า';
COMMENT ON COLUMN product.description IS 'ข้อมูลรายละเอียดสินค้า';
COMMENT ON COLUMN product.brand_id IS 'ไว้เชื่อมตาราง product และ brand';


CREATE TABLE brand
(
  brand_id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(100) NOT NULL
);
COMMENT ON COLUMN brand.name IS 'แสดงชื่อแบรนด์';
COMMENT ON TABLE brand IS 'ไว้สำหรับแสดงแบรนด์ของสินค้า';


CREATE TABLE category
(
  category_id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(100) NOT NULL,
  root BOOLEAN NOT NULL DEFAULT FALSE
);
COMMENT ON COLUMN category.name IS 'ชื่อประเภทของสินค้า';
COMMENT ON COLUMN category.root IS 'ไว้สำหรับเช็คว่า category อันไหนคือ category ใหญ่ จะให้เป็นTrue ส่วน category ที่เล็กกว่าจะให้เป็น false';
COMMENT ON TABLE category IS 'ตารางประเภทสินค้า';


CREATE TABLE sub_category
(
  parent_id INT NOT NULL,
  child_id INT NOT NULL,
  CONSTRAINT sub_category_parent_id_child_id_pk PRIMARY KEY (parent_id, child_id),
  CONSTRAINT sub_category_category_parent_id_fk FOREIGN KEY (parent_id) REFERENCES category (category_id),
  CONSTRAINT sub_category_category_child_id_fk FOREIGN KEY (child_id) REFERENCES category (category_id)
);
COMMENT ON COLUMN sub_category.parent_id IS 'ID ประเภทสินค้าหัวข้อหลัก';
COMMENT ON COLUMN sub_category.child_id IS 'ID ประเภทนสินค้าหัวข้อย่อย';
COMMENT ON TABLE sub_category IS 'ตารางประเภทสินค้าแบบแยกหมวดหมู่';


CREATE TABLE product_category
(
  category_id INT NOT NULL,
  product_id INT NOT NULL,
  CONSTRAINT product_category_category_id_product_id_pk PRIMARY KEY (category_id, product_id),
  CONSTRAINT product_category_category_category_id_fk FOREIGN KEY (category_id) REFERENCES category (category_id),
  CONSTRAINT product_category_product_product_id_fk FOREIGN KEY (product_id) REFERENCES product (product_id) ON DELETE SET NULL
);
COMMENT ON COLUMN product_category.category_id IS 'ID สำหรับเชื่อมความสัมพันธืกับตาราง category';
COMMENT ON COLUMN product_category.product_id IS 'ID สำหรับเชื่อมความสัมพันธ์กับตาราง Product';
COMMENT ON TABLE product_category IS 'ตารางความสัมพันธ์ระหว่าง Product และ Category';


CREATE TABLE payment
(
  payment_id SERIAL PRIMARY KEY NOT NULL,
  payment_date TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  customer_id INT NOT NULL,
  total_price NUMERIC NOT NULL DEFAULT 0,
  CONSTRAINT payment_customer_customer_id_fk FOREIGN KEY (customer_id) REFERENCES customer (customer_id)

);
COMMENT ON COLUMN payment.payment_date IS 'เก็บเวลาการขาย';
COMMENT ON COLUMN payment.customer_id IS 'ไว้สำหรับเก็บข้อมูลลูกค้าที่ขายไห้';
COMMENT ON COLUMN payment.total_price IS 'เก็บยอดค่าใช้จ่ายรวมที่ลูกค้าจะต้องชำระ';
COMMENT ON TABLE payment IS 'ตารางแสดงการจ่ายเงินสำหรับลูกค้า';


CREATE TABLE item
(
  item_id SERIAL PRIMARY KEY NOT NULL,
  product_id INT NOT NULL,
  payment_id INT NOT NULL,
  amount INT NOT NULL,
  total_price NUMERIC DEFAULT 0 NULL,
  CONSTRAINT item_product_product_id_fk FOREIGN KEY (product_id) REFERENCES product (product_id)
);
COMMENT ON COLUMN item.product_id IS 'ไว้เชื่อมความสำพันธืกับตาราง product';
COMMENT ON COLUMN item.payment_id IS 'ไว้เชื่อมความสำพันธืกับตาราง payment';
COMMENT ON COLUMN item.amount IS 'เก็บจำนวนสินค้าที่ซื้อ';
COMMENT ON COLUMN item.total_price  IS 'เก็บจำนวนราคารวมของสินค้าแต่ละชนิด';
COMMENT ON TABLE item IS 'ตารางเก็บจำนวนสินค้าที่ลูกค้าต้องการซื้อ';

ALTER TABLE customer
  ADD CONSTRAINT customer_address_address_id_fk
FOREIGN KEY (address_id) REFERENCES address (address_id);

ALTER TABLE product
  ADD CONSTRAINT product_brand_brand_id_fk
FOREIGN KEY (brand_id) REFERENCES brand (brand_id);

ALTER TABLE item
  ADD CONSTRAINT item_payment_payment_id_fk
FOREIGN KEY (payment_id) REFERENCES payment (payment_id);

INSERT INTO address(description, province, country, zipcode) VALUES
  ('123 test description [1]','BKK','Thailand',10140),
  ('456 test description [2]','Korat','Thailand',30170);

INSERT INTO customer(first_name, last_name, email, phone_number, address_id) VALUES
  ('Dave','John','test@hotmail.com','0908371901',2),
  ('Maree','Hanna','ggg@gmail.com0','0860935385',1);

INSERT INTO brand (name) VALUES
  ('sony'),
  ('EA'),
  ('UBISOFT'),
  ('Bohemier'),
  ('asus'),
  ('nvidia'),
  ('INDY');

INSERT INTO product (name, price, description, brand_id, amount) VALUES
  ('Com A33384267',50000,'4 gb ram GTX2088',1,10),
  ('FIFA19',500,'Foot ball game',2,20),
  ('GTX980',12000,'VGA CARD VERY GOOD',6,1);

INSERT INTO category (name, root) VALUES
  ('Game',TRUE ),
  ('Play Station',DEFAULT ),
  ('Xbox',DEFAULT ),
  ('PC',DEFAULT ),
  ('Electronic',TRUE ),
  ('VGA',DEFAULT ),
  ('Computer',DEFAULT );

INSERT INTO sub_category (parent_id, child_id) VALUES
  (1,2),
  (1,3),
  (1,4),
  (5,6),
  (5,7);

INSERT INTO product_category (product_id,category_id) VALUES
  (1,7),
  (2,4),
  (3,6);

INSERT INTO payment (customer_id, total_price) VALUES
  (1,100500),
  (2,12500),
  (2,500);

INSERT INTO item (product_id, payment_id, amount) VALUES
  (1,1,2),
  (2,1,1),
  (2,2,1),
  (3,2,1),
  (2,3,1);

-- ตารางโชว์ ข้อมูลสินค้าที่อยู่ใน category
CREATE VIEW category_view AS
SELECT
  c.category_id,
  c.name as category_name,
  ch.name as child_category_name,
  p.name,
  p.amount
FROM
  sub_category sc
  INNER JOIN category c ON c.category_id = sc.parent_id
  INNER JOIN category ch ON sc.child_id = ch.category_id
  INNER JOIN product_category pc ON ch.category_id = pc.category_id
  INNER JOIN product p ON pc.product_id = p.product_id
WHERE  c.root = TRUE;


-- ใช้วิธี sub query

CREATE VIEW bill AS
SELECT
  p.payment_id,
  c.first_name,
  (SELECT name FROM product WHERE product.product_id = i.product_id ),
  p.total_price,
  p.payment_date
FROM
  customer c
  INNER JOIN payment p ON p.customer_id = c.customer_id
  INNER JOIN item i ON  i.payment_id = p.payment_id;

CREATE VIEW amount_payment AS
SELECT
  concat(first_name,' ',last_name),
  (SELECT count(payment_id)as count_payment FROM payment WHERE payment.customer_id = customer.customer_id),
  (SELECT sum(total_price) as total_payment FROM payment WHERE payment.customer_id = customer.customer_id)

FROM customer;

-- ตารางแสดงว่าสินค้าถูกขายไปกี่ครั้งและขายไปทะเงหมดเท่าไหร่
CREATE VIEW summary_item AS
SELECT
  name,
  (SELECT count(item_id) as payment_count FROM item WHERE item.item_id = product.product_id ),
  (SELECT sum(amount) as amount FROM item WHERE item.item_id = product.product_id)
FROM product;

--ตารางแสดงจำนวนชนิดสินค้าในแต่ละประเภทสินค้า

CREATE VIEW summary_category AS
SELECT
  name,
  (SELECT count(product_id) FROM product_category WHERE product_category.category_id = category.category_id)
FROM category WHERE  root != TRUE ;










